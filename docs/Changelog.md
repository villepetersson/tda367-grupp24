# Changelog for Playfuel API

## Version CURRENT

**Added maxnumberofslots.**
**Added Choice input type.**
**Added logo.png and screen.png**
**Added description.**

Added input-specific arguments:
- minvalue for Integer
- maxvalue for Integer
- values for Choice

## Version 001

Initial release.
