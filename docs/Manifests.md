# Manifest documentation
*Version 001*

## Logo and screenshot
A logo.png and screen.png should be in the root directory of your jar file.

## Name, format and location
The playfuel.mf file should be in the root directory of your jar file and is formatted using JSON. Be sure to run your manifest through http://jsonlint.com/ to be sure that nothing is malformed.

## Initial example
	{
	"gamename":"Test game",
	"description":"A game description.",
	"settingsfactory":[
		{
			"input":"Integer",
			"defaultval":"5",
			"identifier":"players",
			"prettyname":"Number of players"
		},
		{
			"input":"String",
			"defaultval":"Heya",
			"identifier":"welcome_message",
			"prettyname":"Welcome message"
		},
		{
			"input":"Boolean",
			"defaultval":"TRUE",
			"identifier":"areyoupro",
			"prettyname":"Are you pro"
		},
		{
			"input":"Color",
			"defaultval":"1",
			"identifier":"color",
			"prettyname":"Color",
			"values":[
				"Red",
				"Blue",
				"Green",
				"Pink"
			]
		}
	],
	"slotsettingsfactory":[
		{
			"input":"Integer",
			"defaultval":"1",
			"identifier":"team",
			"prettyname":"Team"
		},
		{
			"input":"Boolean",
			"defaultval":"TRUE",
			"identifier":"areyoupro",
			"prettyname":"Are you pro"
		}
	],
	"playfuelgameserverclass":"Testgame",
	"playfuelgameclientclass":"Testclient",
	"identifier":"Testgame0001",
	"maxnumberofslots":"6"
	}


## Fields

### gamename
This is the human readable name of the game, which will show up in the client GUI.

### settingsfactory
A list of match-specific settings which will be decided by the lobby host.

### slotsettingsfactory
A list of per-slot settings which will be decided by each player.

### Global setting arguments

#### input
The data type of the setting.
Data types currently supported:
- Integer
- String
- Boolean
- Choice

#### defaultval
The default value for the option.

#### identifier
A unique identifier, used to retrieve the setting.

#### prettyname
Human readable name of the setting, shown in the client GUI.

### Integer setting arguments
These settings are only required if "input":"Integer"

#### minvalue
Minimum selectable value

#### maxvalue
Maximum selectable value

### Choice setting arguments

#### values
A list of strings which are the human readable names of the settings.

### playfuelgameserverclass
Name of a java class which implements PlayfuelGameServer.

### playfuelgameclientclass
Name of a java class which implements PlayfuelGameClient.

### identifier
Unique identifier for your game and version

### maxnumberofslots
The maximum number of players the game can handle.
