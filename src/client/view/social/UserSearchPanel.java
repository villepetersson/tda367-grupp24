package client.view.social;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.border.LineBorder;

import models.Friend;
import models.IMessage;
import models.Profile;

import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.UserSearchMessage;
import utilities.msg.messages.UserSearchResultsMessage;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.ImageIcon;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Cursor;
import javax.swing.SwingConstants;

import client.ClientModel;
import client.messages.ClientRefetchSearchResultsMessage;
import client.view.graphics.JPlayfuelButton;

import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridLayout;

/**
 * Panel containing the friend search
 *
 */
public class UserSearchPanel extends JPanel implements IMessageHandler, ActionListener {
	private JTextField txtSk;
	private static String DEFAULT_SEARCH_TEXT = " Search...";
	private JButton btnSk;
	
	JPanel searchResultsPanel;

	/**
	 * Creates a UserSearchPanel
	 */
	public UserSearchPanel() {
		// Register for messages
		MessageBus.getInstance().register(this, UserSearchResultsMessage.class);
		MessageBus.getInstance().register(this, ClientRefetchSearchResultsMessage.class);
		
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent arg0) {
				txtSk.requestFocusInWindow();
			}
		});
		setLayout(null);
		
		txtSk = new JTextField();
		txtSk.setAlignmentX(Component.LEFT_ALIGNMENT);
		txtSk.setFont(new Font("Helvetica", Font.PLAIN, 14));
		txtSk.setHorizontalAlignment(SwingConstants.LEFT);
		txtSk.setBorder(new LineBorder(ClientModel.getTheme().TEXTBOX_BORDER(), 1));
		txtSk.setBounds(6, 37, 164, 25);
		txtSk.setActionCommand("search");
		add(txtSk);
		txtSk.setColumns(10);
		setBorder(new LineBorder(ClientModel.getTheme().DETAIL_BACKGROUND(), 2));
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		//setBorder(new LineBorder(Color.GRAY, 5));
		
		btnSk = new JPlayfuelButton("");
		btnSk.setPressedIcon(new ImageIcon(UserSearchPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SearchPressedSmall.png")));
		btnSk.setBorderPainted(false);
		btnSk.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSk.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				btnSk.setIcon(new ImageIcon(UserSearchPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SearchPressedSmall.png")));
			}
			public void mouseReleased(MouseEvent arg0){
				btnSk.setIcon(new ImageIcon(UserSearchPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SearchSmall.png")));

			}
		});
		btnSk.setAlignmentY(Component.TOP_ALIGNMENT);
		btnSk.setBounds(170, 33, 125, 34);
		btnSk.setIcon(new ImageIcon(UserSearchPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SearchSmall.png")));
		btnSk.setActionCommand("search");
		btnSk.addActionListener(this);
		txtSk.addActionListener(this);
		add(btnSk);
		
		searchResultsPanel = new JPanel();
		searchResultsPanel.setBounds(6, 77, 272, 358);
		searchResultsPanel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());

		add(searchResultsPanel);
		searchResultsPanel.setLayout(new BoxLayout(searchResultsPanel, BoxLayout.Y_AXIS));
		
		JLabel lblNewLabel = new JLabel("Search for friends");
		lblNewLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		lblNewLabel.setFont(ClientModel.getTheme().HEADER_FONT());
		lblNewLabel.setBounds(6, 6, 164, 22);
		add(lblNewLabel);

	}

	@Override
	public void onMessage(IMessage msg) {
		if (msg instanceof UserSearchResultsMessage) {
			List<Profile> profiles = ((UserSearchResultsMessage)msg).getProfiles();
			searchResultsPanel.removeAll();
			
			if (profiles.size() > 0) {
				for(Profile p : profiles) {
					if(!(p instanceof Friend)){
						searchResultsPanel.add(new FriendListItemPanel(p));
					}
				}
				
			} else {
				JLabel label = new JLabel("No search results");
				label.setForeground(ClientModel.getTheme().BODY_COLOR());
				searchResultsPanel.add(label);
				
			}
			searchResultsPanel.updateUI();
		} else if (msg instanceof ClientRefetchSearchResultsMessage) {
			MessageBus.getInstance().pushMessage(new UserSearchMessage(txtSk.getText()));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("search")) {
			// Only search if a query is entered
			if (! txtSk.getText().equals("") && ! txtSk.getText().equals(UserSearchPanel.DEFAULT_SEARCH_TEXT)) {
				MessageBus.getInstance().pushMessage(new UserSearchMessage(txtSk.getText()));
				txtSk.setText(DEFAULT_SEARCH_TEXT);
				txtSk.requestFocus();
				txtSk.requestFocusInWindow();
				txtSk.moveCaretPosition(0);
			}
		}
		
	}
}
