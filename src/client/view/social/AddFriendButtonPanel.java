package client.view.social;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Cursor;

/**
 * Panel containing the add friend button
 *
 */
public class AddFriendButtonPanel extends JButton {
	/**
	 * Creates a new AddFriendButtonPanel
	 */
	public AddFriendButtonPanel(){
		javax.swing.JLabel label = new javax.swing.JLabel(new ImageIcon(AddFriendButtonPanel.class.getResource("/client/view/graphics/image_resources/addFriend.png")));
		label.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		this.add(label);
	}
}
