package client.view.social;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import utilities.msg.MessageBus;
import utilities.msg.messages.ClientMessageMessage;
import utilities.msg.messages.UserFriendAcceptMessage;
import utilities.msg.messages.UserFriendAddMessage;
import utilities.msg.messages.UserFriendRemoveMessage;
import utilities.msg.messages.ClientMessageMessage.Status;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import models.Friend;
import models.Profile;
import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import java.awt.Component;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import client.ClientModel;
import client.messages.ClientRefetchSearchResultsMessage;

import java.awt.Font;


/**
 * 
 * List item panel contaning a friend, in the friend list
 *
 */
public class FriendListItemPanel extends JPanel {
	
	private Profile profile;
	private JLabel image;
	private JLabel lblStatus;
	
	
	/**
	 * Creates a new FriendListItemPanel 
	 *
	 */
	public FriendListItemPanel(final Profile p) {
		setAlignmentY(Component.TOP_ALIGNMENT);
		setAlignmentX(Component.LEFT_ALIGNMENT);
		
		this.profile = p;
		
		setBorder(new LineBorder(ClientModel.getTheme().ACCENT_BACKGROUND(), 1));
		setPreferredSize(new Dimension(190, 45));
		setMaximumSize(new Dimension(190, 45));
		image = new JLabel();
		image.setBounds(148, 3, 36, 36);
		image.setHorizontalAlignment(SwingConstants.LEFT);
		image.setIcon(new ImageIcon(FriendListItemPanel.class.getResource("/client/view/graphics/image_resources/defaultFriend.png")));
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());

		
		JLabel lblPlayerName = new JLabel(p.getUsername());
		lblPlayerName.setBounds(9, 3, 137, 36);
		lblPlayerName.setFont(ClientModel.getTheme().HEADER_FONT());
		lblPlayerName.setForeground(ClientModel.getTheme().BODY_COLOR());
		lblPlayerName.setHorizontalTextPosition(SwingConstants.RIGHT);
		lblPlayerName.setHorizontalAlignment(SwingConstants.LEFT);
		
		lblStatus = new JLabel();
		lblStatus.setBounds(9, 3, 0, 0);
		setLayout(null);
		add(lblPlayerName);
		add(lblStatus);
		add(image);
		
		//Friendlist		
		if (p instanceof Friend) {
			
			final Friend friend = (Friend) p;
			
			lblPlayerName.setForeground(friend.isOnline() ? ClientModel.getTheme().ONLINE() : ClientModel.getTheme().OFFLINE());
			lblPlayerName.setForeground(friend.isBusy() ? ClientModel.getTheme().BUSY() : lblPlayerName.getForeground());
			lblPlayerName.setForeground(friend.isInGame() ? ClientModel.getTheme().INGAME() : lblPlayerName.getForeground());
			
			if (friend.isAccepted()) {
				image.setIcon(new ImageIcon(FriendListItemPanel.class.getResource("/client/view/graphics/image_resources/newButtons/RemoveFriendSmall.png")));
				image.setToolTipText("Delete friend");

				image.addMouseListener(new MouseListener() {
					@Override
					public void mouseReleased(MouseEvent arg0) {}
					@Override
					public void mousePressed(MouseEvent arg0) {
						image.setIcon(new ImageIcon(FriendListItemPanel.class.getResource("/client/view/graphics/image_resources/newButtons/RemoveFriendPressedSmall.png")));
						
					}
					@Override
					public void mouseExited(MouseEvent arg0) {}
					@Override
					public void mouseEntered(MouseEvent arg0) {
						setCursor(new Cursor(Cursor.HAND_CURSOR));
					}
					@Override
					public void mouseClicked(MouseEvent arg0) {
						MessageBus.getInstance().pushMessage(new UserFriendRemoveMessage(friend.getUsername()));
						MessageBus.getInstance().pushMessage(new ClientRefetchSearchResultsMessage());
					}
				});
				updateUI();
			
			// Else their friendship is under review
			} else {
				image.setIcon(new ImageIcon(FriendListItemPanel.class.getResource("/client/view/graphics/image_resources/newButtons/AcceptFriendSmall.png")));
				image.setToolTipText("Accept friend request");
				setBorder(new LineBorder(Color.GRAY, 2));

				image.addMouseListener(new MouseListener() {
					
					@Override
					public void mouseReleased(MouseEvent arg0) {}
					@Override
					public void mousePressed(MouseEvent arg0) {
						image.setIcon(new ImageIcon(FriendListItemPanel.class.getResource("/client/view/graphics/image_resources/newButtons/AcceptFriendPressedSmall.png")));
					}
					@Override
					public void mouseExited(MouseEvent arg0) {}
					
					@Override
					public void mouseEntered(MouseEvent arg0) {
						setCursor(new Cursor(Cursor.HAND_CURSOR));
					}
					@Override
					public void mouseClicked(MouseEvent arg0) {
							MessageBus.getInstance().pushMessage(new UserFriendAcceptMessage(profile.getUsername()));
							MessageBus.getInstance().pushMessage(new ClientRefetchSearchResultsMessage());
					}
					
				});
				updateUI();
				
			}
		//Searchlist
		} else {
			image.setIcon(new ImageIcon(FriendListItemPanel.class.getResource("/client/view/graphics/image_resources/newButtons/AddFriendSmall.png")));
			image.setToolTipText("Add friend");
			//setBorder(new LineBorder(ColorClientModel.getCurrent().TEXTBOX_BORDER, 1));
			image.addMouseListener(new MouseListener() {
				@Override
				public void mouseReleased(MouseEvent e) {
					image.setIcon(new ImageIcon(FriendListItemPanel.class.getResource("/client/view/graphics/image_resources/newButtons/AddFriendSmall.png")));
				}
				@Override
				public void mousePressed(MouseEvent e) {
					image.setIcon(new ImageIcon(FriendListItemPanel.class.getResource("/client/view/graphics/image_resources/newButtons/AddFriendPressedSmall.png")));					
				}
				@Override
				public void mouseExited(MouseEvent e) {}
				@Override
				public void mouseEntered(MouseEvent e) {
					setCursor(new Cursor(12));
				}
				@Override
				public void mouseClicked(MouseEvent e) {
					MessageBus.getInstance().pushMessage(new UserFriendAddMessage(profile.getUsername()));
					String s = "Friend request successfully sent to " + p.getUsername();
					MessageBus.getInstance().pushMessage(new ClientMessageMessage(s , Status.SUCCESS));				

				}
			}); 
				
			updateUI();
			
		}
	}
}
