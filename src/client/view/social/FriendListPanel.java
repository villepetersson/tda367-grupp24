package client.view.social;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import models.Friend;
import models.IMessage;

import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.*;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import java.awt.Component;
import javax.swing.Box;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.util.List;

import javax.swing.LayoutStyle.ComponentPlacement;

import client.ClientModel;

/**
 * Panel containing the friend list
 *
 */
public class FriendListPanel extends JPanel implements IMessageHandler{
	private JPanel panel;
	private JLabel lblNewLabel;
	private ClientModel clientModel;
	/**
	 * Creates a new FriendListPanel
	 * @param model
	 */
	public FriendListPanel(ClientModel model) {
		//Send messages
		MessageBus.getInstance().pushMessage(new UserFriendListGetMessage());
		
		// Register for messages
		MessageBus.getInstance().register(this, UserFriendListMessage.class);
		
		this.clientModel = model;
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		setBorder(new LineBorder(ClientModel.getTheme().DETAIL_BACKGROUND(), 2));

		lblNewLabel = new JLabel("Friendlist");
		lblNewLabel.setBounds(6, 6, 438, 22);
		lblNewLabel.setFont(ClientModel.getTheme().HEADER_FONT());
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		lblNewLabel.setForeground(ClientModel.getTheme().BODY_COLOR());

		panel = new JPanel();
		panel.setBounds(6, 29, 190, 423);
		panel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());

		lblNewLabel.setVisible(true);
		setLayout(null);
		add(lblNewLabel);
		add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		validate();
	}
	
	
	@Override
	public void onMessage(IMessage msg) {
		if (msg instanceof UserFriendListMessage) {
			UserFriendListMessage uflm = (UserFriendListMessage)msg;
			List<Friend> friends = uflm.getFriends();
			
			panel.removeAll();
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			clientModel.setLastFriendsList(friends);
			for(Friend f : friends) {
				panel.add(new FriendListItemPanel(f));
			}
			panel.setVisible(true);
			lblNewLabel.setVisible(true);
			panel.updateUI();
		}
	}
}
