package client.view.social;

import java.awt.Color;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.LineBorder;

import models.IMessage;
import models.User;

import client.ClientModel;

import com.google.gson.Gson;

import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.UserFriendListGetMessage;

import java.awt.FlowLayout;
import java.awt.Font;

/**
 * 
 * Wrapping panel for panels in the social tab
 *
 */
public class SocialPanel extends JPanel {
	
	
	/**
	 * Creates a new SocialPanel
	 * @param model
	 */
	public SocialPanel(ClientModel model) {		
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		//setBorder(new LineBorder(Color.BLACK, 2));
		
		UserSearchPanel searchPanel = new UserSearchPanel();
		
		FriendListPanel FriendsPanel = new FriendListPanel(model);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(searchPanel, GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(FriendsPanel, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(FriendsPanel, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 358, Short.MAX_VALUE)
						.addComponent(searchPanel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE))
					.addGap(6))
		);
		searchPanel.setLayout(null);
		FriendsPanel.setLayout(null);
		setLayout(groupLayout);
	}
}
