package client.view.lobby;

import java.awt.Cursor;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import client.ClientModel;
import client.messages.ClientModalMessage;
import client.view.graphics.JPlayfuelButton;
import client.view.modals.ModalFriendPanel;
import client.view.modals.SettingsPanel;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import models.Slot;
import models.Friend;

import utilities.msg.MessageBus;
import utilities.msg.messages.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.io.IOException;


/**
 * The panel representing each player in the lobby as a "slot", determines what components is appropriate for the context, if for example it is the panel for the own player
 *
 */
public class SlotPanel extends JPanel {
	private Slot slot;
	private ClientModel cmodel;
	
	/**
	 * 
	 * Creates a SlotPanel
	 * @param model - the client model
	 * @param slot - the slot model
	 */
	public SlotPanel(ClientModel model, final Slot slot) {
		this.slot = slot;
		this.cmodel = model;
		
		SlotImagePanel slotImage = new SlotImagePanel(cmodel.getProfile().getUsername(),this.slot);
		GroupLayout groupLayout = new GroupLayout(this);
		JComponent belowPlayer = new JLabel("Waiting for player");
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());

		if (this.slot.isOccupied()) {
			
			// If is me
			if (this.slot.getUser().equals(cmodel.getProfile())) {
				JButton btnSlotSettings = new JPlayfuelButton("My settings");
				
				btnSlotSettings.setIcon(new ImageIcon(SlotPanel.class.getResource("/client/view/graphics/image_resources/newButtons/MySettingsSmall.png")));
				btnSlotSettings.setPressedIcon(new ImageIcon(SlotPanel.class.getResource("/client/view/graphics/image_resources/newButtons/MySettingsPressedSmall.png")));
				btnSlotSettings.setBorderPainted(false);
				btnSlotSettings.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						MessageBus.getInstance().pushMessage( new ClientModalMessage(new SettingsPanel(cmodel, slot,true), "Player settings"));
					}
				});
				belowPlayer = btnSlotSettings;
			
			// Or is opponent
			} else {
				// If waiting for response
				if (this.slot.isAccepted()) {
					belowPlayer = new JLabel(this.slot.getUser().getUsername());
					belowPlayer.setFont(ClientModel.getTheme().HEADER_FONT());
					belowPlayer.setForeground(ClientModel.getTheme().BODY_COLOR());
				} else {
					belowPlayer = new JPanel();
					belowPlayer.add(new JLabel("Waiting for " + this.slot.getUser().getUsername()), ClientModel.getTheme().HEADER_FONT());
					if (cmodel.isOwnerOfCurrentLobby()) {
						JButton uninviteBtn = new JPlayfuelButton("Uninvite");
						uninviteBtn.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent arg0) {
								MessageBus.getInstance().pushMessage(new LobbySlotUninviteMessage(slot.getID()));
							}
						});
						
						belowPlayer.add(uninviteBtn, ClientModel.getTheme().HEADER_FONT());
					}
				}
			}
		} else {
			try {
				slot.setImage(ImageIO.read(SlotPanel.class.getResource("/client/view/graphics/image_resources/newButtons/UnknownPlayer.png")));
			} catch (IOException e1) {
				e1.printStackTrace();
			} //so far...
			
			// If user is Lobby owner, show invite button
			if (cmodel.getCurrentLobby() != null && cmodel.getCurrentLobby().isOwner(cmodel.getProfile())) {
			
				JButton btnInvitePlayer = new JPlayfuelButton("Invite player");
				btnInvitePlayer.setIcon(new ImageIcon(SlotPanel.class.getResource("/client/view/graphics/image_resources/newButtons/InviteSmall.png")));
				btnInvitePlayer.setPressedIcon(new ImageIcon(SlotPanel.class.getResource("/client/view/graphics/image_resources/newButtons/InvitePressedSmall.png")));
				btnInvitePlayer.setBorderPainted(false);
				btnInvitePlayer.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent arg0) {
						
						// Build up a Modal and send it
						
						// Content should scroll
						ModalFriendPanel modalFriendPanel = new ModalFriendPanel(cmodel.getLastFriendsList(), slot);
						
						ClientModalMessage cmm = new ClientModalMessage(modalFriendPanel, "Choose a friend to invite");
						
						MessageBus.getInstance().pushMessage(cmm);
					}
				});
				
				btnInvitePlayer.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseEntered(MouseEvent e) {
						setCursor(new Cursor(Cursor.HAND_CURSOR));
					}
					@Override
					public void mouseExited(MouseEvent e) {
						setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					}
				});
				belowPlayer = btnInvitePlayer;
			}
		}
		
		
		

		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(slotImage, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(belowPlayer, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(slotImage, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(belowPlayer,GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		setLayout(groupLayout);
	}
}
