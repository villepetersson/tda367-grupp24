package client.view.lobby;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import client.ClientModel;


/**
 * The top level containing panel for the lobby in the client, contains the lobby inner panels
 *
 */
public class LobbyPanel extends JPanel {
	LobbyTopBar topBar;
	/**
	 * Creates a LobbyPanel
	 * @param model - the model of the wrapping client
	 */
	public LobbyPanel(ClientModel model){
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setPreferredSize(new Dimension(525,520));

		StartExitButtonsPanel buttonsInLobby = new StartExitButtonsPanel(model);
		PlayersInLobbyPanel playersInLobbyPanel = new PlayersInLobbyPanel(model);
		ChatPanel chat = new ChatPanel(model.getCurrentLobby(), model);
		topBar = new LobbyTopBar(model);
		
		this.add(topBar);
		this.add(playersInLobbyPanel);
		this.add(chat);
		this.add(buttonsInLobby);
		
		setVisible(true);
		repaint();
	}
	
}
