package client.view.lobby;

import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import models.Lobby;

import utilities.msg.MessageBus;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

import client.ClientModel;
import client.messages.ClientModalMessage;
import client.view.graphics.JPlayfuelButton;
import client.view.modals.SettingsPanel;

import java.awt.Cursor;


/**
 * Inner panel containing settings button and header for the lobby
 *
 */
public class LobbyTopBar extends JPanel {
	ClientModel cmodel;
	/**
	 * Creates a lobbytopbar
	 * @param model - the wrapping client model
	 */
	public LobbyTopBar(ClientModel model){
		this.cmodel=model;
		
		//If new lobby is created for update purposes, use the old chat.
		Lobby lobby = cmodel.getCurrentLobby();
		
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		JLabel lobbyText = new JLabel(lobby.getGame().getGameName());
		lobbyText.setFont(ClientModel.getTheme().HEADER_FONT());
		lobbyText.setForeground(ClientModel.getTheme().BODY_COLOR());
		lobbyText.setFont(new Font("Helvetica", Font.BOLD, 16));

		JButton btnSettings = new JPlayfuelButton("");
		btnSettings.setPressedIcon(new ImageIcon(LobbyTopBar.class.getResource("/client/view/graphics/image_resources/newButtons/MatchSettingsPressSmall.png")));
		btnSettings.setBorderPainted(false);
		btnSettings.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSettings.setIcon(new ImageIcon(LobbyTopBar.class.getResource("/client/view/graphics/image_resources/newButtons/MatchSettingsSmall.png")));
		btnSettings.setFont(new Font("Helvetica", Font.PLAIN, 15));
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lobbyText)
					.addPreferredGap(ComponentPlacement.RELATED, 212, Short.MAX_VALUE)
					.addComponent(btnSettings, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnSettings, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addComponent(lobbyText))
					.addContainerGap(257, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
		
		// If has no lobby or if not owner (slot 0) settings should not be editable
		if (model.getCurrentLobby() == null || ! model.getCurrentLobby().getSlot(0).getUser().equals(model.getProfile())) {
			btnSettings.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					MessageBus.getInstance().pushMessage( new ClientModalMessage(new SettingsPanel(cmodel,false), "Match Settings"));
				}
			});
		} else {
			btnSettings.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					MessageBus.getInstance().pushMessage( new ClientModalMessage(new SettingsPanel(cmodel,true), "Match Settings"));
				}
			});
		}
		
		
	}
}
