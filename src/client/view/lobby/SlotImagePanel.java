package client.view.lobby;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import client.ClientModel;

import models.IMessage;
import models.Slot;

import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.LobbyUpdatedMessage;
import utilities.msg.messages.UserAvatarMessage;


/**
 * Panel containing the player-image in the player-slot
 *
 */
public class SlotImagePanel extends JPanel {
	private BufferedImage image;
	private Slot slot;
	
	private final int WIDTH=100;
	private final int HEIGHT=100;
	private final int BORDER=10;
	private String username;
	
	
	/**
	 * Creates a SlotImagePanel
	 * @param username - the name of the player
	 * @param slot - the slot for the image
	 */
	public SlotImagePanel(String username,Slot slot) {
		this.slot=slot;
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		this.username = username;
		BufferedImage buffimg = slot.getImage();
		if(buffimg==null) {
			try {
				buffimg=ImageIO.read(SlotPanel.class.getResource("/client/view/graphics/image_resources/newButtons/UnknownPlayer.png"));
			} catch (IOException e) {
				;
			}
		}
		//image=makeRoundImage(buffimg);
		image = buffimg;
	}
	
	//public SlotImagePanel(BufferedImage image,Slot slot) {
	//	this.image=makeRoundImage(image);
	//}
	
	
	/**
	 * Sets the image in the panel
	 * @param image - the image as a buffered image
	 */
	public void setImage(BufferedImage image){
		this.image = image;
	}

	
	/**
	 * Paints a border around the image, indicating if player is ready
	 */
	protected void paintComponent(Graphics g) {
	
		if(slot.isReady()) {
			g.setColor(ClientModel.getTheme().SUCCESS());
		} else {
			g.setColor(ClientModel.getTheme().NOTICE());
		}
		
		g.fillRect(0, 0, WIDTH+BORDER,HEIGHT+BORDER);

		g.drawImage(image,BORDER/2,BORDER/2,null);
	}

}
