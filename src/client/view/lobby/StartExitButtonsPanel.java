package client.view.lobby;

import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import client.ClientModel;
import client.view.graphics.JPlayfuelButton;

import utilities.msg.MessageBus;
import utilities.msg.messages.LobbySlotLeaveMessage;
import utilities.msg.messages.UserLaunchLobbyMessage;
import utilities.msg.messages.UserSetReadyMessage;
import utilities.msg.messages.UserUpdateLobbyMessage;

import models.Lobby;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Panel containing the buttons for exiting the lobby, and launching the game
 *
 */
public class StartExitButtonsPanel extends JPanel {
	private ClientModel cmodel;
	
	
	/**
	 * Creates a StartExitButtonsPanel 
	 * @param model - the client model
	 */
	public StartExitButtonsPanel(ClientModel model){
		this.cmodel=model;
		this.setPreferredSize(new Dimension(519, 50));
		JButton exitButton = new JPlayfuelButton("");
		exitButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				setCursor(new Cursor(12));
			}
		});
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MessageBus.getInstance().pushMessage(new LobbySlotLeaveMessage());
			}
		});
		
		JButton readyButton = new JPlayfuelButton("");
		readyButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				setCursor(new Cursor(12));
			}
		});
		readyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MessageBus.getInstance().pushMessage(new UserSetReadyMessage(!cmodel.getCurrentLobby().getSlot(cmodel.getProfile().getUsername()).isReady()));
			}
		});

		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());

		
		exitButton.setBorderPainted(false);
		exitButton.setPressedIcon(new ImageIcon(StartExitButtonsPanel.class.getResource("/client/view/graphics/image_resources/newButtons/ExitLobbyPressedSmall.png")));
		exitButton.setIcon(new ImageIcon(StartExitButtonsPanel.class.getResource("/client/view/graphics/image_resources/newButtons/ExitLobbySmall.png")));
		
		readyButton.setBorderPainted(false);
		
		if(cmodel.getCurrentLobby().getSlot(cmodel.getProfile().getUsername()).isReady()) {
			readyButton.setIcon(new ImageIcon(StartExitButtonsPanel.class.getResource("/client/view/graphics/image_resources/newButtons/ReadySmall.png")));
			readyButton.setPressedIcon(new ImageIcon(LobbyPanel.class.getResource("/client/view/graphics/image_resources/newButtons/ReadyPressedSmall.png")));
		} else {
			readyButton.setIcon(new ImageIcon(StartExitButtonsPanel.class.getResource("/client/view/graphics/image_resources/newButtons/NotReadySmall.png")));
			readyButton.setPressedIcon(new ImageIcon(LobbyPanel.class.getResource("/client/view/graphics/image_resources/newButtons/NotReadyPressedSmall.png")));			
		}
		GroupLayout gl_buttonsInLobby = new GroupLayout(this);
		gl_buttonsInLobby.setHorizontalGroup(
			gl_buttonsInLobby.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonsInLobby.createSequentialGroup()
					.addContainerGap()
					.addComponent(exitButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 198, Short.MAX_VALUE)
					.addComponent(readyButton, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_buttonsInLobby.setVerticalGroup(
			gl_buttonsInLobby.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_buttonsInLobby.createSequentialGroup()
					.addGap(12)
					.addGroup(gl_buttonsInLobby.createParallelGroup(Alignment.LEADING)
						.addComponent(readyButton, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
						.addComponent(exitButton, GroupLayout.PREFERRED_SIZE, 38, Short.MAX_VALUE)))
		);
		setLayout(gl_buttonsInLobby);
		
		setVisible(true);
	}
}
