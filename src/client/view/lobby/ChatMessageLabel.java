package client.view.lobby;

import javax.swing.JLabel;

import client.ClientModel;
import client.themes.DarkTheme;

/**
 * 
 * @author Oscar
 *
 */

public class ChatMessageLabel extends JLabel {
	public ChatMessageLabel(String username, String message) {
		super();
		
		String userName;
		String chatMsg; 
		
		if(ClientModel.getTheme() instanceof DarkTheme){
			userName = "<html><b><span style= font face=\"helvetica\" color=\"33A1C9\">"+username+": "+"</span></b>";
			chatMsg ="<span style= font face=\"helvetica\" color=\"C8C8C8\">"+message+"</span><br></html>";
		}else{
			userName = "<html><b><span style= font face=\"helvetica\" color=\"33A1C9\">"+username+": "+"</span></b>";
			chatMsg ="<span style= font face=\"helvetica\" color=\"33a1c9ff\">"+message+"</span><br></html>";
		}
		this.setText("" + userName + chatMsg);
	}
}
