package client.view.lobby;

import java.awt.FlowLayout;

import javax.swing.JPanel;

import client.ClientModel;

import models.Slot;

/**
 * Wrapping panel for the player-slots in the lobby 
 *
 */
public class PlayersInLobbyPanel extends JPanel {
	
	
	/**
	 * Creates a PlayersInLobbyPanel
	 * @param model - the client model
	 */
	public PlayersInLobbyPanel(ClientModel model){
		setLayout(new FlowLayout());
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());

		
		for(Slot slot : model.getCurrentLobby().getSlots()) {
			add(new SlotPanel(model, slot));
		}
		
		setVisible(true);
	}

}
