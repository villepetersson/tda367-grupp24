package client.view.lobby;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import models.IMessage;
import models.Lobby;
import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.LobbyChatMessageRecieved;
import utilities.msg.messages.LobbyChatMessageSend;
import utilities.msg.messages.LobbyUpdatedMessage;

import javax.swing.BoxLayout;

import client.ClientModel;
import client.view.graphics.JPlayfuelButton;

/**
 * Panel displaying the chat, in the lobby
 *
 */
public class ChatPanel extends JPanel implements IMessageHandler {
	private JTextField textField;
	private String message;
	private Lobby lobby;
	private ClientModel cmodel;
	private JScrollPane scrollPane;
	private JPanel chatMessagesPane = new JPanel();
	private final JButton btnNewButton = new JPlayfuelButton("");
	private JLabel lblchat;
	
	
	/**
	 * Creates a chatpanel
	 * @param lobby - the lobby for the chat
	 * @param cmodel - the client model
	 */
	public ChatPanel(Lobby lobby, ClientModel cmodel){
		this.lobby = lobby;
		this.cmodel = cmodel;
		
		// Register for events
		MessageBus.getInstance().register(this, LobbyChatMessageRecieved.class);
		
		// Fill with chat messages upon creation
		for(String chatMsg : lobby.getChatMessage()) {
			String[] splitMsg = chatMsg.split(":");
			String username = splitMsg[0];
			String msg = splitMsg[1];
			ChatMessageLabel label = new ChatMessageLabel(username, msg);
			label.setBackground(ClientModel.getTheme().ACCENT_BACKGROUND());
			chatMessagesPane.add(label);
		}
		chatMessagesPane.setBackground(ClientModel.getTheme().ACCENT_BACKGROUND());
		chatMessagesPane.updateUI();
		
		this.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		this.setMaximumSize(new Dimension(12345, 190));
		//setLocation(700, 0);
		textField = new JTextField();
		textField.setBorder(new LineBorder(ClientModel.getTheme().TEXTBOX_BORDER(), 2));
		textField.setSelectedTextColor(Color.WHITE);
		textField.setSelectionColor(Color.GRAY);
		textField.setFont(new Font("Helvetica", Font.PLAIN, 12));
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().length()>0){	
					message = textField.getText();
					textField.setText("");
					sendMessage();
				}
			}
		});
		textField.setColumns(10);
		btnNewButton.setBorderPainted(false);
		btnNewButton.setPressedIcon(new ImageIcon(ChatPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SendPressedSmaller.png")));
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				btnNewButton.setIcon(new ImageIcon(ChatPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SendPressedSmaller.png")));
			}
			public void mouseReleased(MouseEvent arg0){
				btnNewButton.setIcon(new ImageIcon(ChatPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SendSmaller.png")));
			}
			
			public void mouseEntered(MouseEvent arg0){
				setCursor(new Cursor(12));
			}
			
			public void mouseExited(MouseEvent arg0){
				setCursor(new Cursor(0));
			}
		});
		btnNewButton.setIcon(new ImageIcon(ChatPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SendSmaller.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(textField.getText().length()>0){
					message = textField.getText();
					textField.setText("");
					sendMessage();
				}
			}
		});
		
		scrollPane = new JScrollPane();
		scrollPane.setAutoscrolls(true);
		scrollPane.setBorder(new LineBorder(ClientModel.getTheme().TEXTBOX_BORDER(), 2));
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(ClientModel.getTheme().LOGOMINI());
		
		lblchat = new JLabel("Chat");
		lblchat.setFont(new Font("Helvetica", Font.PLAIN, 11));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblNewLabel_1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblchat))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(textField, GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE)
							.addGap(4)
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addGap(12))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_1)
						.addComponent(lblchat))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(textField)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 29, Short.MAX_VALUE))
					.addContainerGap(110, Short.MAX_VALUE))
		);

		chatMessagesPane.setAutoscrolls(true);
		chatMessagesPane.setRequestFocusEnabled(false);
		scrollPane.setViewportView(chatMessagesPane);
		this.setLayout(groupLayout);
		
		scrollPane.setPreferredSize(new Dimension(400,  140));
		chatMessagesPane.setPreferredSize(new Dimension(400,  125));
		chatMessagesPane.setLayout(new BoxLayout(chatMessagesPane, BoxLayout.Y_AXIS));
		this.setVisible(true);
		
	}
	
	@Override
	public void onMessage(IMessage msg) {
		if(msg instanceof LobbyChatMessageRecieved){
			LobbyChatMessageRecieved recievedMsg = (LobbyChatMessageRecieved) msg;			
			chatMessagesPane.add(new ChatMessageLabel(recievedMsg.username, recievedMsg.message));
			chatMessagesPane.updateUI();
			
		// If lobby updates, show all messages
		} 
		
	}
	
	/**
	 * Sends a chatmessage from the panel 
	 */
	public void sendMessage(){	
		MessageBus.getInstance().pushMessage(new LobbyChatMessageSend(message, cmodel.getProfile().getUsername(),lobby.getID()));
	}

	/**
	 * assigns a lobby to the chatpanel
	 * @param lobby - the lobby
	 */
	public void setLobby(Lobby lobby) {
		this.lobby = lobby;		
	}
	
}
