package client.view.graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

import client.ClientModel;

/**
 * Floating panel that displays the logo in the upper right corner 
 *
 *
 */
public class LogoPanel extends JPanel {
	
	
	/**
	 * Creates a new LogoPanel
	 */
	public LogoPanel(){
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		setOpaque(false);
		JLabel logoLabel = new JLabel("");
		logoLabel.setIcon(ClientModel.getTheme().LOGOSMALL());
		add(logoLabel);
		
	}
}
