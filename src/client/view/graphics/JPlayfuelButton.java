package client.view.graphics;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

/**
 * JButton subclass to make all buttons look alike in Playfuel.
 * @see JButton
 */
public class JPlayfuelButton extends JButton {
	
	

	public JPlayfuelButton() {
		super();
		this.setContentAreaFilled(false);
	}

	public JPlayfuelButton(Action a) {
		super(a);
		this.setContentAreaFilled(false);
	}

	public JPlayfuelButton(Icon icon) {
		super(icon);
		this.setContentAreaFilled(false);
	}

	public JPlayfuelButton(String text, Icon icon) {
		super(text, icon);
		this.setContentAreaFilled(false);
	}

	public JPlayfuelButton(String name) {
		super(name);
		this.setContentAreaFilled(false);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2195396445538557997L;

}
