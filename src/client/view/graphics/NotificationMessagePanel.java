package client.view.graphics;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JLabel;
import javax.swing.JPanel;

import client.ClientModel;

import utilities.msg.messages.ClientMessageMessage;


/**
 * Panel that fades in from the bottom of the client with a notification message
 *
 */
public class NotificationMessagePanel extends JPanel {
	
	private float opacity = 0.0f;
	private Color bg;
	private Color fg;

	/**
	 * Creates a NotificationMessagePanel
	 */
	public NotificationMessagePanel(ClientMessageMessage cm) {
		
		// Color code the message
		if (cm.status.equals(ClientMessageMessage.Status.SUCCESS)) {
			bg = ClientModel.getTheme().SUCCESS();
			fg = ClientModel.getTheme().SUCCESS_INVERSE();
		} else if (cm.status.equals(ClientMessageMessage.Status.FAIL)) {
			bg = ClientModel.getTheme().FAIL();
			fg = ClientModel.getTheme().FAIL_INVERSE();
		} else {
			bg = ClientModel.getTheme().NOTICE();
			fg = ClientModel.getTheme().NOTICE_INVERSE();
		}
		
		this.setBackground(bg);

		// The actual content of the message
		JLabel jl = new JLabel(cm.message);
		jl.setForeground(fg);
		add(jl);
		
		new Thread(){
			public void run() {
				// First sleep before showing the message
				try {
					Thread.sleep(100);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				// Fade in animation
				while(opacity < 1.0f) {
				
					try {
						// Animation frequency
						Thread.sleep(10);

						repaint();
						
						// Increase the opacity for every step
						opacity += 0.01;
						
						// Make sure opacity never reached full
						if (opacity > 1.0f) {
							opacity = 1.0f;
						}
						
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				// This is the message shown duration
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				
				// Fade out animation
				while(opacity > 0.0f) {
					
					try {
						// Animation frequency
						Thread.sleep(50);
						
						repaint();
						
						// Increase the opacity for every step
						opacity -= 0.06;
						
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	@Override
	public void paint(Graphics g) {	
		// Make sure opacity is not negative
		if (opacity < 0.0f) {
			this.getParent().remove(this);
			return;
		}
		
		Graphics2D g2D = (Graphics2D) g.create(); 
		g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity));
		g2D.setBackground(bg);
		super.paint(g2D); 
		g2D.dispose();
		
		// To force repaint for fade out animation
		this.getParent().repaint();
	}
}
