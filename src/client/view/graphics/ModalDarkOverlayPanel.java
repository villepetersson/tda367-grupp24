package client.view.graphics;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

/**
 * Panel for the dark background when displaying a modal panel 
 *
 *
 */
public class ModalDarkOverlayPanel extends JPanel {
	
	private float opacity = 0.0f;

	/**
	 * Creates the ModalDarkOverlayPanel.
	 */
	public ModalDarkOverlayPanel(float opacity) {
		this.opacity = opacity;
		setOpaque(false);
		repaint();
	}
	
	@Override
	public void paint(Graphics g) {	
		Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity));
        g2d.setColor(Color.BLACK);
        g2d.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
	}

}
