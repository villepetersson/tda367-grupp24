package client.view.modals;

import java.awt.BorderLayout;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.ClientModel;

import utilities.msg.messages.MatchResultsMessage;

/**
 * Panels for displaying the results and new rankings after game 
 *
 *
 */
public class ResultsPanel extends JPanel {
	
	private JPanel inScroll = new JPanel();
	private Map<String, Double> preResults;
	private Map<String, Double> resultsDiff;
	

	/**
	 * Creates a new ResultsPanel
	 * @param message
	 */
	public ResultsPanel(MatchResultsMessage message){
		this.preResults = message.preResults;
		this.resultsDiff = message.resultsDiff;
		this.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());

	
		this.setLayout(new BorderLayout());
		JPanel innerPanel = new JPanel();
		innerPanel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		innerPanel.setLayout(new BoxLayout(innerPanel, BoxLayout.Y_AXIS));
		
		//formatting for label with html
		JLabel lblNewLabel = new JLabel("<html>&nbsp&nbsp<u>Spelare</u>	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp	<u>New Ranking</u>	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp	<u>Difference</u></html>");
		innerPanel.add(lblNewLabel);
		JLabel label2 = new JLabel("    ");
		innerPanel.add(label2);
		
		
		//search for matching player (keys) in the two maps, and then displays the values in a label
		for(Map.Entry<String, Double> entryPre : preResults.entrySet() ){
			String user = entryPre.getKey();
			double preScore = entryPre.getValue();
			
			for(Map.Entry<String, Double> entryDiff : resultsDiff.entrySet() ){
				String user2 = entryDiff.getKey();
				double diffScore = entryDiff.getValue();
				
				if(user.equals(user2)){
					double newRank = preScore + diffScore;
					preScore = Math.round(preScore);
					diffScore = Math.round(diffScore);
					newRank = Math.round(newRank);
					
					int preScoreInt = (int)preScore;
					int diffScoreInt = (int)diffScore;
					int newRankInt = (int)newRank;
					
					innerPanel.add(new JLabel("  " + user + "                  " + newRankInt + "                    " + diffScoreInt ));
				}
				
			}
			
			
			
			
		}		
		this.add(innerPanel, BorderLayout.CENTER);
		this.setVisible(true);
	}
	
	
	
	
}
