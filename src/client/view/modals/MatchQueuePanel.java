package client.view.modals;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;

import client.ClientModel;
import client.messages.ClientModalDisposeAllMessage;
import client.view.graphics.JPlayfuelButton;

import utilities.msg.MessageBus;
import utilities.msg.messages.UserExitQueueMessage;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Modal panel informing the user that he/she is being matched with other players
 *
 */
public class MatchQueuePanel extends JPanel implements MouseListener {
	public String game;
	public JButton btnNewButton;
	
	/**
	 * Creates a new MatchQueuePanel
	 * @param string - the game user searches for players in
	 */
	public MatchQueuePanel(String string) {
		game = string;
		this.setVisible(true);
		this.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());

		
		JLabel lblWaitingToJoin = new JLabel("<html>Searching for players<br> &nbsp&nbsp with similar rank...</html>");
		lblWaitingToJoin.setIcon(null);
		lblWaitingToJoin.setHorizontalTextPosition(SwingConstants.CENTER);
		lblWaitingToJoin.setHorizontalAlignment(SwingConstants.CENTER);
		lblWaitingToJoin.setFont(ClientModel.getTheme().HEADER_FONT());
		lblWaitingToJoin.setForeground(ClientModel.getTheme().BODY_COLOR());
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(ClientModel.getTheme().LOGOMINI());
		
		btnNewButton = new JPlayfuelButton("");
		btnNewButton.setBorderPainted(false);
		btnNewButton.addMouseListener(this);
		btnNewButton.setIcon(new ImageIcon(MatchQueuePanel.class.getResource("/client/view/graphics/image_resources/newButtons/ExitSmallSmall.png")));
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MessageBus.getInstance().pushMessage(new ClientModalDisposeAllMessage());
				MessageBus.getInstance().pushMessage(new UserExitQueueMessage(game));
				
			}
		});
		
		JLabel label = new JLabel("");
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setAlignmentY(0.0f);
		lblNewLabel_1.setIcon(ClientModel.getTheme().LOADINGIMAGE());
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(58)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(6)
									.addComponent(lblWaitingToJoin))
								.addComponent(label))))
					.addContainerGap(65, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(174, Short.MAX_VALUE)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(125)
					.addComponent(lblNewLabel_1)
					.addContainerGap(127, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel)
					.addGap(101)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(label)
						.addComponent(lblWaitingToJoin, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNewLabel_1)
					.addPreferredGap(ComponentPlacement.RELATED, 84, Short.MAX_VALUE)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		setLayout(groupLayout);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {}
	@Override
	public void mouseEntered(MouseEvent arg0) {}
	@Override
	public void mouseExited(MouseEvent arg0) {}
	@Override
	public void mousePressed(MouseEvent arg0) {
		btnNewButton.setIcon(new ImageIcon(MatchQueuePanel.class.getResource("/client/view/graphics/image_resources/newButtons/ExitSmallPressedSmall.png")));
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		btnNewButton.setIcon(new ImageIcon(MatchQueuePanel.class.getResource("/client/view/graphics/image_resources/newButtons/ExitSmallSmall.png")));
	}
}
