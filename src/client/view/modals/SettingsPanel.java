package client.view.modals;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.plaf.SpinnerUI;

import client.ClientModel;
import client.messages.ClientModalDisposeAllMessage;
import client.view.graphics.JPlayfuelButton;

import models.Setting;
import models.Setting.NotSetInManifestException;
import models.Slot;
import utilities.msg.MessageBus;
import utilities.msg.messages.LobbySettingsUpdateMessage;
import utilities.msg.messages.LobbySlotSettingsUpdateMessage;

@SuppressWarnings("serial")

/*
 * Modal panel for displaying the custom settings for the individual game, generates appropriate components dynamically.
 */
public class SettingsPanel extends JPanel implements ActionListener {	

		
	
		// ### DEFINE THE COMPONENTS FOR DIFFERENT INPUTS ###
		public abstract class SettingsComponent extends JPanel {
			protected Setting setting;
			protected Object defaultvalue;
			public SettingsComponent(Setting setting) {
				super();
				this.setting=setting;
				
				setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
				
				if(perPlayer()) {
					this.defaultvalue=slot.getSettings().get(setting);

				} else {
					this.defaultvalue=model.getCurrentLobby().getGlobalSettings().get(setting);
				}
				
				JLabel label = new JLabel(setting.getPrettyname() + ": ");
				label.setForeground(ClientModel.getTheme().BODY_COLOR());
				this.add(label);
				this.add(initComponent());
				
				this.setMaximumSize(new Dimension(300, 70));
			}

			public abstract JComponent initComponent();
			public abstract Object componentValue();
		}

		public class IntegerComponent extends SettingsComponent {
			public IntegerComponent(Setting setting) {
				super(setting);
			}

			private JSpinner spinner;

			@Override
			public JComponent initComponent() {
				if(editable) {
					spinner=new JSpinner();
					if(defaultvalue instanceof Integer) {
						spinner.setValue((Integer)defaultvalue);
					} else if (defaultvalue instanceof String) {
						spinner.setValue(new Integer(((String)defaultvalue)).intValue());
					}
					
					// Adhere to theme!
					for(Component comp : spinner.getComponents()) {
						comp.setBackground(ClientModel.getTheme().ACCENT_BACKGROUND());
					}
					for(Component comp : spinner.getEditor().getComponents()) {
						comp.setBackground(ClientModel.getTheme().ACCENT_BACKGROUND());
						comp.setForeground(ClientModel.getTheme().BODY_COLOR());
					}
					return spinner;
				} else {
					return new JLabel(""+defaultvalue);
				}
			}

			@Override
			public Object componentValue() {
				return spinner.getValue();
			}
			
		}

		public class ChoiceComponent extends SettingsComponent {
			public ChoiceComponent(Setting setting) {
				super(setting);
			}

			private JComboBox combo;

			@Override
			public JComponent initComponent() {
				if(editable) {
					try {
						combo = new JComboBox(setting.getValues());
						if(defaultvalue instanceof Integer) {
							combo.setSelectedIndex((Integer)defaultvalue);
						} else if (defaultvalue instanceof String) {
							combo.setSelectedIndex(new Integer(((String)defaultvalue)).intValue());
						}
					} catch (NotSetInManifestException e) {
						combo = new JComboBox();
					}
					return combo;
				} else {
					try {
						return new JLabel(setting.getValues()[new Integer((String)defaultvalue.toString())]);
					} catch (Exception e) {
						try {
							return new JLabel(setting.getValues()[0]);
						} catch (NotSetInManifestException e1) {
							return new JLabel("Default");
						}
					}
				}
			}

			@Override
			public Object componentValue() {
				return combo.getSelectedIndex();
			}
			
		}
		
		public class StringComponent extends SettingsComponent {
			private JTextField textField;
			public StringComponent(Setting setting) {
				super(setting);
			}

			@Override
			public JComponent initComponent() {
				this.textField=new JTextField(defaultvalue.toString());
				textField.setEditable(editable);
				return textField;
			}

			@Override
			public Object componentValue() {
				return textField.getText();
			}
			
		}
		
		public class BooleanComponent extends SettingsComponent {
			private JCheckBox checkbox;
			public BooleanComponent(Setting setting) {
				super(setting);
			}
			@Override
			public JComponent initComponent() {
				this.checkbox=new JCheckBox();
				this.checkbox.setSelected(defaultvalue.equals(true) || defaultvalue.equals("TRUE"));
				checkbox.setEnabled(editable);
				return checkbox;
			}
			@Override
			public Object componentValue() {
				return checkbox.isSelected();
			}
			
		}
		// ### END COMPONENT DEFINITIONS ###
	
	
	List<SettingsComponent> guisettings = new ArrayList<SettingsComponent>();
	private ClientModel model;
	private Slot slot=null;
	private boolean editable;
	public SettingsPanel(ClientModel model, boolean editable) {
		this(model,null, editable);
	}
	
	/**
	 * @wbp.parser.constructor
	 */
	public SettingsPanel(ClientModel model,Slot slot, boolean editable) {
		super();
		
		this.model=model;
		this.slot = slot;
		this.editable=editable;
		//JPanel panel = new JPanel();
		
		Box panel = new Box(BoxLayout.Y_AXIS);
		panel.setOpaque(false);

		//panel.setLayout(new FlowLayout());
		
		List<Setting> settingsfactory;
		if(perPlayer()) {
			settingsfactory = model.getCurrentLobby().getGame().getSlotSettingsFactory();
		} else {
			settingsfactory = model.getCurrentLobby().getGame().getSettingsFactory();
		}
		for(Setting setting : settingsfactory) {
			try {
				panel.add(constructComponentForSetting(setting));
			} catch (IllegalArgumentException e) {
				System.out.println("Invalid input type "+setting.getInput());
			}
		}

		
		setVisible(true);
		
		JButton saveSettings = new JPlayfuelButton("Save settings");
		saveSettings.setBorderPainted(false);
		saveSettings.setCursor(new Cursor(Cursor.HAND_CURSOR));
		saveSettings.setIcon(new ImageIcon(SettingsPanel.class.getResource("/client/view/graphics/image_resources/newButtons/AcceptFriendSmall.png")));
		saveSettings.setPressedIcon(new ImageIcon(SettingsPanel.class.getResource("/client/view/graphics/image_resources/newButtons/AcceptFriendPressedSmall.png")));
		saveSettings.setText("");
		saveSettings.setToolTipText("");
		saveSettings.setActionCommand("save");
		
		saveSettings.setVisible(editable);
		
		saveSettings.addActionListener(this);
		JButton cancel = new JPlayfuelButton("Cancel");
		cancel.setPressedIcon(new ImageIcon(SettingsPanel.class.getResource("/client/view/graphics/image_resources/newButtons/RemoveFriendPressedSmall.png")));
		cancel.setIcon(new ImageIcon(SettingsPanel.class.getResource("/client/view/graphics/image_resources/newButtons/RemoveFriendSmall.png")));
		cancel.setText("");
		cancel.setBorderPainted(false);
		cancel.setCursor(new Cursor(Cursor.HAND_CURSOR));
		
		cancel.setVisible(editable);
		
		cancel.setActionCommand("cancel");
		cancel.addActionListener(this);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(cancel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
							.addComponent(saveSettings, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(51)
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 193, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addGap(16)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(cancel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(saveSettings, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		setLayout(groupLayout);
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		//pack();

		repaint();
	}
	
	public void actionPerformed(ActionEvent e){
		String command = e.getActionCommand();
		if (command.equals("save")) {
			
			Map<Setting,Object> settings = new HashMap<Setting,Object>();
			
			for(SettingsComponent s : guisettings){
			 	settings.put(s.setting,s.componentValue());	
			}
			
			if (perPlayer()) {
				MessageBus.getInstance().pushMessage(new LobbySlotSettingsUpdateMessage(settings));
			} else {
				MessageBus.getInstance().pushMessage(new LobbySettingsUpdateMessage(settings));
			}
			this.setVisible(false);
			MessageBus.getInstance().pushMessage(new ClientModalDisposeAllMessage());

		} else if(command.equals("cancel")) {
			this.setVisible(false);
			MessageBus.getInstance().pushMessage(new ClientModalDisposeAllMessage());
		}
		
	}
	
	private JPanel constructComponentForSetting(final Setting setting) {
		SettingsComponent guisetting;
		
		// In the future, might have a MultipleChoice
		if(setting.getInput().equals("Integer")) {
			guisetting = new IntegerComponent(setting);
		} else 
		if (setting.getInput().equals("String")) {
			guisetting = new StringComponent(setting);
		} else
		if (setting.getInput().equals("Bolean")) {
			guisetting = new BooleanComponent(setting);
		} else 
		if (setting.getInput().equals("Choice")) {
			guisetting = new ChoiceComponent(setting);
		} else 
			{
		// (setting.getInput()==Boolean.class) {
			throw new IllegalArgumentException();
		}
		guisettings.add(guisetting);
		
		guisetting.setVisible(true);
		return guisetting;
	}
	
	private boolean perPlayer(){
		return slot!=null;
	}
}
