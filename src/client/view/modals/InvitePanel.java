package client.view.modals;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;

import utilities.msg.MessageBus;
import utilities.msg.messages.LobbySlotInviteAcceptMessage;
import utilities.msg.messages.LobbySlotInviteDeclineMessage;
import client.ClientModel;
import client.messages.ClientModalDisposeAllMessage;
import client.view.ClientFrame;
import client.view.graphics.JPlayfuelButton;

import java.awt.Font;



/**
 * Modal panel accepting invite to a lobby
 *
 */
public class InvitePanel extends JPanel {
	private String gameName;
	
	/**
	 * Creates a new InvitePanel
	 */
	public InvitePanel(String gameName, final int lobbyId, final int slotId){
		this.gameName = gameName;
		this.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		JLabel lblNewLabel = new JLabel("<html><p style=\"width:220px\">"+"You have been invited to play:</p><html>");
		JLabel gameNameLbl = new JLabel(gameName);
		gameNameLbl.setFont(ClientModel.getTheme().HEADER_FONT());

		JLabel lblNewLabel_1 = new JLabel("<html><p style=\"width:200px\">" +"Click accept to join."+"</p><html>");
		lblNewLabel_1.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		//lblNewLabel_1.setBorder(new LineBorder(Theme.TEXTBOX_BORDER, 5));
		
		lblNewLabel.setFont(ClientModel.getTheme().HEADER_FONT());
		lblNewLabel_1.setFont(ClientModel.getTheme().BODY_FONT());
		lblNewLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		lblNewLabel_1.setForeground(ClientModel.getTheme().BODY_COLOR());
		lblNewLabel_1.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		JButton inviteAcceptBtn = new JPlayfuelButton("");
		inviteAcceptBtn.setIcon(new ImageIcon(ClientFrame.class.getResource("/client/view/graphics/image_resources/newButtons/AcceptFriendSmall.png")));
		inviteAcceptBtn.setPressedIcon(new ImageIcon(ClientFrame.class.getResource("/client/view/graphics/image_resources/newButtons/AcceptFriendPressedSmall.png")));
		inviteAcceptBtn.setBorderPainted(false);
		JButton inviteDeclineBtn = new JPlayfuelButton("");
		inviteDeclineBtn.setBorderPainted(false);
		inviteDeclineBtn.setPressedIcon(new ImageIcon(ClientFrame.class.getResource("/client/view/graphics/image_resources/newButtons/RemoveFriendPressedSmall.png")));
		inviteDeclineBtn.setIcon(new ImageIcon(ClientFrame.class.getResource("/client/view/graphics/image_resources/newButtons/RemoveFriendSmall.png")));
		
		inviteAcceptBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MessageBus.getInstance().pushMessage(new LobbySlotInviteAcceptMessage(lobbyId, slotId));
				// Then dispose the modal
				MessageBus.getInstance().pushMessage(new ClientModalDisposeAllMessage());
			}
		});
		
		inviteDeclineBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MessageBus.getInstance().pushMessage(new LobbySlotInviteDeclineMessage(lobbyId, slotId));
				// Then dispose the modal
				MessageBus.getInstance().pushMessage(new ClientModalDisposeAllMessage());
			}
		});
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(InvitePanel.class.getResource("/client/view/graphics/image_resources/logoSmall.png")));
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(inviteDeclineBtn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 198, Short.MAX_VALUE)
					.addComponent(inviteAcceptBtn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(label))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(24)
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 237, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(39, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(102)
					.addComponent(gameNameLbl)
					.addContainerGap(125, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(76)
					.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(88, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(label)
					.addGap(18)
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(gameNameLbl)
					.addGap(60)
					.addComponent(lblNewLabel_1)
					.addPreferredGap(ComponentPlacement.RELATED, 197, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(inviteDeclineBtn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(inviteAcceptBtn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		setLayout(groupLayout);
		
		
	}
}
