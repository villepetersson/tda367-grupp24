package client.view.modals;

import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import client.ClientModel;

/**
 * List item for modal that shows friends to invite
 * @author Oscar
 *
 */
public class FriendInviteListItemPanel extends JPanel {
	JButton btnNewButton;
	public JLabel lblNewLabel; 
	
	/**
	 * Creates a new FriendInviteListItemPanel
	 * @param label
	 * @param button
	 */
	public FriendInviteListItemPanel(JLabel label, JButton button){
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		lblNewLabel = label;
		btnNewButton = button;
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(40)
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
					.addComponent(btnNewButton))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
					.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
		);
		setLayout(groupLayout);
		
		
			
	}

}
