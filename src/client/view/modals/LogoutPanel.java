package client.view.modals;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.LineBorder;

import client.ClientModel;
/**
 * Modal panel informing user why he was logged out
 *
 */
public class LogoutPanel extends JPanel {

	
	/**
	 * Creates a new LogoutPanel
	 * @param reason - String explaining why user was logged out
	 */
	public LogoutPanel(String reason){
		
		JLabel lblNewLabel = new JLabel("<html><p style=\"width:180px\">"+reason+"</p><html>");
		lblNewLabel.setFont(ClientModel.getTheme().BODY_FONT());
		lblNewLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		this.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		this.setBorder(new LineBorder(new Color(237, 41, 57), 2));
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(LogoutPanel.class.getResource("/client/view/graphics/image_resources/logoSmall.png")));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(label))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(55)
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 297, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(94, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(label)
					.addGap(54)
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(112, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
		
	}
	
}
