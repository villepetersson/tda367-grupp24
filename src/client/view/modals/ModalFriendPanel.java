package client.view.modals;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import models.Friend;
import models.Slot;
import utilities.msg.MessageBus;
import utilities.msg.messages.ClientMessageMessage;
import utilities.msg.messages.LobbySlotInviteMessage;
import client.ClientModel;
import client.messages.ClientModalDisposeAllMessage;
import client.view.graphics.JPlayfuelButton;


/**
 * Modal for choosing friend to invite to lobby 
 *
 *
 */
public class ModalFriendPanel extends JScrollPane {	
	private List<Friend> list;	
	private Slot localslot;
	
	/**
	 * Creates a ModalFriendPanel
	 * @param list - the friendlist
	 * @param slot - the slot to invite to
	 */
	public ModalFriendPanel(List<Friend> list, Slot slot){
		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		this.list = list;
		localslot = slot;
		this.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
				
		JPanel panel_1 = new JPanel();
		setViewportView(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		
		
		if(list.size() == 0){
			JLabel noFriends = new JLabel("Your have no friends to invite!");
			panel_1.setLayout(new BorderLayout());
			panel_1.add(noFriends, BorderLayout.CENTER);
		}
		
		for(final Friend f : list) {
			JLabel opponent = new JLabel(f.getUsername());
			opponent.setFont(new Font("Helvetica", Font.ITALIC, 16));	
			opponent.setForeground(f.isOnline() ? ClientModel.getTheme().ONLINE() : ClientModel.getTheme().OFFLINE());
			opponent.setForeground(f.isBusy() ? ClientModel.getTheme().BUSY() : opponent.getForeground());
			
			JButton button = new JPlayfuelButton("Invite");
			button.setIcon(new ImageIcon(ModalFriendPanel.class.getResource("/client/view/graphics/image_resources/newButtons/InviteSmall.png")));
			button.setPressedIcon(new ImageIcon(ModalFriendPanel.class.getResource("/client/view/graphics/image_resources/newButtons/InvitePressedSmall.png")));
			button.setBorderPainted(false);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					JButton button = (JButton) arg0.getSource();
					FriendInviteListItemPanel panel = (FriendInviteListItemPanel) button.getParent();
					JLabel src = (JLabel) panel.lblNewLabel;
					if (src.getForeground().equals(ClientModel.getTheme().BUSY())) {
						MessageBus.getInstance().pushMessage(new ClientMessageMessage(src.getText() + " is currently in a Lobby and cant be invited", ClientMessageMessage.Status.FAIL));
					} else if (src.getForeground().equals(ClientModel.getTheme().OFFLINE())) {
						MessageBus.getInstance().pushMessage(new ClientMessageMessage(src.getText() + " is offline and cannot be invited", ClientMessageMessage.Status.FAIL));
					} else {
						MessageBus.getInstance().pushMessage(new LobbySlotInviteMessage(f.getUsername(), localslot.hashCode()));
						MessageBus.getInstance().pushMessage(new ClientModalDisposeAllMessage());
					}
				}
			});
			
			FriendInviteListItemPanel panel = new FriendInviteListItemPanel(opponent, button);
			panel_1.add(panel);	
		}
		
	}
	
}
