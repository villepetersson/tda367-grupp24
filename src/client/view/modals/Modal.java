package client.view.modals;

import javax.swing.JComponent;

/**
 * Wrapper class for modals, with content and header
 *
 */
public class Modal {

	private String header;
	private JComponent content;

	/**
	 * Actions performed on modal dispose
	 */
	public void onDispose() {}

	/**
	 * Returns the modal header
	 * @return - the header
	 */
	public String getHeader() {
		return this.header;
	}
	
	
	/**
	 * Sets the header of the modal
	 * @param header - the header
	 */
	public void setHeader(String header) {
		this.header = header;
	}

	
	
	/**
	 * gets the content component
	 * @return - the component with content
	 */
	public JComponent getContent() {
		return this.content;
	}
	
	
	/**
	 * Sets the content in the modal
	 * @param content - content
	 */
	public void setContent(JComponent content) {
		this.content = content;
	}
}
