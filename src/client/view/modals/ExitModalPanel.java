package client.view.modals;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Cursor;

import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

import client.ClientModel;
import client.messages.ClientModalDisposeAllMessage;
import client.view.graphics.JPlayfuelButton;

import utilities.msg.MessageBus;


/**
 * Modal panel confirming user signout of client
 *
 */
public class ExitModalPanel extends JPanel {

	
	/**
	 * Creates a new ExitModalPanel
	 */
	public ExitModalPanel(){
		this.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		JLabel lblAreYouSure = new JLabel("Are you sure you want to quit?");
		lblAreYouSure.setHorizontalTextPosition(SwingConstants.CENTER);
		lblAreYouSure.setFont(ClientModel.getTheme().BODY_FONT());
		lblAreYouSure.setForeground(ClientModel.getTheme().BODY_COLOR());
		
		JPlayfuelButton btnNewButton = new JPlayfuelButton("");
		btnNewButton.setBorderPainted(false);
		btnNewButton.setIcon(new ImageIcon(ExitModalPanel.class.getResource("/client/view/graphics/image_resources/newButtons/RemoveFriendSmall.png")));
		btnNewButton.setPressedIcon(new ImageIcon(ExitModalPanel.class.getResource("/client/view/graphics/image_resources/newButtons/RemoveFriendPressedSmall.png")));
		btnNewButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MessageBus.getInstance().pushMessage(new ClientModalDisposeAllMessage());
			}
		});
		
		JPlayfuelButton button = new JPlayfuelButton("");
		button.setBorderPainted(false);
		button.setIcon(new ImageIcon(ExitModalPanel.class.getResource("/client/view/graphics/image_resources/newButtons/AcceptFriendSmall.png")));
		button.setPressedIcon(new ImageIcon(ExitModalPanel.class.getResource("/client/view/graphics/image_resources/newButtons/AcceptFriendPressedSmall.png")));
		button.setCursor(new Cursor(Cursor.HAND_CURSOR));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		JLabel label = new JLabel("");
		label.setIcon(ClientModel.getTheme().LOGOMINI());
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(label))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(53)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(button, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblAreYouSure, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(45, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(label)
					.addGap(53)
					.addComponent(lblAreYouSure)
					.addPreferredGap(ComponentPlacement.RELATED, 119, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(button, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(46))
		);
		setLayout(groupLayout);
	}
}
