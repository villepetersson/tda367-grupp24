package client.view.login;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import models.IMessage;

import client.ClientModel;
import client.messages.ClientCardSwitchMessage;
import client.view.graphics.JPlayfuelButton;

import utilities.*;
import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.UserRegisterFailedMessage;
import utilities.msg.messages.UserRegisterMessage;
import utilities.msg.messages.UserRegisterSuccessMessage;

import java.util.Arrays;
import java.util.List;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;


/**
 * Panel containing the textboxes for entering user credentials for a new user
 *
 */
public class LoginSignupPanel extends JPanel implements ActionListener, IMessageHandler {
	private JTextField usernameTextField;
	private JPasswordField repeatPasswordField;
	private JLabel usernameLabel;
	private JLabel repeatPasswordLabel;
	private JButton createAccountButton;
	private JPasswordField passwordField;
	private JLabel passwordLabel;
	private JLabel emailLabel;
	private JTextField emailTextField;
	
	private JLabel lblUsernameError;
	private JLabel lblPasswordError;
	private JLabel lblPasswordConfirmError;
	private JLabel lblEmailError;
	
	private Border redline = BorderFactory.createLineBorder(new Color(226,74,51), 2);
	private JLabel lblNewLabel;
	
	
	/**
	 * Creates a LoginSignUpPanel
	 */
	public LoginSignupPanel() {		
		MessageBus.getInstance().register(this, UserRegisterSuccessMessage.class);
		MessageBus.getInstance().register(this, UserRegisterFailedMessage.class);

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent arg0) {
				usernameTextField.requestFocusInWindow();
			}
		});
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		usernameTextField = new JTextField();
		usernameTextField.setActionCommand("created");
		usernameTextField.setColumns(10);
		usernameTextField.addActionListener(this);
		
		repeatPasswordField = new JPasswordField();
		repeatPasswordField.setActionCommand("created");
		repeatPasswordField.setColumns(10);
		repeatPasswordField.addActionListener(this);
		
		JLabel logo = new JLabel("");
		logo.setIcon(new ImageIcon(LoginPanel.class.getResource("/client/view/graphics/image_resources/PlayfuelLogo.png")));
		
		usernameLabel = new JLabel("Username");
		usernameLabel.setFont(new Font("Helvetica", Font.PLAIN, 13));
		usernameLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		
		repeatPasswordLabel = new JLabel("Re-enter password");
		repeatPasswordLabel.setFont(new Font("Helvetica", Font.PLAIN, 13));
		repeatPasswordLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		
		createAccountButton = new JPlayfuelButton("");
		createAccountButton.setActionCommand("created");
		createAccountButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				createAccountButton.setCursor(new Cursor(12));
			}
		});

		
		createAccountButton.setPressedIcon(new ImageIcon(LoginSignupPanel.class.getResource("/client/view/graphics/image_resources/newButtons/CreateAccountPressedSmall.png")));
		createAccountButton.setSelectedIcon(new ImageIcon(LoginSignupPanel.class.getResource("/client/view/graphics/image_resources/CreateAccountShade.png")));
		createAccountButton.setBorderPainted(false);
		createAccountButton.setIcon(new ImageIcon(LoginSignupPanel.class.getResource("/client/view/graphics/image_resources/newButtons/CreateAccountSmall.png")));
		
		createAccountButton.addActionListener(this);
		
		passwordField = new JPasswordField();
		passwordField.setActionCommand("created");
		passwordField.setColumns(10);
		passwordField.addActionListener(this);
		
		passwordLabel = new JLabel("Password");
		passwordLabel.setFont(new Font("Helvetica", Font.PLAIN, 13));
		passwordLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		
		emailLabel = new JLabel("Email");
		emailLabel.setFont(new Font("Helvetica", Font.PLAIN, 13));
		emailLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		
		
		
		emailTextField = new JTextField();
		emailTextField.setActionCommand("created");
		emailTextField.setColumns(10);
		emailTextField.addActionListener(this);
		
		lblUsernameError = new JLabel();
		lblPasswordError = new JLabel();
		lblPasswordConfirmError = new JLabel();
		lblEmailError = new JLabel();
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		lblNewLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				lblNewLabel.setIcon(new ImageIcon(LoginSignupPanel.class.getResource("/client/view/graphics/image_resources/backArrow35x35shade.png")));
			}
			
			public void mouseReleased(MouseEvent arg0) {
				lblNewLabel.setIcon(new ImageIcon(LoginSignupPanel.class.getResource("/client/view/graphics/image_resources/backArrow35x35.png")));
			}
			
			public void mouseClicked(MouseEvent arg0) {
				MessageBus.getInstance().pushMessage(new ClientCardSwitchMessage("login"));
			}
		});
		lblNewLabel.setIcon(new ImageIcon(LoginSignupPanel.class.getResource("/client/view/graphics/image_resources/backArrow35x35.png")));
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(25)
							.addComponent(logo))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(21)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(emailLabel)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(emailTextField, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(passwordLabel)
										.addComponent(usernameLabel)
										.addComponent(repeatPasswordLabel))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(repeatPasswordField)
										.addComponent(usernameTextField)
										.addComponent(passwordField, GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addContainerGap()
									.addComponent(lblNewLabel)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(lblEmailError, GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE))
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
									.addGroup(groupLayout.createSequentialGroup()
										.addGap(47)
										.addComponent(createAccountButton))
									.addGroup(groupLayout.createSequentialGroup()
										.addGap(57)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
											.addComponent(lblPasswordError, GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE)
											.addComponent(lblUsernameError, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(lblPasswordConfirmError, GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)))))
							.addGap(22)))
					.addGap(15))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(21)
					.addComponent(logo)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(usernameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(usernameLabel))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(passwordLabel, GroupLayout.PREFERRED_SIZE, 13, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(repeatPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(repeatPasswordLabel))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(emailTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(emailLabel))
					.addGap(18)
					.addComponent(createAccountButton)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblUsernameError, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblPasswordError, GroupLayout.PREFERRED_SIZE, 11, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblPasswordConfirmError, GroupLayout.DEFAULT_SIZE, 11, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(lblNewLabel))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblEmailError, GroupLayout.PREFERRED_SIZE, 11, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		setLayout(groupLayout);
		

	}
	//TODO: should maybe use toasts?
	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand().equals("created")) {
			
			//Some variables
			boolean hasError = false;
			String alphaDashNumericRegex = "^[a-zA-Z0-9_]*$";
			String validEmail = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			
			//Empty all error messages prior to testing
			lblUsernameError.setText("");
			lblPasswordError.setText("");
			lblPasswordConfirmError.setText("");
			lblEmailError.setText("");
			//Reset all borders prior to testing
			usernameTextField.setBorder(new LineBorder(new Color(240,240,240), 2));
			passwordField.setBorder(new LineBorder(new Color(240,240,240), 2));
			repeatPasswordField.setBorder(new LineBorder(new Color(240,240,240), 2));
			emailTextField.setBorder(new LineBorder(new Color(240,240,240), 2));
			
			// Check username
			if (usernameTextField.getText().length() == 0) {
				usernameTextField.setBorder(redline);
				lblUsernameError.setText("You must provide a username");
				hasError = true;
			} else if (usernameTextField.getText().length() < 3) {
				usernameTextField.setBorder(redline);
				lblUsernameError.setText("The username must be > 3 characters");
				hasError = true;
			}
			if (! usernameTextField.getText().matches(alphaDashNumericRegex)) {
				usernameTextField.setBorder(redline);
				lblUsernameError.setText("The username can only contain numbers, letters and underscores");
				hasError = true;
			}
			
			//Check password
			if (passwordField.getPassword().length == 0) {
				passwordField.setBorder(redline);
				lblPasswordError.setText("You must provide a password");
				hasError = true;
			} else if (passwordField.getPassword().length < 3) {
				passwordField.setBorder(redline);
				lblPasswordError.setText("The password must be > 3 characters");
				hasError = true;
			}
			
			//Check repeat password
			if (! Arrays.equals(passwordField.getPassword(), repeatPasswordField.getPassword())) {
				repeatPasswordField.setBorder(redline);
				lblPasswordConfirmError.setText("Passwords doesn't match");
				hasError = true;
			}
			
			// Check email
			if (emailTextField.getText().length() == 0) {
				emailTextField.setBorder(redline);
				lblEmailError.setText("You must provide a email");
				hasError = true;
			} else if (! emailTextField.getText().matches(validEmail)) {
				emailTextField.setBorder(redline);
				lblEmailError.setText("The email doesn't seem to be valid");
				hasError = true;
			}
			
			if (! hasError) {
				MessageBus.getInstance().pushMessage(new UserRegisterMessage(usernameTextField.getText(), passwordField.getPassword(), emailTextField.getText()));
			}
		}
	}
	
	
	public void onMessage(IMessage msg) {
		if (msg instanceof UserRegisterSuccessMessage) {
			System.out.println("Successfully registered as " + ((UserRegisterSuccessMessage)msg).getUsername());
			MessageBus.getInstance().pushMessage(new ClientCardSwitchMessage("login"));
		} else if (msg instanceof UserRegisterFailedMessage) {
			System.out.println("Couldn't register");
			//TODO: visual output
		}
	}
}
