package client.view.login;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.smartcardio.CardPermission;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import client.ClientModel;
import client.messages.ClientCardSwitchMessage;
import client.messages.ClientClientFrameMessage;
import client.messages.ClientLoginFrameMessage;
import client.view.graphics.JPlayfuelButton;

import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.ClientMessageMessage;
import utilities.msg.messages.UserLoginFailedMessage;
import utilities.msg.messages.UserLoginMessage;
import utilities.msg.messages.UserLoginSuccessMessage;

import javax.swing.SwingConstants;

import models.IMessage;

import java.awt.CardLayout;

/**
 * The panel containing the components for logging in the user
 *
 */
public class LoginPanel extends JPanel implements ActionListener, IMessageHandler {
	
	
	private JButton loginButton;
	private JButton createAccountButton;

	private ClientModel clientModel;
	private JPanel cap;
	private LoginTextBoxes ltb;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JPanel cardPanel;
	private CardLayout layout;
	
	
	/**
	 * Creates a Loginpanel
	 * @param model - the clientmodel
	 */
	public LoginPanel(ClientModel model) {
		
		this.clientModel = model;
		
		setMinimumSize(new Dimension(340, 380));
		setPreferredSize(new Dimension(340, 574));
		// Set Messages to listen to
		MessageBus.getInstance().register(this, UserLoginSuccessMessage.class);
		MessageBus.getInstance().register(this, UserLoginFailedMessage.class);
		MessageBus.getInstance().register(this, ClientMessageMessage.class);
		
		
		// Check for saved credentials for auto login
		File inFile = new File(ClientModel.clientResourcesFolderPath + File.separator + ".credentials");
		
		if (inFile.exists()) {
			try {
				FileReader inFileReader = new FileReader(inFile);
				BufferedReader br = new BufferedReader(inFileReader);
				
				String line;
				while ((line = br.readLine()) != null)   {
					String[] credentials = line.split(":");
					MessageBus.getInstance().pushMessage(new UserLoginMessage(credentials[0], credentials[1]));
				}
				
				inFileReader.close();

			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		
		
		ltb = new LoginTextBoxes(this);
		ltb.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		
		
		
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		setSize(new Dimension(340, 380));
		
		JLabel logo = new JLabel("");
		logo.setIcon(ClientModel.getTheme().LOGO());
		
		cap = new JPanel();
		cap.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		createAccountButton = new JPlayfuelButton("");
		createAccountButton.setPressedIcon(new ImageIcon(LoginPanel.class.getResource("/client/view/graphics/image_resources/newButtons/CreateAccountPressedSmallGray.png")));
		createAccountButton.setIcon(new ImageIcon(LoginPanel.class.getResource("/client/view/graphics/image_resources/newButtons/CreateAccountSmallGray.png")));
		createAccountButton.setBorderPainted(false);
		createAccountButton.setActionCommand("create");
		createAccountButton.addActionListener(this);
		
		createAccountButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				createAccountButton.setCursor(new Cursor(12));
			}
		});
		
		GroupLayout gl_panel = new GroupLayout(cap);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(createAccountButton, GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
					.addGap(12))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(createAccountButton, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		cap.setLayout(gl_panel);
		
		lblNewLabel = new JLabel("Username or Password Incorrect");
		JPanel messagePanel = new JPanel();
		messagePanel.add(lblNewLabel);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(ClientModel.getTheme().BODY_FONT());
		lblNewLabel.setForeground(ClientModel.getTheme().FAIL());
		lblNewLabel.setVisible(false);
		
		lblNewLabel_1 = new JLabel(ClientModel.getTheme().LOADINGIMAGE());
		lblNewLabel_1.setVisible(false);
		JPanel loadingPanel = new JPanel();
		loadingPanel.add(lblNewLabel_1);
		loadingPanel.setVisible(true);
		
		
		
		
		
		loginButton = new JPlayfuelButton("");
		
		
		loginButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				loginButton.setCursor(new Cursor(12));
			}
		});
		
		
		
		loginButton.setActionCommand("login");
		loginButton.addActionListener(this);
		
		loginButton.setPressedIcon(new ImageIcon(LoginPanel.class.getResource("/client/view/graphics/image_resources/newButtons/LoginPressedSmall.png")));
		loginButton.setSelectedIcon(new ImageIcon(LoginPanel.class.getResource("/client/view/graphics/image_resources/LoginButtonShade.png")));
		loginButton.setBorderPainted(false);
		loginButton.setIcon(new ImageIcon(LoginPanel.class.getResource("/client/view/graphics/image_resources/newButtons/LoginSmall.png")));
		
		cardPanel = new JPanel();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(24)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(ltb, GroupLayout.PREFERRED_SIZE, 257, GroupLayout.PREFERRED_SIZE)
									.addGap(493))
								.addComponent(logo, GroupLayout.DEFAULT_SIZE, 750, Short.MAX_VALUE)))
						.addComponent(cap, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(58)
							.addComponent(loginButton, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(58)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(cardPanel, GroupLayout.PREFERRED_SIZE, 227, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(21)
					.addComponent(logo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(ltb, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
					.addGap(15)
					.addComponent(loginButton, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(cap, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNewLabel)
					.addGap(18)
					.addComponent(cardPanel, GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		layout = new CardLayout(0, 0);
		cardPanel.setLayout(layout);
		cardPanel.add(messagePanel, "message");
		cardPanel.add(loadingPanel, "loading");
		cardPanel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		messagePanel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		loadingPanel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		
		setLayout(groupLayout);

		
	}
	
	
	/**
	 * 
	 * listens for either of the buttons, or messages
	 */
	public void actionPerformed(ActionEvent e) {
		
		String command = e.getActionCommand();
		
		if (command.equals("login")) {
			boolean error = false;
						
			// Check username
			if (ltb.usernameTextField.getText().trim().length() == 0) {
				layout.show(cardPanel, "message");
				lblNewLabel.setText("Must enter Username");
				lblNewLabel.setVisible(true);
				error = true;
			}
			
			// Check password
			if (ltb.passwordField.getPassword().length == 0) {
				layout.show(cardPanel, "message");
				lblNewLabel.setText("Must enter password");
				lblNewLabel.setVisible(true);
				error = true;
			}
			
			if (!error) {
				layout.show(cardPanel, "loading");
				lblNewLabel_1.setVisible(true);
				if (ltb.chckbxRememberLogin.isSelected()) {
					
					File res = new File(ClientModel.clientResourcesFolderPath);
					if(! res.isDirectory()){
			        	res.mkdir();
			        }
					FileWriter outFile;
					try {
						outFile = new FileWriter(res.getPath() + File.separator + ".credentials");
						PrintWriter out = new PrintWriter(outFile);
						
						out.println(ltb.usernameTextField.getText().trim() + ":" + new String(ltb.passwordField.getPassword()));
						out.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}					
				}
				MessageBus.getInstance().pushMessage(new UserLoginMessage(ltb.usernameTextField.getText().trim(), new String(ltb.passwordField.getPassword())));
			}
		} else if(command.equals("create")) {
			
			// Switch to the register card
			MessageBus.getInstance().pushMessage(new ClientCardSwitchMessage("lsp"));
		}
	}
	
	
	@Override
	public void onMessage(IMessage msg) {
		//Login success
		if (msg instanceof UserLoginSuccessMessage) {
			this.clientModel.setProfile(((UserLoginSuccessMessage)msg).getProfile());
			// Send messages to handle window switchingjos
			MessageBus.getInstance().pushMessage(new ClientLoginFrameMessage(false));
			MessageBus.getInstance().pushMessage(new ClientClientFrameMessage(true));
		} 
		//Login failed
		else if(msg instanceof UserLoginFailedMessage) {
			layout.show(cardPanel, "message");
			lblNewLabel.setText("Username or Password Incorrect");
			lblNewLabel.setVisible(true);			
		}
	}
}
