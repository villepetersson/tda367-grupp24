package client.view.login;


import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;

import models.IMessage;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import client.ClientModel;
import client.messages.ClientCardSwitchMessage;
import client.view.ClientFrame;

import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;

/**
 * The initial frame in the client, enabling the user to log in, or switch to a create account card
 *
 */
public class LoginFrame extends JFrame implements IMessageHandler {
	
	private JLayeredPane layerPanel;
	private CardLayout layout;
	private ClientModel clientModel;
	/**
	 * Creates a loginframe
	 * @param clientModel
	 */
	public LoginFrame (ClientModel clientModel) {
		
		// Register for messages
		MessageBus.getInstance().register(this, ClientCardSwitchMessage.class);
		
		this.clientModel = clientModel;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 440, 520);
		setResizable(false);
		layerPanel = new JLayeredPane();
		layerPanel.setPreferredSize(new Dimension(300, 310));
		
		if(!System.getProperty("os.name").contains("windows")){
			ImageIcon img = new ImageIcon(ClientFrame.class.getResource("/client/view/graphics/image_resources/UpdatedLogoBlackSmallerst.png"));
			
			this.setIconImage(img.getImage());
			
		}
		
		// To be able to reach from IMessage below
		layout = new CardLayout(0, 0);
		
		layerPanel.setBackground(Color.RED);
		layerPanel.setLayout(layout);		
		
		LoginPanel login = new LoginPanel(clientModel);
		LoginSignupPanel lsp = new LoginSignupPanel();
		
		setSize(340, 420);
		setMinimumSize(new Dimension(340, 420));
		
		layerPanel.add(login, "login");
		layerPanel.add(lsp, "lsp");
		
		getContentPane().add(layerPanel);
	}

	@Override
	public void onMessage(IMessage msg) {
		// Switch to passed card
		if (msg instanceof ClientCardSwitchMessage) {
			layout.show(layerPanel, ((ClientCardSwitchMessage)msg).getCard());
		}
	}
}
