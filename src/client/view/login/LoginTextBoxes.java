package client.view.login;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import client.ClientModel;

public class LoginTextBoxes extends JPanel {
	public JTextField usernameTextField;
	public JPasswordField passwordField;
	public JCheckBox chckbxRememberLogin;
	
	
	/**
	 * Panel containing the textboxes for entering user credentials for login
	 * @param parent - the wrapping panel
	 */
	public LoginTextBoxes(LoginPanel parent){
		
		
		usernameTextField = new JTextField();
		passwordField = new JPasswordField();
		chckbxRememberLogin = new JCheckBox("Remember me!");
		chckbxRememberLogin.setForeground(ClientModel.getTheme().BODY_COLOR());
		chckbxRememberLogin.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		JLabel label = new JLabel("Password");
		label.setFont(ClientModel.getTheme().HEADER_FONT_NOT_BOLD());
		label.setForeground(ClientModel.getTheme().BODY_COLOR());
		
		JLabel label_1 = new JLabel("Username");
		label_1.setFont(ClientModel.getTheme().HEADER_FONT_NOT_BOLD());
		label_1.setForeground(ClientModel.getTheme().BODY_COLOR());
		
		//usernameTextField = new JTextField();
		usernameTextField.setBorder(new LineBorder( ClientModel.getTheme().TEXTBOX_BORDER(), 2));
		
		//passwordField = new JPasswordField();
		passwordField.setBorder(new LineBorder( ClientModel.getTheme().TEXTBOX_BORDER(), 2));
		
		usernameTextField.setActionCommand("login");
		usernameTextField.setColumns(10);	
		usernameTextField.addActionListener(parent);
		passwordField.setActionCommand("login");
		passwordField.setColumns(10);
		passwordField.addActionListener(parent);
		
		
		
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent arg0) {
				usernameTextField.requestFocusInWindow();
			}
		});
		
		//JCheckBox chckbxRememberLogin = new JCheckBox("remember me!");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(46)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(label_1)
						.addComponent(label))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(usernameTextField, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
						.addComponent(passwordField, 0, 0, Short.MAX_VALUE))
					.addGap(43))
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(72)
					.addComponent(chckbxRememberLogin)
					.addContainerGap(75, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(label_1)
						.addComponent(usernameTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(chckbxRememberLogin, GroupLayout.PREFERRED_SIZE, 20, Short.MAX_VALUE)
					.addContainerGap())
		);
		setLayout(groupLayout);
		
		
		
		
	}
}
