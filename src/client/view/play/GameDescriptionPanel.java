package client.view.play;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;

import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.LineBorder;

import models.Game;
import models.IMessage;

import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.GameInfoMessage;
import utilities.msg.messages.LobbyCreateMessage;
import utilities.msg.messages.UserJoinQueueMessage;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.ImageIcon;

import client.ClientModel;
import client.messages.ClientModalMessage;
import client.view.graphics.JPlayfuelButton;
import client.view.modals.MatchQueuePanel;

import java.awt.Dimension;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.BoxLayout;
import java.awt.Component;
import javax.swing.Box;

/**
 * Panel containing the description of the choosen game, also contains buttons to join/start lobby 
 *
 *
 */
public class GameDescriptionPanel extends JPanel implements IMessageHandler {
	
	private ClientModel clientModel;
	private JLabel lblNewLabel_1;
	private String gameidentifier;
	private JTextArea textArea;
	private JPanel panel;
	private Component horizontalStrut;
	
	/**
	 * Creates a new GameDescriptionPanel
	 * @param clientModel - the client model
	 * @param name - The name of the game 
	 * @param description - the game description
	 */
	public GameDescriptionPanel(ClientModel clientModel, String name, String description) {
		//Register for msgs
		MessageBus.getInstance().register(this, GameInfoMessage.class);
		
		this.clientModel = clientModel;
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		JPanel strutPanel = new JPanel();
		strutPanel.setLayout(new BorderLayout());
		strutPanel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		textArea = new JTextArea();
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		textArea.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		textArea.setForeground(ClientModel.getTheme().BODY_COLOR());
		textArea.setText(description);
		textArea.setFont(ClientModel.getTheme().BODY_FONT());
		
		lblNewLabel_1 = new JLabel(name);
		lblNewLabel_1.setFont(ClientModel.getTheme().HEADER_FONT());
		lblNewLabel_1.setForeground(ClientModel.getTheme().BODY_COLOR());
		strutPanel.add(lblNewLabel_1, BorderLayout.NORTH);
		strutPanel.add(textArea, BorderLayout.CENTER);
		
		
		
		
		final JPanel dis = this;
		this.addComponentListener(new ComponentListener() {
			@Override
			public void componentShown(ComponentEvent arg0) {}
			@Override
			public void componentResized(ComponentEvent arg0) {
				textArea.setPreferredSize(new Dimension(10,10));
			}
			@Override
			public void componentMoved(ComponentEvent arg0) {}
			@Override
			public void componentHidden(ComponentEvent arg0) {}
		});
		setLayout(new BorderLayout(0, 0));
		
		add(strutPanel, BorderLayout.CENTER);
		
		horizontalStrut = Box.createHorizontalStrut(9);
		strutPanel.add(horizontalStrut, BorderLayout.WEST);

		
		panel = new JPanel();
		panel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		JButton btnNewButton = new JPlayfuelButton("");
		panel.add(btnNewButton);
		btnNewButton.setPressedIcon(new ImageIcon(GameDescriptionPanel.class.getResource("/client/view/graphics/image_resources/newButtons/CreateGamePressedSmall.png")));
		btnNewButton.setIcon(new ImageIcon(GameDescriptionPanel.class.getResource("/client/view/graphics/image_resources/newButtons/CreateGameSmall.png")));
		btnNewButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnNewButton.setBorderPainted(false);
		btnNewButton.setFont(new Font("Helvetica", Font.PLAIN, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MessageBus.getInstance().pushMessage(new LobbyCreateMessage(gameidentifier));
			}
		});
		
		//setBorder(new LineBorder(Color.GRAY, 5));

		JButton btnNewButton_1 = new JPlayfuelButton("");
		panel.add(btnNewButton_1);
		btnNewButton_1.setPressedIcon(new ImageIcon(GameDescriptionPanel.class.getResource("/client/view/graphics/image_resources/newButtons/QuickplayPressedSmall.png")));
		btnNewButton_1.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnNewButton_1.setBorderPainted(false);
		btnNewButton_1.setIcon(new ImageIcon(GameDescriptionPanel.class.getResource("/client/view/graphics/image_resources/newButtons/QuickplaySmall.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MessageBus.getInstance().pushMessage(new UserJoinQueueMessage(gameidentifier));
				MessageBus.getInstance().pushMessage(new ClientModalMessage(new MatchQueuePanel(gameidentifier), "In queue"));

			}
		});
		btnNewButton_1.setFont(new Font("Helvetica", Font.PLAIN, 12));
	}
	
	
	
	/**
	 * Sets the game name in the header
	 */
	public void setName(String name){
		lblNewLabel_1.setText(name);
		
	}

	/**
	 * Sets the game description
	 * 
	 */
	public void setDescription(String description){
		textArea.setText(description);
	}

	@Override
	public void onMessage(IMessage msg) {
		if(msg instanceof GameInfoMessage){
			GameInfoMessage gimsg = (GameInfoMessage) msg;
			this.setName(gimsg.name);
			this.setDescription(gimsg.description);
			this.gameidentifier=gimsg.identifier;
			repaint();
			
			// Also set the currently viewed game in the client model
			clientModel.setCurrentGameViewed(this.gameidentifier);
		}
	}
}
