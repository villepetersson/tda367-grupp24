package client.view.play;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import client.ClientModel;

/**
 * Panel for the game tabs  with rankings and screenshots
 *
 *
 */
public class GameTabsPanel extends JPanel {
	
	private ClientModel clientModel;
	private TopRankingsPanel highScorePanel;
	
	
	/**
	 * Creates a new GameTabsPanel
	 * @param clientModel
	 */
	public GameTabsPanel(ClientModel clientModel) {
		this.clientModel = clientModel;
		
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		setMinimumSize(new Dimension(200,300));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(tabbedPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
		);
		setLayout(groupLayout);
		
		ScreenShotPanel screenshotPanel = new ScreenShotPanel(clientModel);
		tabbedPane.addTab("Screenshots", null, screenshotPanel, null);
		
		highScorePanel = new TopRankingsPanel(clientModel);
		tabbedPane.addTab("Top Rankings", null, highScorePanel, null);
		

		UIManager.put("TabbedPane.background",ClientModel.getTheme().ACCENT_BACKGROUND());
		UIManager.put("TabbedPane.shadow",ClientModel.getTheme().ACCENT_BACKGROUND());
		UIManager.put("TabbedPane.font",ClientModel.getTheme().BODY_FONT());
		UIManager.put("TabbedPane.darkShadow", ClientModel.getTheme().ACCENT_BACKGROUND());
		UIManager.put("TabbedPane.highlight",ClientModel.getTheme().MAIN_BACKGROUND());   
		tabbedPane.setBackgroundAt(0, ClientModel.getTheme().ACCENT_BACKGROUND());
		tabbedPane.setForeground(ClientModel.getTheme().BODY_COLOR());
		tabbedPane.setBackgroundAt(1, ClientModel.getTheme().ACCENT_BACKGROUND());
        tabbedPane.setUI(new BasicTabbedPaneUI() {      
                       @Override
                       protected void installDefaults() {
                           super.installDefaults();                         
                           focus = ClientModel.getTheme().MAIN_BACKGROUND();
                       }
                    });
        
        ChangeListener changeListener = new ChangeListener() {
            public void stateChanged(ChangeEvent changeEvent) {
              JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
              int index = sourceTabbedPane.getSelectedIndex();
              
              // Fetch highscores when clicking
              if (sourceTabbedPane.getTitleAt(index) == sourceTabbedPane.getTitleAt(1)) {
            	  highScorePanel.getHighScores();
              }
            }
          };
          tabbedPane.addChangeListener(changeListener);
	}

}
