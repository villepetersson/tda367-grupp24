package client.view.play;

import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.border.LineBorder;

import models.Game;
import models.IMessage;

import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.*;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.BoxLayout;

import client.ClientModel;

/**
 * Panel containing the list of avaliable games in the play tab
 */
public class GameListPanel extends JPanel implements IMessageHandler, MouseListener {
	
	private List<Game> games;
	private GameDescriptionPanel descriptionPanel;
	private ClientModel model;
	/**
	 * Creates a new GameListPanel
	 * @param cmodel - the client model
	 * @param panel - the GameDescriptionPanel that shows information about the chosen game
	 */
	public GameListPanel(ClientModel cmodel,GameDescriptionPanel panel) {
		this.model=cmodel;
		descriptionPanel = panel;
		// Register for messages
		MessageBus.getInstance().register(this, GameListMessage.class);
		
		// Push messages
		MessageBus.getInstance().pushMessage(new GameListGetMessage());
		setBorder(new LineBorder(ClientModel.getTheme().DETAIL_BACKGROUND(), 2));
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}
	
	public void onMessage(IMessage msg) {
		if (msg instanceof GameListMessage) {
			// First clear the list
			this.removeAll();
			
			games = ((GameListMessage)msg).getGames();
			int i = 0;
			
			if (games.size() > 0) {
				for(final Game g : games) {
					if(i == 0){
						MessageBus.getInstance().pushMessage(new GameInfoGetMessage(g.getIdentifier()));
					}
					
					GameListItemPanel panel = new GameListItemPanel(model,g.getIdentifier(),g.getGameName());
					panel.addMouseListener(this);
					
					panel.addMouseListener(new MouseListener() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							MessageBus.getInstance().pushMessage(new GameInfoGetMessage(g.getIdentifier()));
							MessageBus.getInstance().pushMessage(new GameRankingGetMessage(g.getIdentifier()));

						}
						@Override
						public void mouseEntered(MouseEvent arg0) {}
						@Override
						public void mouseExited(MouseEvent arg0) {}
						@Override
						public void mousePressed(MouseEvent arg0) {}
						@Override
						public void mouseReleased(MouseEvent arg0) {}
					});
					
					add(panel);
					i++;
				}
			} else {
				add(new JLabel("No games to display"));
			}
		}
	}
	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {
		setCursor(new Cursor(12));
	}
	@Override
	public void mouseExited(MouseEvent e) {
		setCursor(new Cursor(0));		
	}
	@Override
	public void mousePressed(MouseEvent e) {}
	@Override
	public void mouseReleased(MouseEvent e) {}
}
