package client.view.play;

import javax.swing.JPanel;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.BorderLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import client.ClientModel;
/**
 * The list item of a game, in the game list
 * @author Oscar
 *
 */
public class GameListItemPanel extends JPanel {
	private static List<Color> colorList = new ArrayList();
	private Random rand;
	
	/**
	 * Creates a new GameListItemPanel
	 * @param model - the client model
	 * @param id - the game identifier
	 * @param name - the name of the game
	 */
	public GameListItemPanel(ClientModel model, String id, String name) {
		rand = new Random();
		this.setSize(new Dimension(184, 42));
		this.setMaximumSize(new Dimension(184, 42));
		
		JLabel panel = new JLabel();
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		
		JLabel lblGamelabel = new JLabel(name);
		lblGamelabel.setFont(ClientModel.getTheme().BODY_FONT());
		lblGamelabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		lblGamelabel.setHorizontalAlignment(SwingConstants.CENTER);
		setLayout(new BorderLayout(0, 0));
		Color col = randColor();
		while(true){
			if(colorList.contains(col)){
				col = randColor();
			}else{
				panel.setBackground(randColor());
				break;
			}
		}
		
		panel.setIcon(new ImageIcon(model.getLogo(id).getImage().getScaledInstance(this.getHeight(), this.getHeight(), Image.SCALE_SMOOTH)));
		
		panel.setMinimumSize(new Dimension(this.getHeight(), this.getHeight()));
		panel.setMaximumSize(new Dimension(this.getHeight(), this.getHeight()));
		panel.setPreferredSize(new Dimension(this.getHeight(), this.getHeight()));
		add(panel, BorderLayout.WEST);
		add(lblGamelabel);
	}
	
	
	/**
	 * Returns a random color
	 * @return
	 */
	public Color randColor(){
		return new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)); 
	}
	
}
