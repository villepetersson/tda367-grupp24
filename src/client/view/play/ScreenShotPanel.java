package client.view.play;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.image.BufferedImage;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import client.ClientModel;

import models.IMessage;

import utilities.ImageHandler;
import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.GameInfoMessage;


/**
 * Panel tab for displaying screenshot of the chosen game 
 *
 *
 */
public class ScreenShotPanel extends JPanel implements IMessageHandler {
	private JLabel lblImg;
	private String gameidentifier;
	private ClientModel model;
	private static final int ssWidth = 260;
	private static final int ssHeight = 170;

	/**
	 * Creates a new ScreenShotPanel
	 * @param model
	 */
	public ScreenShotPanel(ClientModel model) {
		MessageBus.getInstance().register(this, GameInfoMessage.class);
		this.model=model;
		lblImg = new JLabel("<image>");
		lblImg.setText("");
		ImageIcon icon = model.getScreenshot(gameidentifier);
		setBackground(ClientModel.getTheme().ACCENT_BACKGROUND());
		//setBorder(new LineBorder(ClientModel.getTheme().ACCENT_BACKGROUND(), 4));
		setLayout(new BorderLayout(0, 0));
		
		
		Component verticalStrut2 = Box.createVerticalStrut(10);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		lblImg.setIcon(model.getScreenshot(gameidentifier));
		panel.add(lblImg, BorderLayout.CENTER);
		panel.add(verticalStrut2, BorderLayout.NORTH);

		Component horizontalStrut = Box.createHorizontalStrut(15);
		panel.setBackground(ClientModel.getTheme().ACCENT_BACKGROUND());
		panel.add(horizontalStrut, BorderLayout.WEST);
		add(panel, BorderLayout.NORTH);

	}
	
	@Override
	public void onMessage(IMessage msg) {
		if(msg instanceof GameInfoMessage){
			GameInfoMessage gimsg = (GameInfoMessage) msg;
			this.gameidentifier=gimsg.identifier;
			
			ImageIcon icon = model.getScreenshot(gameidentifier);
			BufferedImage image = ImageHandler.makeBufferedImage(icon);
			lblImg.setIcon(new ImageIcon(ImageHandler.rescale(image, ssWidth, ssHeight)));

			repaint();

		}
	}
}
