package client.view.play;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;

import models.IMessage;
import models.Profile;
import models.RankingResult;

import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.GameRankingGetMessage;
import utilities.msg.messages.GameRankingMessage;

import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.border.LineBorder;
import javax.swing.ScrollPaneConstants;

import client.ClientModel;

/**
 * Panel tab for displaying top rankings in the chosen game
 *
 */

public class TopRankingsPanel extends JPanel implements IMessageHandler {
	
	private ClientModel clientModel;
	private JPanel content;
	
	
	/**
	 * Creates a new TopRankingsPanel
	 * @param clientModel
	 */
	public TopRankingsPanel(ClientModel clientModel) {
		this.clientModel = clientModel;
		setBackground(ClientModel.getTheme().ACCENT_BACKGROUND());
		// Register for messages
		MessageBus.getInstance().register(this, GameRankingMessage.class);
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrollPane, BorderLayout.CENTER);
		scrollPane.setBorder(null);
		
		content = new JPanel();
		scrollPane.setViewportView(content);
		scrollPane.setBackground(ClientModel.getTheme().ACCENT_BACKGROUND());
		scrollPane.setViewportBorder(null);
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
		content.setBackground(ClientModel.getTheme().ACCENT_BACKGROUND());
		
	}
	
	public void getHighScores() {
		// Send messages
		MessageBus.getInstance().pushMessage(new GameRankingGetMessage(clientModel.getCurrentGameViewed()));
	}
	@Override
	public void onMessage(IMessage msg) {
		if (msg instanceof GameRankingMessage) {
			GameRankingMessage grm = (GameRankingMessage) msg;
			
			// First clear the panel
			content.removeAll();
			
			if(grm.rankings.size() == 0){
			    JLabel noRankings = new JLabel("   No ELO-ranked players registered!");
			    noRankings.setForeground(ClientModel.getTheme().BODY_COLOR());
			    content.add(noRankings);
			} else {
			
				for (RankingResult res : grm.rankings) {
				    Profile profile = res.getProfile();
				    Double ranking = res.getRanking();
				    JPanel row = new JPanel(new GridLayout(1,3));
				    row.setBackground(ClientModel.getTheme().ACCENT_BACKGROUND());
				    Dimension size = new Dimension(280,15);
				    row.setMaximumSize(size);
				    row.setPreferredSize(size);
				    JLabel name = new JLabel(profile.getUsername());
				    JLabel rank = new JLabel((int)Math.round(ranking)  + "");
				    name.setForeground(ClientModel.getTheme().BODY_COLOR());
				    rank.setForeground(ClientModel.getTheme().BRANDING_LIGHT());
				    name.setFont(ClientModel.getTheme().BODY_FONT());
				    rank.setFont(ClientModel.getTheme().HEADER_FONT());
	
				    row.add(name);
				    row.add(rank);
				 
				    content.add(row);
				}
			}
			
			content.updateUI();
		}
		
	}
}
