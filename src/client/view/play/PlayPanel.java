package client.view.play;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.LineBorder;

import java.awt.GridLayout;
import javax.swing.JLabel;

import client.ClientModel;
	
/**
 * Top level wrapper for play tab, container of other panels
 *
 */
public class PlayPanel extends JPanel {
		
		
		private GameDescriptionPanel descriptionPanel;
		private ClientModel clientModel;
		/**
		 * Creates a new PlayPanel
		 * @param clientModel
		 */
		public PlayPanel(ClientModel clientModel) {
			this.clientModel = clientModel;
			this.descriptionPanel = new GameDescriptionPanel(clientModel, "name", "description");
			//this.setBorder(new LineBorder(Color.BLACK, 2));
			
			setBackground(ClientModel.getTheme().MAIN_BACKGROUND());

			JPanel listListPanel = new JPanel();
			listListPanel.setBackground(Color.GRAY);
			listListPanel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
			
			JPanel launchAndInfoPanel = new JPanel();		
			launchAndInfoPanel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
			
			JLabel lblGames = new JLabel("Games");
			lblGames.setFont(ClientModel.getTheme().HEADER_FONT());
			lblGames.setForeground(ClientModel.getTheme().BODY_COLOR());
			GroupLayout groupLayout = new GroupLayout(this);
			groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
					.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
							.addComponent(lblGames)
							.addComponent(listListPanel, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(launchAndInfoPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap())
			);
			groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
					.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addGroup(groupLayout.createSequentialGroup()
								.addContainerGap()
								.addComponent(launchAndInfoPanel, GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE))
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(5)
								.addComponent(lblGames)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(listListPanel, GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)))
						.addContainerGap())
			);
			launchAndInfoPanel.setLayout(new BoxLayout(launchAndInfoPanel, BoxLayout.Y_AXIS));
			setLayout(groupLayout);
			listListPanel.setLayout(new BorderLayout());
			
			listListPanel.add(new GameListPanel(clientModel,descriptionPanel), BorderLayout.CENTER);
			launchAndInfoPanel.add(descriptionPanel);
			launchAndInfoPanel.add(new GameTabsPanel(this.clientModel));
			
			
		}
}
