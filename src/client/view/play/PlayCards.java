package client.view.play;

import java.awt.CardLayout;

import javax.swing.*;

import client.ClientModel;
import client.view.lobby.LobbyPanel;

/**
 * 
 * The panel containing the cardLayout for switching between lobby and play view
 *
 */
public class PlayCards extends JPanel {
	private ClientModel clientModel;
	private CardLayout cardLayout = new CardLayout();
	private LobbyPanel lobbyPanel;

	
	/**
	 * Creates a new PlayCards panel 
	 *
	 */
	public PlayCards(ClientModel model) {
		this.clientModel = model;
		
		this.setLayout(cardLayout);
		this.add(new PlayPanel(model),"play");
		
		setVisible(true);
	}
	
	
	/**
	 * Shows the lobby card, if present
	 */
	public void updateCards() {
		if (clientModel.getCurrentLobby()!=null) {
			//baseLayer.setLayout(new GridLayout(2,1));
			lobbyPanel = new LobbyPanel(clientModel);
	    	this.add(lobbyPanel,"lobby");
	    	cardLayout.show(this, "lobby");
		} else {
	    	cardLayout.show(this, "play");
			if (lobbyPanel!=null) {
				this.remove(lobbyPanel);
			}
		}
    	
		this.setVisible(true);
    	this.repaint();		
	}
}
