package client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.InvocationTargetException;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import api.PlayfuelGameClient;

import models.IMessage;

import client.ClientModel;
import client.messages.ClientModalDisposeAllMessage;
import client.messages.ClientModalMessage;
import client.view.account.AccountPanel;
import client.view.graphics.NotificationMessagePanel;
import client.view.graphics.LogoPanel;
import client.view.graphics.ModalDarkOverlayPanel;
import client.view.modals.ExitModalPanel;
import client.view.modals.Modal;
import client.view.play.GameDescriptionPanel;
import client.view.play.PlayCards;
import client.view.social.SocialPanel;

import utilities.ClientConnection;
import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.ClientMessageMessage;
import utilities.msg.messages.ConnectToGameHubMessage;
import utilities.msg.messages.LobbySlotInviteReceivedMessage;
import utilities.msg.messages.LobbyUpdatedMessage;
import utilities.msg.messages.MatchResultsMessage;
import utilities.msg.messages.MatchmakingLobbyJoinedMessage;
import utilities.msg.messages.UserExitQueueMessage;
import utilities.msg.messages.UserFriendListGetMessage;
import utilities.msg.messages.UserIdleMessage;
/**
 * Top level frame started after user login
 * Contains TabbedPane for play, social and account views
 * Contains LayeredPane for showing modals and notification popups
 */
public class ClientFrame extends JFrame implements IMessageHandler {
	
	private ClientModel clientModel;
	private JLayeredPane layerPanel = new JLayeredPane();
	private JPanel baseLayer;
	private static Dimension frameSize = new Dimension(525,530);
	JTabbedPane tabbedPane;
	private Modal openModal = null;
	private JPanel modal;
	private ModalDarkOverlayPanel modalBG;
	private LogoPanel logo;
	
	private PlayCards playCards;
	
	/**
	 * Creates a new ClientFrame
	 * @param model - the client model
	 */
	public ClientFrame(ClientModel model) {
		
		this.clientModel = model;
		
		// Register for messages
		MessageBus.getInstance().register(this, LobbyUpdatedMessage.class);
		MessageBus.getInstance().register(this, ConnectToGameHubMessage.class);
		MessageBus.getInstance().register(this, ClientMessageMessage.class);
		MessageBus.getInstance().register(this, ClientModalMessage.class);	
		MessageBus.getInstance().register(this, ClientModalDisposeAllMessage.class);
		
		
		setTitle("Playfuel.");
		setFont(new Font("Helvetica", Font.PLAIN, 12));
		setForeground(ClientModel.getTheme().BODY_COLOR());
		setBackground(ClientModel.getTheme().BACKGROUND());
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setMinimumSize(new Dimension(525, 545));
		
		
		if(!System.getProperty("os.name").contains("windows")){
			ImageIcon img = new ImageIcon(ClientFrame.class.getResource("/client/view/graphics/image_resources/UpdatedLogoBlackSmallerst.png"));
			
			this.setIconImage(img.getImage());
			
		}
		
		this.addWindowListener(new WindowAdapter()
		{
		      public void windowClosing(WindowEvent e)
		      {
		          closeClicked();
		      }
		});
		
		// Center on screen
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((screenSize.width / 2) - (frameSize.width / 2), (screenSize.height / 2) - (frameSize.height / 2), frameSize.width, frameSize.height);
		
		// This is the base layer with the tabs and all normal content. -20 to not go all the way down
		baseLayer = new JPanel(new GridLayout(1,1));
		baseLayer.setForeground(ClientModel.getTheme().BODY_COLOR());
		baseLayer.setBackground(ClientModel.getTheme().BACKGROUND());
		//this.setPreferredSize(new Dimension(frameSize.width, frameSize.height-100));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, frameSize.width, frameSize.height);
		tabbedPane.setForeground(ClientModel.getTheme().BODY_COLOR());
		tabbedPane.setBackground(ClientModel.getTheme().BACKGROUND());
		
		playCards = new PlayCards(this.clientModel);
		SocialPanel social = new SocialPanel(this.clientModel);
		AccountPanel account = new AccountPanel(this.clientModel);
				
		
		//adds tabs to tabbedpane, html/ccs is used for styling 
		tabbedPane.addTab("<html><p style='padding:3px 20px 3px 20px;'>Play<p><html>", null, playCards, null);
		tabbedPane.addTab("<html><p style='padding:3px 20px 3px 20px;'>Social<p><html>", null, social, null);
		tabbedPane.addTab("<html><p style='padding:3px 20px 3px 20px;'>Account<p><html>", null, account, null);
		
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent changeEvent) {
				JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
				int index = sourceTabbedPane.getSelectedIndex();
				
				String tabChangedTo = sourceTabbedPane.getTitleAt(index);
				if (tabChangedTo.contains("Social")) {
					MessageBus.getInstance().pushMessage(new UserFriendListGetMessage());
				}
			}
		});

			
	    
		if(!System.getProperty("os.name").equals("Mac OS X")) {
			UIManager.put("TabbedPane.tabsOverlapBorder", false);
			UIManager.put("TabbedPane.selectedTabPadInsets",new Insets(0, 0, 5, 0));
			UIManager.put("TabbedPane.tabInsets",new Insets(0, 0, 5, 0));

		    UIManager.put("TabbedPane.contentBorderInsets", new Insets(0,0,0,0));
		}
		
		UIManager.put("TabbedPane.tabsOverlapBorder", true);
        UIManager.put("TabbedPane.selected",ClientModel.getTheme().MAIN_BACKGROUND());
        UIManager.put("TabbedPane.background",ClientModel.getTheme().MAIN_BACKGROUND());
        UIManager.put("TabbedPane.shadow",ClientModel.getTheme().MAIN_BACKGROUND());
        UIManager.put("TabbedPane.font",ClientModel.getTheme().HEADER_FONT_NOT_BOLD());
        UIManager.put("TabbedPane.borderColor", Color.RED);
        UIManager.put("TabbedPane.darkShadow", ClientModel.getTheme().MAIN_BACKGROUND());
		UIManager.put("TabbedPane.highlight",ClientModel.getTheme().BACKGROUND());   
        UIManager.put("TabbedPane.darkShadow", ClientModel.getTheme().MAIN_BACKGROUND());
	    UIManager.put("TabbedPane.highlight",ClientModel.getTheme().BACKGROUND());


	    
        tabbedPane.setBackgroundAt(0, ClientModel.getTheme().MAIN_BACKGROUND());
        tabbedPane.setBackgroundAt(1, ClientModel.getTheme().MAIN_BACKGROUND());
        tabbedPane.setBackgroundAt(2, ClientModel.getTheme().MAIN_BACKGROUND());
        tabbedPane.setUI(new BasicTabbedPaneUI() {      
                       @Override
                       protected void installDefaults() {
                           super.installDefaults();                         
                           focus = ClientModel.getTheme().MAIN_BACKGROUND();
                       }
            });
        logo = new LogoPanel();

		layerPanel.addComponentListener(new ComponentListener() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				baseLayer.setSize(layerPanel.getWidth(), layerPanel.getHeight());
		        logo.setBounds(layerPanel.getWidth()-130, 0, 120, 40);
			}

			@Override
			public void componentHidden(ComponentEvent e) {}

			@Override
			public void componentMoved(ComponentEvent e) {}

			@Override
			public void componentShown(ComponentEvent e) {}
		});
		baseLayer.add(tabbedPane);
		layerPanel.add(baseLayer,JLayeredPane.DEFAULT_LAYER);
		layerPanel.add(logo, new Integer(200));
		layerPanel.setVisible(true);
		layerPanel.repaint();
			
		getContentPane().add(layerPanel, BorderLayout.CENTER);
		setVisible(true);
		
		// Notify the server that user is not in game or in lobby upon creation of ClientFrame
		MessageBus.getInstance().pushMessage(new UserIdleMessage());
	}
	@Override
	public void onMessage(IMessage imsg) {
		// Refresh the UI then lobby is updates
		if(imsg instanceof LobbyUpdatedMessage) {
			
	    	playCards.updateCards();
		        
	    // Refresh the UI and hide when a game starts
		} else if(imsg instanceof ConnectToGameHubMessage) {
			
			this.playCards.updateCards();
			this.setVisible(false);
			this.repaint();	
			
		// Toast messages
		} else if (imsg instanceof ClientMessageMessage) {
			ClientMessageMessage message = (ClientMessageMessage)imsg;
			NotificationMessagePanel fm = new NotificationMessagePanel(message);
			
			// Set message size and center it
			int messageHeight = 50;
			int messageWidth = this.getWidth();
			int xCoord = 0;
			int yCoord = this.getHeight() - messageHeight;
			fm.setBounds(xCoord, yCoord, messageWidth, messageHeight);
			layerPanel.add(fm, JLayeredPane.POPUP_LAYER);

		// Modals
		} else if (imsg instanceof ClientModalMessage) {
			ClientModalMessage cmm = (ClientModalMessage) imsg;
			
			// First make sure that there is no active modal (delete if there are)
			if (this.modal instanceof JPanel || this.modalBG instanceof ModalDarkOverlayPanel) {
				layerPanel.remove(modal);
			}
			if (this.modalBG instanceof ModalDarkOverlayPanel) {
				layerPanel.remove(modalBG);
			}
			
			this.openModal = cmm.modal;
			
			// The modal wrapping layout
			modal = new JPanel(new BorderLayout());
			
			int headerHeight = 20;
			int modalHeight = cmm.height + headerHeight;
			int modalWidth = cmm.width;
			// Center in frame
			int modalX = this.getWidth() / 2 - modalWidth / 2;
			int modalY = this.getHeight() / 2 - modalHeight / 2;
			modal.setBounds(modalX, modalY, modalWidth, modalHeight);

			modal.setBorder(new LineBorder(ClientModel.getTheme().ACCENT_BACKGROUND(), 1));
		

			
			// Header
			JPanel headerPane = new JPanel();
			headerPane.setBackground(ClientModel.getTheme().BRANDING_LIGHT());
			headerPane.setBounds(0, 0, modalWidth, headerHeight);
			

			JLabel header = new JLabel(cmm.modal.getHeader());
			header.setForeground(ClientModel.getTheme().BRANDING_LIGHT_INVERSE());
			header.setFont(ClientModel.getTheme().HEADER_FONT());
			headerPane.add(header);
			
			modal.add(headerPane, BorderLayout.NORTH);
			
			modal.add(cmm.modal.getContent(), BorderLayout.CENTER);
			
			// Make sure cursor is default when hovering
			modal.addMouseListener(new MouseListener() {
				public void mouseReleased(MouseEvent arg0) {}
				public void mousePressed(MouseEvent arg0) {}

				public void mouseExited(MouseEvent arg0) {}

				public void mouseEntered(MouseEvent arg0) {
					setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}

				public void mouseClicked(MouseEvent arg0) {}
			});
			
			// Faded Background for Modal
			modalBG = new ModalDarkOverlayPanel(0.5f);
			modalBG.setBackground(Color.BLACK);
			modalBG.setBounds(0, 0, this.getWidth(), this.getHeight());
			// Hand cursor to indicate that you can close
			modalBG.addMouseListener(new MouseListener() {
				@Override
				public void mouseReleased(MouseEvent arg0) {}
				@Override
				public void mousePressed(MouseEvent arg0) {}
				@Override
				public void mouseExited(MouseEvent arg0) {}
				@Override
				public void mouseEntered(MouseEvent arg0) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				}
				@Override
				public void mouseClicked(MouseEvent arg0) {
					//Exit matchmaking queue at dispose of modal
					MessageBus.getInstance().pushMessage(new UserExitQueueMessage(clientModel.getCurrentGameViewed()));
					
					// Hide the modal on click outside it
					layerPanel.remove(modalBG);
					layerPanel.remove(modal);
					setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					repaint();
					
					// Run modals dispose actions
					if (openModal instanceof Modal) {
						openModal.onDispose();
						openModal = null;
					}
				}
			});
			
			// Add to new layers
			layerPanel.add(modalBG, JLayeredPane.PALETTE_LAYER);
			layerPanel.add(modal, JLayeredPane.MODAL_LAYER);
			
		// To dispose all active modals from a place far, far away
		} else if (imsg instanceof ClientModalDisposeAllMessage) {
			layerPanel.remove(modalBG);
			layerPanel.remove(modal);
			repaint();
			
			if (this.openModal != null) {
				// Run modals dispose actions
				openModal.onDispose();
				openModal = null;
			}
						
		}	
	}
	
	
	/**
	 * Displays modal panel confirming user exit
	 */
	public void closeClicked(){
		Modal modal = new Modal();
		modal.setContent(new ExitModalPanel());
		modal.setHeader("Quit?");
		MessageBus.getInstance().pushMessage(new ClientModalMessage(modal));
	}
}
