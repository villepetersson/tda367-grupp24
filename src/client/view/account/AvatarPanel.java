package client.view.account;

import java.awt.AlphaComposite;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.LineBorder;

import client.ClientModel;
import client.view.graphics.JPlayfuelButton;

import java.awt.Font;

import utilities.ImageHandler;
import utilities.JPEGImageFileFilter;
import utilities.msg.MessageBus;
import utilities.msg.messages.UserAvatarMessage;


/**
 * Panel enabling the user to change avatar
 * @author Oscar
 *
 */

public class AvatarPanel extends JPanel {
	 private File targetFile;
	 private BufferedImage targetImg;
	 public JPanel panel;
	 public JPanel imgPanel;
	 private static final int baseSize = 100;
	 private static final String basePath = "/client/view/graphics/image_resources/";
	 private JLabel img;
	 private ClientModel model;
	
	 /**
	  * Creates a panel for changing user avatars
	  * @param model - the corresponding client model
	  */
	 public AvatarPanel(ClientModel model){
		 
		 	this.model = model;
	        panel = new JPanel();
	        panel.setBorder(null);
	        setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
	        panel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
	        
	        JButton btnBrowse = new JPlayfuelButton("");
	        btnBrowse.setCursor(new Cursor(Cursor.HAND_CURSOR));
	        btnBrowse.setPressedIcon(new ImageIcon(AvatarPanel.class.getResource("/client/view/graphics/image_resources/newButtons/BrowsePressedSmall.png")));
	        btnBrowse.setBorderPainted(false);
	        btnBrowse.setIcon(new ImageIcon(AvatarPanel.class.getResource("/client/view/graphics/image_resources/newButtons/BrowseSmall.png")));
	        btnBrowse.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e) {
	                browseButtonActionPerformed(e);
	            }
	        });
	        	        
	        JLabel lblSelectTargetPicture = new JLabel("Change Avatar");
	        lblSelectTargetPicture.setFont(ClientModel.getTheme().BODY_FONT());
	        lblSelectTargetPicture.setForeground(ClientModel.getTheme().BODY_COLOR());

	        imgPanel = new JPanel();
	        img = new JLabel("");
	        imgPanel.add(img);
	        
	        imgPanel.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
	        imgPanel.setBorder(new LineBorder(ClientModel.getTheme().TEXTBOX_BORDER(), 3, true));
	        
	        GroupLayout gl_panel = new GroupLayout(panel);
	        gl_panel.setHorizontalGroup(
	        	gl_panel.createParallelGroup(Alignment.LEADING)
	        		.addGroup(gl_panel.createSequentialGroup()
	        			.addContainerGap()
	        			.addComponent(lblSelectTargetPicture)
	        			.addGap(47)
	        			.addComponent(imgPanel, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
	        			.addGap(18)
	        			.addComponent(btnBrowse, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
	        			.addContainerGap(22, Short.MAX_VALUE))
	        );
	        gl_panel.setVerticalGroup(
	        	gl_panel.createParallelGroup(Alignment.LEADING)
	        		.addGroup(gl_panel.createSequentialGroup()
	        			.addContainerGap()
	        			.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
	        				.addComponent(lblSelectTargetPicture)
	        				.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
	        					.addComponent(btnBrowse, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
	        					.addComponent(imgPanel, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
	        			.addContainerGap(169, Short.MAX_VALUE))
	        );
	        panel.setLayout(gl_panel);
	        GroupLayout groupLayout = new GroupLayout(this);
	        groupLayout.setHorizontalGroup(
	        	groupLayout.createParallelGroup(Alignment.LEADING)
	        		.addComponent(panel, GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
	        );
	        groupLayout.setVerticalGroup(
	        	groupLayout.createParallelGroup(Alignment.LEADING)
	        		.addComponent(panel, GroupLayout.PREFERRED_SIZE, 134, Short.MAX_VALUE)
	        );
	        setLayout(groupLayout);
	    }
	 
	 
	/**
	 * Reads a File (user chosen image), converts it to an image and bytearrayoutputstream respectively, and pushes to server
	 * @param reference - the file reference chosen by the user
	 * @throws IOException
	 */
    public void setTarget(File reference) throws IOException{
        try {
            targetFile = reference;
            targetImg = ImageHandler.rescale(ImageIO.read(reference), baseSize, baseSize);
        } catch (IOException e) {
        	e.printStackTrace();
        }
        
        File res = new File(ClientModel.clientResourcesFolderPath);
        if(! res.isDirectory()){
        	res.mkdir();
        }
        String savePath = ClientModel.clientResourcesFolderPath + File.separator + model.getProfile().getUsername() + ".png";
        ImageIO.write(targetImg, "png",new File(savePath));
        img.setIcon(new ImageIcon(targetImg));
        //setVisible(true);
                   
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(targetImg,"jpg",baos);
        byte[] image = baos.toByteArray();
        baos.close();
    	MessageBus.getInstance().pushMessage(new UserAvatarMessage(image));
    }
    
    /**
     * Action performed for the browse button, opens file browsing dialog
     * @param evt - the event from the browse button
     */
    
    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt) {
        JFileChooser fc = new JFileChooser(basePath);
        fc.setFileFilter(new JPEGImageFileFilter());
        int res = fc.showOpenDialog(null);
        try {
            if (res == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                setTarget(file);
            }
        } catch (Exception iOException) {
        }
        repaint();
    }
}
