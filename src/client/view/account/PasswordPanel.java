package client.view.account;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JPasswordField;

import client.ClientModel;

import java.awt.Font;


/**
 * Panel containing the functionality to change the user password
 * @author Oscar
 *
 */

public class PasswordPanel extends JPanel {
	public JPasswordField newPassword;
	public JPasswordField repeatNewPassword;
	public PasswordPanel(ClientModel model) {
		JLabel newPasswordLabel = new JLabel("New password");
		newPasswordLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		JLabel repeatNewPasswordLabel = new JLabel("Repeat new password");
		newPasswordLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		newPassword = new JPasswordField();
		repeatNewPassword = new JPasswordField();
		repeatNewPasswordLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		
		JLabel lblChangePassword = new JLabel("Change Password");
		lblChangePassword.setFont(ClientModel.getTheme().BODY_FONT());
		lblChangePassword.setForeground(ClientModel.getTheme().BODY_COLOR());
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblChangePassword)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addGroup(groupLayout.createSequentialGroup()
								.addComponent(newPasswordLabel)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(newPassword, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE))
							.addGroup(groupLayout.createSequentialGroup()
								.addComponent(repeatNewPasswordLabel)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(repeatNewPassword, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(6, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblChangePassword)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(newPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(newPasswordLabel))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(repeatNewPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(repeatNewPasswordLabel))
					.addContainerGap(39, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
	}
}
