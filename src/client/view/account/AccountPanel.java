package client.view.account;


import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import utilities.LocalSettings;
import utilities.msg.MessageBus;
import utilities.msg.messages.ClientMessageMessage;
import utilities.msg.messages.UserLogoutMessage;
import utilities.msg.messages.UserPasswordUpdateMessage;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import client.ClientModel;
import client.view.graphics.JPlayfuelButton;


/**
 * Panel containing the user account setting UI
 *
 */

public class AccountPanel extends JPanel{
	
	 public JPanel panel;
	 public JPanel imgPanel;
	
	
	 private AvatarPanel ava;
	 private PasswordPanel pass;
	 private JScrollPane scrollPane;
	 private JButton btnSaveSettings;
	 private JLabel label;
	 private ClientModel model;
	protected JComboBox comboBox;

	/**
	 * Creates a account panel
	 * @param model - the Client Model 
	 */
	 public AccountPanel(ClientModel model){
		 
		 this.model = model;
		 
		 ava = new AvatarPanel(model);
		 ava.panel.setBorder(null);
		 pass = new PasswordPanel(model);
		 JPanel inScroll = new JPanel();
		 inScroll.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		 inScroll.setLayout(new BoxLayout(inScroll, BoxLayout.Y_AXIS));
		 inScroll.add(pass);
		 
		 JPanel theme = new JPanel();
		 theme.setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		 inScroll.add(theme);
		 
		 JLabel lblColorTheme = new JLabel("Color theme");
		 lblColorTheme.setFont(ClientModel.getTheme().BODY_FONT());
		 lblColorTheme.setForeground(ClientModel.getTheme().BODY_COLOR());

		 
		 comboBox = new JComboBox();
		 comboBox.setModel(new DefaultComboBoxModel(new String[] {"Dark", "Light", "Native"}));
		 comboBox.setSelectedIndex(LocalSettings.getSettings().getTheme());

		 JLabel lblNewLabel = new JLabel("Restart the client for theme changes to take effect.");
		 lblNewLabel.setForeground(ClientModel.getTheme().BODY_COLOR());
		 GroupLayout gl_theme = new GroupLayout(theme);
		 gl_theme.setHorizontalGroup(
		 	gl_theme.createParallelGroup(Alignment.TRAILING)
		 		.addGroup(Alignment.LEADING, gl_theme.createSequentialGroup()
		 			.addContainerGap()
		 			.addComponent(lblColorTheme, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
		 			.addPreferredGap(ComponentPlacement.RELATED)
		 			.addGroup(gl_theme.createParallelGroup(Alignment.LEADING)
		 				.addComponent(lblNewLabel)
		 				.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
		 			.addContainerGap(37, Short.MAX_VALUE))
		 );
		 gl_theme.setVerticalGroup(
		 	gl_theme.createParallelGroup(Alignment.TRAILING)
		 		.addGroup(gl_theme.createSequentialGroup()
		 			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		 			.addGroup(gl_theme.createParallelGroup(Alignment.BASELINE)
		 				.addComponent(lblColorTheme)
		 				.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
		 			.addPreferredGap(ComponentPlacement.RELATED)
		 			.addComponent(lblNewLabel)
		 			.addContainerGap())
		 );
		 gl_theme.setAutoCreateContainerGaps(true);
		 theme.setLayout(gl_theme);
		 inScroll.add(ava);
		 setBackground(ClientModel.getTheme().MAIN_BACKGROUND());
		 btnSaveSettings = new JPlayfuelButton("");
		 btnSaveSettings.setBorderPainted(false);
		 btnSaveSettings.setIcon(new ImageIcon(AccountPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SaveSmall.png")));
		 btnSaveSettings.setPressedIcon(new ImageIcon(AccountPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SavePressedSmall.png")));
		 btnSaveSettings.setCursor(new Cursor(Cursor.HAND_CURSOR));
		 
		 btnSaveSettings.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		if(!(new String(pass.newPassword.getPassword()).equals(new String(pass.repeatNewPassword.getPassword())))){
		 			MessageBus.getInstance().pushMessage(new ClientMessageMessage("Passwords doesn't match", ClientMessageMessage.Status.FAIL));
		 			return; // Not the prettiest solution.
		 		} else if(new String(pass.newPassword.getPassword()).length() == 0 ) {
		 			// Not setting new password, move on!
		 			;
		 		} else {
		 			MessageBus.getInstance().pushMessage(new UserPasswordUpdateMessage(new String(pass.newPassword.getPassword())));
		 		}	 
		 			
		 		//remove auto-login file TODO: save in settings.json?
	 			File inFile = new File(ClientModel.clientResourcesFolderPath + File.separator + ".credentials");
				
				if (inFile.exists()) {
					inFile.delete();
				}
				
				//theme
				LocalSettings.getSettings().setTheme(comboBox.getSelectedIndex());
				
	 			MessageBus.getInstance().pushMessage(new ClientMessageMessage("Settings updated.", ClientMessageMessage.Status.SUCCESS));
		 	}
		 });
		 String name = "Account Settings for user:  " + model.getProfile().getUsername();
		 label = new JLabel(name);
		 label.setFont(ClientModel.getTheme().HEADER_FONT());
		 label.setForeground(ClientModel.getTheme().BODY_COLOR());
		 
		 JButton btnSignOut = new JPlayfuelButton("");
		 btnSignOut.setCursor(new Cursor(Cursor.HAND_CURSOR));
		 btnSignOut.setPressedIcon(new ImageIcon(AccountPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SignoutPressedSmall.png")));
		 btnSignOut.setBorderPainted(false);
		 btnSignOut.setIcon(new ImageIcon(AccountPanel.class.getResource("/client/view/graphics/image_resources/newButtons/SignoutSmall.png")));
		 btnSignOut.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		File inFile = new File(ClientModel.clientResourcesFolderPath + File.separator + ".credentials");
				
				if (inFile.exists()) {
					inFile.delete();
				}
				
				MessageBus.getInstance().pushMessage(new UserLogoutMessage());
		 	}
		 });
		
		 GroupLayout groupLayout = new GroupLayout(this);
		 groupLayout.setHorizontalGroup(
		 	groupLayout.createParallelGroup(Alignment.LEADING)
		 		.addGroup(groupLayout.createSequentialGroup()
		 			.addContainerGap()
		 			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
		 				.addComponent(inScroll, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 506, Short.MAX_VALUE)
		 				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
		 					.addComponent(label, GroupLayout.PREFERRED_SIZE, 291, GroupLayout.PREFERRED_SIZE)
		 					.addPreferredGap(ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
		 					.addComponent(btnSignOut, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
		 				.addComponent(btnSaveSettings, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE))
		 			.addContainerGap())
		 );
		 groupLayout.setVerticalGroup(
		 	groupLayout.createParallelGroup(Alignment.LEADING)
		 		.addGroup(groupLayout.createSequentialGroup()
		 			.addContainerGap()
		 			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
		 				.addComponent(btnSignOut, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
		 				.addComponent(label, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
		 			.addPreferredGap(ComponentPlacement.RELATED)
		 			.addComponent(inScroll, GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
		 			.addPreferredGap(ComponentPlacement.RELATED)
		 			.addComponent(btnSaveSettings, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
		 			.addContainerGap())
		 );
		 setLayout(groupLayout);
		 
		 
		

		 
	 }
}