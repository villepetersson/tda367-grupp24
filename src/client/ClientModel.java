package client;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;

import client.themes.*;

import com.google.gson.JsonSyntaxException;

import utilities.LocalSettings;
import utilities.ManifestHandler;
import models.Game;

import models.Friend;
import models.Lobby;
import models.Profile;

/**
 * Model keeping track of stuff local to the client.
 *
 */
public class ClientModel {
	private Profile currentProfile;
	private Lobby currentLobby;
	private boolean ownerOfCurrentLobby = false;
	private List<Game> games = new ArrayList<Game>();
	private HashMap<String,Class<?>> gameClasses = new HashMap<String,Class<?>>();
	private HashMap<String,ImageIcon> gameScreens = new HashMap<String, ImageIcon>();
	private HashMap<String,ImageIcon> gameLogos = new HashMap<String, ImageIcon>();
	private String currentGameViewed;
	private List<Friend> lastFriendsList;
	public static String clientResourcesFolderPath = System.getProperty("user.home") + File.separator + ".PlayFuelResources";

	/**
	 * Sets up things needed for the client to function.
	 * More specifically, searches for games in the games folder.
	 */
	public ClientModel() {
		final File folder = new File("./games/");
		
		for (final File fileEntry : folder.listFiles()) {
			try {
				if(fileEntry.getName().startsWith(".DS_")) {
					continue;
				}
				URL[] mfURLs = { fileEntry.toURI().toURL() };
				URLClassLoader mfucl = new URLClassLoader(mfURLs);
				URL mfurl = mfucl.getResource("playfuel.mf");
				
				InputStream in = mfurl.openStream();

				Game game = ManifestHandler.getGameFromManifest(in);
				
		    	Class<?> c;
				c = ManifestHandler.getClassFromURL(fileEntry.toURI().toURL(), game.getGameClientClassName());
				
				gameClasses.put(game.getIdentifier(), c);
				games.add(game);
				
				//SCREENSHOT
				try {
					URL[] urls = { fileEntry.toURI().toURL() };
					URLClassLoader ucl = new URLClassLoader(urls);
					URL url = ucl.getResource("screen.png");
					
					ImageIcon img = new ImageIcon(ImageIO.read(url));
			        gameScreens.put(game.getIdentifier(),img);		
				} catch(Exception e) {
			        gameScreens.put(game.getIdentifier(),null);					
				}
				
		        //LOGO
				try {
					URL[] urls = { fileEntry.toURI().toURL() };
					URLClassLoader ucl = new URLClassLoader(urls);
					URL url = ucl.getResource("logo.png");
					
					ImageIcon img = new ImageIcon(ImageIO.read(url));
			        gameLogos.put(game.getIdentifier(),img);	
				} catch(Exception e) {
			        gameLogos.put(game.getIdentifier(),null);					
				}
				
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			} catch (IOException e) {
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {}
        }
    
	}
	
	/**
	 * @return The lobby the logged in user is currently in, returns null if user
	 * isn't in any lobby.
	 */
	public Lobby getCurrentLobby() {
		return currentLobby;
	}

	/**
	 * Updates which lobby the logged in user currently is in.
	 * @param currentLobby
	 */
	public void setCurrentLobby(Lobby currentLobby) {
		this.currentLobby = currentLobby;
		
		if (this.currentLobby != null) {
			// Set if I am Lobby owner or not
	    	if (this.currentLobby.getSlot(0).getUser().equals(this.currentProfile)) {
	    		this.setOwnerOfCurrentLobby(true);
	    	}
		}
	}
	
	public boolean isOwnerOfCurrentLobby() {
		return ownerOfCurrentLobby;
	}

	private void setOwnerOfCurrentLobby(boolean ownerOfCurrentLobby) {
		this.ownerOfCurrentLobby = ownerOfCurrentLobby;
	}

	public void setProfile(Profile p) {
		this.currentProfile = p;
	}
	
	public Profile getProfile() {
		return this.currentProfile;
	}

	public List<Game> getGames() {
		return games;
	}

	/**
	 * @return a HashMap of game identifiers, and the respective PlayfuelGameClient classes.
	 */
	public HashMap<String, Class<?>> getGameClasses() {
		return gameClasses;
	}

	public void setCurrentGameViewed(String gameIdentifier) {
		this.currentGameViewed = gameIdentifier;
	}
	
	public String getCurrentGameViewed() {
		return this.currentGameViewed;
	}
	
	public void setLastFriendsList(List<Friend> friends) {
		this.lastFriendsList = friends;
	}
	
	public List<Friend> getLastFriendsList() {
		return this.lastFriendsList;
	}

	public ImageIcon getScreenshot(String gameidentifier) {
		return gameScreens.get(gameidentifier);
	}

	public ImageIcon getLogo(String gameidentifier) {
		return gameLogos.get(gameidentifier);
	}	
	
	/**
	 * @return the locally selected theme.
	 */
	public static Theme getTheme() {
		if(LocalSettings.getSettings().getTheme()==1) {
			return new LightTheme();
		} else if(LocalSettings.getSettings().getTheme()==2) {
			return new NativeTheme();
		} else {
			return new DarkTheme();
		}
	}
}
