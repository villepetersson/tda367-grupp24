package client;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.border.LineBorder;

import client.messages.ClientLoginFrameMessage;

import utilities.*;
import utilities.msg.MessageBus;

public class Main extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// Setup UI
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					setUIDefs();
					
					// Create a new connection that holds a threaded connection to the server
					new ClientConnection(LocalSettings.getSettings().getMasterserver());

					// Initialize the application
					new ClientController(new ClientModel());
					
					// Push message to open the LoginFrame
					MessageBus.getInstance().pushMessage(new ClientLoginFrameMessage(true));

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Overrides default UI component styles
	 */
	public static void setUIDefs() {
		UIManager.put("Spinner.background",ClientModel.getTheme().ACCENT_BACKGROUND());
        UIManager.put("Spinner.foreground",ClientModel.getTheme().BODY_COLOR());
        UIManager.put("SpinnerUI", "javax.swing.plaf.basic.BasicSpinnerUI");
        
        UIManager.put("ComboBox.background",new ColorUIResource(ClientModel.getTheme().ACCENT_BACKGROUND()));
        UIManager.put("ComboBox.foreground",ClientModel.getTheme().BODY_COLOR());
        UIManager.put("ComboBox.buttonBackground",new ColorUIResource(ClientModel.getTheme().ACCENT_BACKGROUND()));
        UIManager.put("ComboBox.buttonForeground",new ColorUIResource(ClientModel.getTheme().BODY_COLOR()));
        UIManager.put("ComboBoxUI", "javax.swing.plaf.basic.BasicComboBoxUI");

        UIManager.put("PopupMenu.background",new ColorUIResource(ClientModel.getTheme().ACCENT_BACKGROUND()));
        UIManager.put("PopupMenu.foreground",ClientModel.getTheme().BODY_COLOR());

        UIManager.put("TextField.background",ClientModel.getTheme().ACCENT_BACKGROUND());
        UIManager.put("TextField.foreground",ClientModel.getTheme().BODY_COLOR());
        
        UIManager.put("EditorPane.background",ClientModel.getTheme().ACCENT_BACKGROUND());
        UIManager.put("EditorPane.foreground",ClientModel.getTheme().BODY_COLOR());

        UIManager.put("TextField.border", new LineBorder(ClientModel.getTheme().TEXTBOX_BORDER(),2));
        UIManager.put("PasswordField.background",ClientModel.getTheme().ACCENT_BACKGROUND());
        UIManager.put("PasswordField.foreground",ClientModel.getTheme().BODY_COLOR());
        UIManager.put("PasswordField.border", new LineBorder(ClientModel.getTheme().TEXTBOX_BORDER(),2));
        
		// Linux/GTK specific settings.	
		if(!UIManager.getSystemLookAndFeelClassName().contains("GTK")) {
			UIManager.put("TabbedPane.tabsOverlapBorder",false);
		}
	}
}
