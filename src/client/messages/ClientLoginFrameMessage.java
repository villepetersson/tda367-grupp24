package client.messages;

import models.IMessage;

public class ClientLoginFrameMessage implements IMessage {
	public boolean show;
	
	public ClientLoginFrameMessage() {
		;
	}
	
	public ClientLoginFrameMessage(boolean show) {
		this.show = show;
	}
}
