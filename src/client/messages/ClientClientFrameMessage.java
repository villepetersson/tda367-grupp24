package client.messages;

import models.IMessage;

public class ClientClientFrameMessage implements IMessage {
	public boolean show;
	
	public ClientClientFrameMessage() {
		;
	}
	
	public ClientClientFrameMessage(boolean show) {
		this.show = show;
	}
}
