package client.messages;

import javax.swing.JComponent;

import models.IMessage;


import client.view.modals.Modal;

public class ClientModalMessage implements IMessage{
	
	public Modal modal;
	public int height = 400;
	public int width = 300;
	
	public ClientModalMessage(){
		;
	}
	
	public ClientModalMessage(JComponent content, String header) {
		this.modal = new Modal();
		modal.setContent(content);
		modal.setHeader(header);
	}
	
	public ClientModalMessage(JComponent content, String header, int height, int width) {
		this(content, header);
		this.height = height;
		this.width = width;
	}
	
	public ClientModalMessage(Modal modal) {
		this.modal = modal;
	}
	
	public ClientModalMessage(Modal modal, int height, int width) {
		this(modal);
		this.height = height;
		this.width = width;
	}
}
