package client.messages;

import models.IMessage;

public class ClientCardSwitchMessage implements IMessage {
	private String card;
	
	public ClientCardSwitchMessage() {
		;
	}
	
	public ClientCardSwitchMessage(String card) {
		this.card = card;
	}
	
	public String getCard() {
		return this.card;
	}
}
