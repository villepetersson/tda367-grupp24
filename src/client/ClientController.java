package client;

import java.awt.BorderLayout;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JComponent;
import javax.swing.JPanel;

import api.PlayfuelGameClient;


import client.messages.ClientClientFrameMessage;
import client.messages.ClientLoginFrameMessage;
import client.messages.ClientModalDisposeAllMessage;
import client.messages.ClientModalMessage;
import client.view.ClientFrame;
import client.view.login.LoginFrame;
import client.view.modals.InvitePanel;
import client.view.modals.LogoutPanel;
import client.view.modals.Modal;
import client.view.modals.ResultsPanel;
import models.IMessage;
import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.ConnectToGameHubMessage;
import utilities.msg.messages.LobbySlotInviteReceivedMessage;
import utilities.msg.messages.LobbyUpdatedMessage;
import utilities.msg.messages.MatchResultsMessage;
import utilities.msg.messages.MatchmakingLobbyJoinedMessage;
import utilities.msg.messages.UserIdleMessage;
import utilities.msg.messages.UserLogoutForceMessage;
/**
 * Controller class for handling messages in the client
 * 
 *
 */
public class ClientController implements IMessageHandler {
	
	private ClientModel clientModel;
	private LoginFrame loginFrame;
	private ClientFrame clientFrame;

	
	/**
	 * Creates a new ClientController
	 * @param clientModel
	 */
	public ClientController(ClientModel clientModel) {
		this.clientModel = clientModel;
		
		// Register for messages
		MessageBus.getInstance().register(this, ClientLoginFrameMessage.class);
		MessageBus.getInstance().register(this, ClientClientFrameMessage.class);
		MessageBus.getInstance().register(this, UserLogoutForceMessage.class);
		MessageBus.getInstance().register(this, ConnectToGameHubMessage.class);
		MessageBus.getInstance().register(this, LobbyUpdatedMessage.class);
		MessageBus.getInstance().register(this, LobbySlotInviteReceivedMessage.class);
		MessageBus.getInstance().register(this, MatchmakingLobbyJoinedMessage.class);
		MessageBus.getInstance().register(this, MatchResultsMessage.class);
	}

	@Override
	public void onMessage(IMessage msg) {
		// Change visibility of the LoginFrame
		if (msg instanceof ClientLoginFrameMessage) {
			// If not already exists, create a new one
			if (! (this.loginFrame instanceof LoginFrame)) {
				this.loginFrame = new LoginFrame(this.clientModel);
			}
			// Update visibility
			this.loginFrame.setVisible(((ClientLoginFrameMessage)msg).show);
			
		// Change visibility of the ClientFrame
		} else if (msg instanceof ClientClientFrameMessage) {
			// If not already exists, create a new one
			if (! (this.clientFrame instanceof ClientFrame)) {
				this.clientFrame = new ClientFrame(this.clientModel);
			}
			// Update visibility
			this.clientFrame.setVisible(((ClientClientFrameMessage)msg).show);
			
		// Lobby is updated
		} else if(msg instanceof LobbyUpdatedMessage) {

			// Update the model with the new lobby
			clientModel.setCurrentLobby(((LobbyUpdatedMessage)msg).lobby);
			
		// Reveived slot invite
		} else if (msg instanceof LobbySlotInviteReceivedMessage) {
			final LobbySlotInviteReceivedMessage lobbymsg = (LobbySlotInviteReceivedMessage)msg;
			
			// Build up a modal
			JPanel layout = new JPanel(new BorderLayout());
			InvitePanel panel = new InvitePanel(lobbymsg.game.getGameName(), lobbymsg.lobbyId, lobbymsg.slotid);
			
			layout.add(panel, BorderLayout.CENTER);
						
			// Push the modal message
			MessageBus.getInstance().pushMessage(new ClientModalMessage(layout, "Invite received"));
		
		// The player was matchmaked
		} else if(msg instanceof MatchmakingLobbyJoinedMessage){
			// Push to dispose the modal
			MessageBus.getInstance().pushMessage(new ClientModalDisposeAllMessage());
		
		// User is forced logged out from the server
		} else if (msg instanceof UserLogoutForceMessage) {
			
			UserLogoutForceMessage ulm = (UserLogoutForceMessage)msg;
			
			if (ulm.showModal) {
				// Build a new modal that closes the application on dispose
				final String header = "You have been signed out";
				final LogoutPanel panel = new LogoutPanel(ulm.reason);
				
				// Push the modal with custom onDispose
				MessageBus.getInstance().pushMessage(new ClientModalMessage(new Modal () {
					@Override
					public void onDispose() {
						//TODO: This makes the server disconnect the "new" client as well on logout
						// Remove the ClientFrame
						clientFrame.dispose();
						clientFrame = null;
						// Show the LoginFrame again
						MessageBus.getInstance().pushMessage(new ClientLoginFrameMessage(true));
					}
					@Override
					public String getHeader() {
						return header;
					}
					@Override
					public JComponent getContent() {
						return panel;
					}
				}
				));
			} else {
				// Remove the ClientFrame
				clientFrame.dispose();
				clientFrame = null;
				// Show the LoginFrame again
				MessageBus.getInstance().pushMessage(new ClientLoginFrameMessage(true));
			}
			
		// A match is started!
		} else if(msg instanceof ConnectToGameHubMessage) {
			ConnectToGameHubMessage ctghm = (ConnectToGameHubMessage)msg;

			Class<?> c = clientModel.getGameClasses().get(clientModel.getCurrentLobby().getGame().getIdentifier());
			
			Object obj;
			try {
				obj = c.getConstructor().newInstance();

		    	if(obj instanceof PlayfuelGameClient) {

		    		// The user is no longer in a Lobby
		    		clientModel.setCurrentLobby(null);
					
		    		// Launch the game client
		    		PlayfuelGameClient gameserver = (PlayfuelGameClient)obj;
		    		gameserver.clientLaunchedByPlayfuel(ctghm.ipaddr, ctghm.port, ctghm.password);
		    		
		    	}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
			
		// A match ended
		} else if (msg instanceof MatchResultsMessage) {
			MatchResultsMessage message = (MatchResultsMessage)msg;
			
			// Push message to show the ClientFrame again
			MessageBus.getInstance().pushMessage(new ClientClientFrameMessage(true));
			
			// Create a custom modal and send
			final JPanel showResults = new ResultsPanel(message);
			MessageBus.getInstance().pushMessage(new ClientModalMessage(new Modal () {
				@Override
				public String getHeader() {
					return "Match results";
				}
				@Override
				public JComponent getContent() {
					return showResults;
				}
				@Override
				public void onDispose() {
					// Notify the server that client is not in game anymore
					MessageBus.getInstance().pushMessage(new UserIdleMessage());
				}
			}));
		}
	}

}
