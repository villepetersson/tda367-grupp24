package client.themes;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.ImageIcon;


public class DarkTheme extends LightTheme {

	// BACKGROUNDS
	public Color MAIN_BACKGROUND(){ return new Color(60,60,60); }
	public Color TEXTBOX_BORDER(){ return BRANDING_DARK(); }
	public Color ACCENT_BACKGROUND(){ return new Color(80,80,80); }
	public Color DETAIL_BACKGROUND(){ return new Color(80,80,80); }
	
	// BRANDING COLORS
	public Color BRANDING_LIGHT(){ return new Color(51,161,201); }
	public Color BRANDING_LIGHT_INVERSE(){ return new Color(255,255,255); }
	public Color BRANDING_DARK(){ return new Color(18,102,132); }
	public Color BRANDING_DARK_INVERSE(){ return new Color(255,255,255); }
	
	public Color BODY_COLOR(){ return new Color(200,200,200); }

	
	public Color BACKGROUND(){ return new Color(35,35,35); }
	
	//IMAGES
	public ImageIcon LOADINGIMAGE(){return new ImageIcon(DarkTheme.class.getResource("/client/view/graphics/image_resources/loadingDark.gif"));}
	public ImageIcon LOGO(){return new ImageIcon(DarkTheme.class.getResource("/client/view/graphics/image_resources/UpdatedLogoWhiteSmall.png"));}
	public ImageIcon LOGOSMALL(){return new ImageIcon(DarkTheme.class.getResource("/client/view/graphics/image_resources/UpdatedLogoWhiteSmaller.png"));}
	public ImageIcon LOGOMINI(){return new ImageIcon(LightTheme.class.getResource("/client/view/graphics/image_resources/UpdatedLogoWhiteSmallerst.png"));}

}
