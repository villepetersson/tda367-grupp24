package client.themes;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;

import client.themes.*;

public interface Theme {

	// STATUSES
	public Color SUCCESS();
	public Color SUCCESS_INVERSE();
	
	public Color FAIL();
	public Color FAIL_INVERSE();
	
	public Color WARNING();
	public Color WARNING_INVERSE();
	
	public Color NOTICE();
	public Color NOTICE_INVERSE();
	
	public Color ONLINE();
	public Color OFFLINE();
	public Color BUSY();
	public Color INGAME();
	
	// BACKGROUNDS
	public Color MAIN_BACKGROUND();
	public Color TEXTBOX_BORDER();
	public Color ACCENT_BACKGROUND();
	public Color DETAIL_BACKGROUND();

	
	// BRANDING COLORS
	public Color BRANDING_LIGHT();
	public Color BRANDING_LIGHT_INVERSE();
	public Color BRANDING_DARK();
	public Color BRANDING_DARK_INVERSE();
	
	// FONTS
	public Font HEADER_FONT();
	public Font HEADER_FONT_NOT_BOLD();
	public Font BODY_FONT();
	public Color BODY_COLOR();
	
	public Color BACKGROUND();
	
	//IMAGES
	public ImageIcon LOADINGIMAGE();
	public ImageIcon LOGO();
	public ImageIcon LOGOSMALL();
	public ImageIcon LOGOMINI();



}
