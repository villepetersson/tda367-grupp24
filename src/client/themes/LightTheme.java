package client.themes;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;


public class LightTheme implements Theme {
	// STATUSES
	public Color SUCCESS(){ return new Color(109,195,8); }
	public Color SUCCESS_INVERSE(){ return Color.WHITE; } 
	
	public Color FAIL(){ return new Color(217,48,51); }
	public Color FAIL_INVERSE(){ return Color.WHITE; }
	
	public Color WARNING(){ return new Color(217,48,51); }
	public Color WARNING_INVERSE(){ return Color.WHITE; }
	
	public Color NOTICE(){ return new Color(251,199,1); }
	public Color NOTICE_INVERSE(){ return Color.BLACK; }
	
	public Color ONLINE(){ return new Color(146,179,36); }
	public Color OFFLINE(){ return new Color(217,48,51); }
	public Color BUSY(){ return new Color(251,199,1); }
	public Color INGAME(){ return new Color(251,199,1); }
	
	// BACKGROUNDS
	public Color MAIN_BACKGROUND(){ return new Color(245,245,245); }
	public Color TEXTBOX_BORDER(){ return new Color(220,220,220); }
	public Color ACCENT_BACKGROUND(){ return new Color(235,235,235); }
	public Color DETAIL_BACKGROUND(){ return this.ACCENT_BACKGROUND(); }

	
	// BRANDING COLORS
	public Color BRANDING_LIGHT(){ return new Color(51,161,201); }
	public Color BRANDING_LIGHT_INVERSE(){ return new Color(255,255,255); }
	public Color BRANDING_DARK(){ return new Color(18,102,132); }
	public Color BRANDING_DARK_INVERSE(){ return new Color(255,255,255); }
	
	// FONTS
	public Font HEADER_FONT(){ return new Font("Helvetica", Font.BOLD, 16); }
	public Font HEADER_FONT_NOT_BOLD(){ return new Font("Helvetica", Font.PLAIN, 16); }
	public Font BODY_FONT(){ return new Font("Helvetica", Font.PLAIN, 14); }
	
	public Color BACKGROUND(){ return new Color(160,160,160); }
	
	public Color BODY_COLOR() { return Color.BLACK; }
	
	//IMAGES
	public ImageIcon LOADINGIMAGE(){return new ImageIcon(LightTheme.class.getResource("/client/view/graphics/image_resources/loadingDefault.gif"));}
	public ImageIcon LOGO(){return new ImageIcon(LightTheme.class.getResource("/client/view/graphics/image_resources/UpdatedLogoBlackSmall.png"));}
	public ImageIcon LOGOSMALL(){return new ImageIcon(LightTheme.class.getResource("/client/view/graphics/image_resources/UpdatedLogoBlackSmaller.png"));}
	public ImageIcon LOGOMINI(){return new ImageIcon(LightTheme.class.getResource("/client/view/graphics/image_resources/UpdatedLogoBlackSmallerst.png"));}

}
