package client.themes;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.ImageIcon;

/**
 * Theme which tries to use system colors where possible.
 */
public class NativeTheme extends LightTheme {

	// BACKGROUNDS
	public Color MAIN_BACKGROUND(){ return SystemColor.control; }
	public Color TEXTBOX_BORDER(){ return SystemColor.activeCaption; }
	public Color ACCENT_BACKGROUND(){ return SystemColor.window; }
	
	// BRANDING COLORS
	public Color BRANDING_LIGHT(){ return SystemColor.activeCaption; }
	public Color BRANDING_LIGHT_INVERSE(){ return new Color(255,255,255); }
	public Color BRANDING_DARK(){ return SystemColor.activeCaption; }
	public Color BRANDING_DARK_INVERSE(){ return new Color(255,255,255); }
	
	public Color BACKGROUND(){ return new Color(150,150,150); }
	public ImageIcon LOGOSMALL(){return new ImageIcon(DarkTheme.class.getResource("/client/view/graphics/image_resources/UpdatedLogoWhiteSmaller.png"));}

	// FONTS
	public Font HEADER_FONT(){ return new Font("Arial", Font.BOLD, 16); }
	public Font HEADER_FONT_NOT_BOLD(){ return new Font("Arial", Font.PLAIN, 16); }
	public Font BODY_FONT(){ return new Font("Arial", Font.PLAIN, 14); }

}
