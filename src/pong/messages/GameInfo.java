package pong.messages;

import java.awt.Image;
import java.awt.image.ImageObserver;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameInfo {
	//public int bgr;
	//public int bgb;
	//public int bgg;
	
	public int bgcolor = 0;

	public int thisplayerred = 255;
	public int thisplayerblue = 255;
	public int thisplayergreen = 255;

	public int otherplayerred = 255;
	public int otherplayerblue = 255;
	public int otherplayergreen = 255;
	
	public byte[] thisplayerimage = null;
	public byte[] otherplayerimage = null;

	public Image getThisPlayerImage() throws IOException {
		return ImageIO.read(new ByteArrayInputStream(thisplayerimage));
	}
	
	public Image getOtherPlayerImage() throws IOException {
		return ImageIO.read(new ByteArrayInputStream(otherplayerimage));
	}
	
	public GameInfo() {
		;
	}
}
