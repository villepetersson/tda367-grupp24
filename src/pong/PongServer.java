package pong;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;


import pong.messages.*;

import api.*;
import api.Playfuel.InvalidSettingException;
import api.Playfuel.InvalidSlotException;
import api.Playfuel.MalformedResultsException;
import api.Playfuel.NoSuchPasswordException;

import com.esotericsoftware.kryonet.Connection;
import utilities.Network;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

public class PongServer extends Thread {
	private int WINNING_SCORE = 10;
	public final int PADDLEHEIGHT = 70;
	public final int BALL_SIZE = 10;

	private Point p1Pos;
	private Point p2Pos;
	private Ball ball;
	private int p1Score = 0;
	private int p2Score = 0;
	private Dimension gameSize;
	private Random random = new Random();
	private boolean gameOver;
	private boolean scoringRound;
	private Server server;
	private Connection user1connection;
	private Connection user2connection;

	private int user1red;
	private int user1blue;
	private int user1green;
	
	private int user2red;
	private int user2blue;
	private int user2green;
	
	private Object settingsLock = new Object();

	
	public Playfuel playfuel;
	
	public static void main(String[] args) {
		try {
			new PongServer(new Point(5, 500/2-20), new Point(500-15, 500/2-20),new Point(500/2-5, 500/2), new Dimension(500,500),null).start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public PongServer(Point p1Pos, Point p2Pos, Point ballPos, Dimension gameSize, Playfuel pf) throws IOException{
		//Give default values to positions and stuff
		
		this.gameSize = gameSize;
		this.p1Pos = p1Pos;
		this.p2Pos = p2Pos;

		this.ball = new Ball(ballPos, 10, 5);
		
		server = new Server() {
			protected Connection newConnection () {
				// By providing our own connection implementation, we can store per
				// connection state without a connection ID to state look up.
				return new ClientConnection();
			}
		};

		// For consistency, the classes to be sent over the network are
		// registered by the same method for both the client and server.
		PongNetwork.register(server);
		
		server.addListener(new Listener() {
			public void connected (Connection c) {
			
				/*if (user1connection == null) {
					System.out.println("Player 1 connected");
					user1connection = c;
					((ClientConnection) c).identifier = 1;
				} else {
					System.out.println("Player 2 connected");
					user2connection = c;
					((ClientConnection) c).identifier = 2;
				}*/
			}
			
			
			public void received (Connection c, Object object) {
				synchronized(getLock()) {	
					ClientConnection cc = ((ClientConnection) c);
					
					if(object instanceof String) {
						try {
							int id = playfuel.getSlotWithPassword((String)object);
							
							int ocr=(Integer)playfuel.getValueOfSlotSetting(id,"red");
							int ocg=(Integer)playfuel.getValueOfSlotSetting(id,"green");
							int ocb=(Integer)playfuel.getValueOfSlotSetting(id,"blue");
	
	
							if(id==0) {
								cc.identifier=1; // Player 1.
								System.out.println("Player 1 connected");
								user1connection = c;
								
								user1red=ocr;
								user1green=ocg;
								user1blue=ocb;
							}
							if(id==1) {
								cc.identifier=2; // Player 1.
								System.out.println("Player 2 connected");
								user2connection = c;
								
								user2red=ocr;
								user2green=ocg;
								user2blue=ocb;
							}
							if (user2connection != null && user1connection != null) {
								//User 1.
								{
									GameInfo message = new GameInfo();

									message.bgcolor=(Integer)playfuel.getValueOfGlobalSetting("bgcolor");

									message.thisplayerred=user1red;
									message.thisplayergreen=user1green;
									message.thisplayerblue=user1blue;
									
									message.otherplayerred=user2red;
									message.otherplayergreen=user2green;
									message.otherplayerblue=user2blue;
							        
									user1connection.sendUDP(message);
								}
								// User 2
								{
									GameInfo message = new GameInfo();

									message.bgcolor=(Integer)playfuel.getValueOfGlobalSetting("bgcolor");

									message.thisplayerred=user2red;
									message.thisplayergreen=user2green;
									message.thisplayerblue=user2blue;
					
									message.otherplayerred=user1red;
									message.otherplayergreen=user1green;
									message.otherplayerblue=user1blue;
									
									user2connection.sendUDP(message);
								}
							}
	
							
						} catch (NoSuchPasswordException e) {
							;
						} catch (InvalidSettingException e) {
							e.printStackTrace();
						} catch (InvalidSlotException e) {
							e.printStackTrace();
						}
					}
					if(object instanceof PlayerMovement) {
						int newY = ((PlayerMovement)object).newY;
							
						if(cc.identifier == 1  && Math.abs(newY-getP1Y())==1) {
							setP1Y(newY);
						}
						if(cc.identifier == 2 && Math.abs(newY-getP2Y())==1) {
							setP2Y(newY);
						} 
						
						
						UpdatePaddle message = new UpdatePaddle();
						message.p1y=getP1Y();
						message.p2y=getP2Y();
						broadcastPaddleMove(message);
					}
				}
			}
			
			public void disconnected (Connection c) {
				;
			}
		});
		if(pf==null) {
			server.bind(PongNetwork.port,PongNetwork.port+1);
		} else {
			server.bind(pf.getPortNumber(),pf.getPortNumber()+1);
			WINNING_SCORE = ((Integer)pf.getValueOfGlobalSetting("winscore")).intValue();
		}
		server.start();
	}
	
	public Object getLock() {
		return settingsLock;
	}

	public void setP1Y(int y) {
		this.p1Pos.y=y;
	}
	
	public void setP2Y(int y) {
		this.p2Pos.y=y;
	}
	
	public void broadcastPaddleMove(UpdatePaddle message) {
		if (user1connection != null) {
			user1connection.sendUDP(message);
		}
		if (user2connection != null) {
			int swap;
			swap = message.p1y;
			message.p1y = message.p2y;
			message.p2y = swap;
			user2connection.sendUDP(message);
		}
	}
	
	public void broadcastBallMove(UpdateBall message) {
		if (user1connection != null) {
			user1connection.sendUDP(message);
		}
		if (user2connection != null) {
			message.ballX = 500 - message.ballX - BALL_SIZE;
			user2connection.sendUDP(message);
		}
	}
	
	public void broadcastScores() {
		UpdateScore message=new UpdateScore();
		message.p1Score=p1Score;
		message.p2Score=p2Score;
		
		if (user1connection != null) {
			user1connection.sendUDP(message);
		}
		if (user2connection != null) {
			message.p2Score=p1Score;
			message.p1Score=p2Score;
			user2connection.sendUDP(message);
		}
	}

	public int getP1Y() {
		return this.p1Pos.y;
	}
	public int getP2Y() {
		return this.p2Pos.y;
	}
	
	@Override
	public void run(){
		while(!gameOver){
			updateBall();
			
			UpdateBall message=new UpdateBall();
			message.ballX=(int)this.ball.getX();
			message.ballY=(int)this.ball.getY();
			
			broadcastBallMove(message);
			
			try {
				if(isScoringRound()){
					Thread.sleep(1000);
				}else if(isGameOver()){
					break;
				}else{
					Thread.sleep(20);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	

	public void updateBall(){
		handleCollision();
		ball.updatePosition();
		
		if(ball.getX()<=0){
	            p2Score++;
	            scoringRound=true;
	            if(p2Score==WINNING_SCORE){
	            	gameOver = true;
	            	playerWon(1);
	            }
	            ball.setPosition(gameSize.width/2, gameSize.height/2);
		    	scoringRound=false;
		    	broadcastScores();
		    	
		    	double angle=random.nextDouble()*Math.PI*2;
		    	if(Math.abs(Math.sin(angle))>Math.abs(Math.cos(angle))) {
		    		angle+=Math.PI/2;
		    	}
		    	
	            ball.setAngle(angle);
	            try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	    }else if(ball.getX()>=gameSize.width){
	            p1Score++;
	            scoringRound=true;
	            if(p1Score==WINNING_SCORE){
	            	gameOver = true;
	            	playerWon(0);
	            }
	            
	            ball.setPosition(gameSize.width/2, gameSize.height/2);
		    	scoringRound=false;
		    	broadcastScores();

		    	double angle=random.nextDouble()*Math.PI*2;
		    	if(Math.abs(Math.sin(angle))>Math.abs(Math.cos(angle))) {
		    		angle+=Math.PI/2;
		    	}
		    	
	            ball.setAngle(angle);
	            try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	    }else if(ball.getY()<=0 || ball.getY()>=gameSize.height-10){
	    	ball.setAngle(ball.getAngle()*-1);
	    }
		
		
		//pcs.firePropertyChange("ballUpdate", true, false);
	}
	
	public void playerWon(int player) {
		Map<Integer, Integer> results = new HashMap<Integer, Integer>();
		if(player==0) {
			results.put(0, 1);
			results.put(1, 2);
		} else {
			results.put(0, 2);
			results.put(1, 1);
		}
		try {
			playfuel.matchEndedWithResult(results);
		} catch (MalformedResultsException e) {
			e.printStackTrace();
		}
		user1connection.sendTCP(new MatchOver());
		user2connection.sendTCP(new MatchOver());
	}

	public void handleCollision(){
		if(		ball.getX() <= p1Pos.x+10 && 
				ball.getY()-p1Pos.y>=-BALL_SIZE && 
				ball.getY()-p1Pos.y<=PADDLEHEIGHT+BALL_SIZE &&
				ball.getAngle()>=Math.PI/2 &&
				ball.getAngle()<=3*Math.PI/2) {
			
			System.out.println("traffade p1");
			
			double changeangle = ball.getY()-p1Pos.y-(PADDLEHEIGHT/2);
			changeangle*= 2;
			changeangle/=120; // Effectiveness
			
			ball.setAngle((Math.PI-ball.getAngle())+changeangle);

		}else if(ball.getX()+BALL_SIZE >= p2Pos.x && 
				 ball.getY()-p2Pos.y>=-BALL_SIZE && 
				 ball.getY()-p2Pos.y<=PADDLEHEIGHT+BALL_SIZE &&
				 (ball.getAngle()<=Math.PI/2 ||
				 ball.getAngle()>=3*Math.PI/2)) {
			
			double changeangle = ball.getY()-p2Pos.y-(PADDLEHEIGHT/2);
			changeangle*=-2; // Player 2.
			changeangle/=120; // Effectiveness
			
			ball.setAngle((Math.PI-ball.getAngle())+changeangle);
		}
	}
	

	public int getP1Score() {
		return p1Score;
	}
	
	public int getP2Score() {
		return p2Score;
	}
	public void setP1Score(int score) {
		p1Score=score;
	}
	
	public void setP2Score(int score) {
		p2Score=score;
	}
	
	public boolean isGameOver(){
		return gameOver;
	}
	
	public boolean isScoringRound(){
		return scoringRound;
	}
	
	// This holds per connection state.
	static class ClientConnection extends Connection {
		public int identifier;
	}
}
