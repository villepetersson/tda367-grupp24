package pong;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;

import api.*;


public class PlayfuelServerLauncher implements PlayfuelGameServer{

	@Override
	public void serverLaunchedByPlayfuel(Playfuel playfuel) {
		try {
			PongServer srv = new PongServer(new Point(5, 500/2-20), new Point(500-15, 500/2-20),new Point(500/2-5, 500/2), new Dimension(500,500),playfuel);
			srv.playfuel=playfuel;
			srv.start();
			playfuel.serverIsReady();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
