package pong;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import javax.swing.JFrame;

import pong.messages.GameInfo;
import pong.messages.MatchOver;
import pong.messages.PlayerMovement;
import pong.messages.UpdateBall;
import pong.messages.UpdatePaddle;
import pong.messages.UpdateScore;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;


public class PongController extends Thread implements KeyListener {
	//The model to update
	private PongModel model;
	private Main frame;
	private boolean upPressed;
	private boolean downPressed;
	private boolean wPressed;
	private boolean sPressed;
	private Client client;
	private Connection thisConnection;
	private String password;
	
	public class ClientListener extends Listener {
		public void connected (Connection connection) {
        	System.out.println("-- Successfully connected --");
        	
        	thisConnection=connection;
        	connection.sendTCP(password);
        	
        	initView();
        }

        public void received (Connection connection, Object object) {
        	if(object instanceof UpdateBall) {
        		UpdateBall ball = (UpdateBall)object;
				model.updateBall(ball.ballX, ball.ballY);
			}
        	
        	if(object instanceof GameInfo) {
        		GameInfo info = (GameInfo)object;
        		
        		model.setGameinfo(info);
        	}
        	
        	if(object instanceof UpdatePaddle) {
        		UpdatePaddle paddle = (UpdatePaddle)object;

        		model.setP1Pos(new Point(model.getP1Pos().x,paddle.p1y));

        		model.setP2Pos(new Point(model.getP2Pos().x,paddle.p2y));
			}
        	
        	if(object instanceof UpdateScore) {
        		UpdateScore score = (UpdateScore)object;

        		model.setP1Score(score.p1Score);
        		model.setP2Score(score.p2Score);
        	}
        	
        	if(object instanceof MatchOver) {
        		frame.dispose();
        	}

        }

        public void disconnected (Connection connection) {
            EventQueue.invokeLater(new Runnable() {
                public void run () {
                	//frame.dispose();
                }
            });
        }
	}
	
	public PongController(PongModel model, Main frame, final String address, final int port,String password){
		this.model = model;
		this.frame=frame;
		this.password=password;
		
		client = new Client(50000, 50000);
		client.start();
		
		// For consistency, the classes to be sent over the network are
        // registered by the same method for both the client and server.
        PongNetwork.register(client);
        
		
		client.addListener(new ClientListener());
		
		new Thread("Connect") {
			public void run () {
				try {
					client.connect(5000, address, port,port+1);
				} catch (IOException ex) {
					ex.printStackTrace();
					System.exit(1);
				}
			}
		}.start();
	}
	
	private void initView() {
		PongView view = new PongView(model);
		
		view.setSize(new Dimension(500,500));
		frame.add(view);
				
		view.repaint();
		
		view.addKeyListener(this);
		model.getPropertyChangeSupport().addPropertyChangeListener(view);
		view.requestFocusInWindow();
		frame.pack();
	}
	
	public void keyPressed(KeyEvent event){
		if(event.getKeyCode() == KeyEvent.VK_DOWN){
			downPressed = true;
				
		}
		if(event.getKeyCode() == KeyEvent.VK_UP){
			upPressed = true;
		}
		if(event.getKeyCode() == KeyEvent.VK_W){
			wPressed = true;
				
		}
		if(event.getKeyCode() == KeyEvent.VK_S){
			sPressed = true;
		}
		
		
	}
	
		
	public void keyReleased(KeyEvent event){

		if(event.getKeyCode() == KeyEvent.VK_DOWN){
			downPressed = false;
				
		}
		if(event.getKeyCode() == KeyEvent.VK_UP){
			upPressed = false;
			
		}
		
		if(event.getKeyCode() == KeyEvent.VK_W){
			wPressed = false;
				
		}
		if(event.getKeyCode() == KeyEvent.VK_S){
			sPressed = false;
		}
		
	}


	@Override
	public void keyTyped(KeyEvent e) {
		
	}
	
	
	@Override
	public void run(){
		while(true){
			//model.updateBall();
			if(upPressed){
				model.updatePaddle("up");
			}
			if(downPressed){
				model.updatePaddle("down");
			}
			if(wPressed){
				model.updatePaddle("w");
			}if(sPressed){
				model.updatePaddle("s");
			}
			PlayerMovement message = new PlayerMovement();
			message.newY=model.getP1Pos().y;
			if(thisConnection!=null) {
				thisConnection.sendUDP(message);
			}
			
			try {
				sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	

}
