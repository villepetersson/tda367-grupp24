package pong;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeSupport;
import java.util.Random;

import pong.messages.GameInfo;

public class PongModel {
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	
	private Point p1Pos=new Point(5,250);
	private Point p2Pos= new Point(485,250);
	private int ballx=30;
	private int bally=30;
	private Dimension gameSize;
	private int p1Score = 0;
	private int p2Score = 0;

	private GameInfo gameinfo = new GameInfo();

	
	public void updatePaddle(String command){
		//calculate new values and notify view for repaint

		if(command.equals("down")){
			if(p1Pos.y<500-60){
				p1Pos.y++;
			}
				
		}else if(command.equals("up")){
			if(p1Pos.y>0){
				p1Pos.y--;
			}
		}else if(command.equals("s")){
			if(p2Pos.y<500-60){
				p2Pos.y++;
			}
				
		}else if(command.equals("w")){
			if(p2Pos.y>0){
				p2Pos.y--;
			}
		}
		
		
		pcs.firePropertyChange("paddleUpdate", true, false);
		
	}
	
	
	public PropertyChangeSupport getPropertyChangeSupport(){
		return pcs;
	}

	public int getBallX() {
		return ballx;
	}
	
	public int getBallY() {
		return bally;
	}

	public void updateBall(int x, int y) {
		this.ballx=x;
		this.bally=y;
		
		pcs.firePropertyChange("ballUpdate", true, false);
	}

	public Point getP1Pos() {
		return p1Pos;
	}


	public Point getP2Pos() {
		return p2Pos;
	}


	public int getP1Score() {
		return p1Score;
	}
	
	public int getP2Score() {
		return p2Score;
	}

	public void setP1Pos(Point p1Pos) {
		this.p1Pos = p1Pos;
		
		pcs.firePropertyChange("paddleUpdate", true, false);
		
	}


	public void setP2Pos(Point p2Pos) {
		this.p2Pos = p2Pos;
		
		pcs.firePropertyChange("paddleUpdate", true, false);
	}

	public void setP1Score(int score) {
		p1Score=score;
	}

	public void setP2Score(int score) {
		p2Score=score;
	}


	public GameInfo getGameinfo() {
		return gameinfo;
	}


	public void setGameinfo(GameInfo gameinfo) {
		this.gameinfo = gameinfo;
	}

}
