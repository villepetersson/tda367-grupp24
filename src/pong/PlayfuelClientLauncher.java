package pong;

import javax.swing.JFrame;

import api.PlayfuelGameClient;


public class PlayfuelClientLauncher implements PlayfuelGameClient {

	@Override
	public void clientLaunchedByPlayfuel(String host, int ip, String password) {
		Main frame = new Main("PONG");
		frame.setLocation(430, 150);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		PongModel model = new PongModel();
		PongController controller = new PongController(model,frame,host,ip,password);
		controller.start();
	}

}
