package pong;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;

public class PongView extends JPanel implements PropertyChangeListener {
	private PongModel model;
	private JLabel lblNewLabel;
	//private Font font;
	//private Font unSizedFont;
	
	public PongView(PongModel model){
		this.model = model;
		
		lblNewLabel = new JLabel("GAME OVER");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Helvetica", Font.BOLD, 30));
		lblNewLabel.setBackground(Color.BLACK);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(134)
					.addComponent(lblNewLabel)
					.addContainerGap(133, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(94)
					.addComponent(lblNewLabel)
					.addContainerGap(175, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
		lblNewLabel.setVisible(false);
		
	}
	
	public void paintComponent(Graphics g){
		Color bgcolor;
		switch(model.getGameinfo().bgcolor) {
    		case 0:
    			bgcolor=Color.RED;
    			break;
    			
    		case 1:
    			bgcolor=Color.BLUE;
    			break;
    		case 2:
    			bgcolor=Color.GREEN;
    			break;
    		case 3:
    			bgcolor=Color.PINK;
    			break;
    			
    		default:
    			bgcolor=Color.BLACK;
    				
		}
		g.setColor(bgcolor);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		//g.setFont(font);
		g.setFont(new Font("Helvetica", Font.BOLD, 30));
		g.setColor(Color.WHITE);
		
		//middle line
		g.drawLine(this.getWidth()/2, 0, this.getWidth()/2, this.getHeight());
		
		
		
		//Ball
		g.fillRect((int)model.getBallX(), (int)model.getBallY(), 10, 10);
		
		//Scores
		g.drawString(model.getP1Score()+"", this.getWidth()/2-35, 25);
		g.drawString(model.getP2Score()+"", this.getWidth()/2+20, 25);

		//Paddles
		g.setColor(new Color(model.getGameinfo().thisplayerred,model.getGameinfo().thisplayergreen,model.getGameinfo().thisplayerblue));
		g.fillRect(5, model.getP1Pos().y, 10, 70);
		
		g.setColor(new Color(model.getGameinfo().otherplayerred,model.getGameinfo().otherplayergreen,model.getGameinfo().otherplayerblue));
		g.fillRect((this.getWidth()-15), model.getP2Pos().y, 10, 70);
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		repaint();
	}
}
