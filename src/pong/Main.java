package pong;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import api.*;


@SuppressWarnings("serial")
public class Main extends JFrame {
	
	public Main(String string){
		super(string);
		if(System.getProperty("os.name").equals("Mac OS X")){
			this.setSize(new Dimension(500, 520));
			this.setPreferredSize(new Dimension(500, 520));

		}else{
			this.setSize(new Dimension(500, 500));
			this.setPreferredSize(new Dimension(500, 500));
		}
		this.setResizable(false);
		this.setBackground(Color.BLACK);
		
	} 
	
	public static void main(String[] args) {
		Main frame = new Main("PONG");
		frame.setLocation(430, 150);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.requestFocus();
		
		
		PongModel model = new PongModel();
		PongController controller = new PongController(model, frame,"localhost",PongNetwork.port,"");
		controller.start();
	}

}
