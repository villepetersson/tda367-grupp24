
package pong;

import java.awt.Color;

import pong.messages.*;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

// This class is a convenient place to keep things common to both the client and server.
public class PongNetwork {
	static public final int port = 54559;

	// This registers objects that are going to be sent over the network.
	static public void register (EndPoint endPoint) {
		Kryo kryo = endPoint.getKryo();
		
		// General
		kryo.register(Object.class);
		kryo.register(PlayerMovement.class);
		kryo.register(UpdatePaddle.class);
		kryo.register(UpdateBall.class);
		kryo.register(UpdateScore.class);
		kryo.register(GameInfo.class);
		kryo.register(Color.class);
		kryo.register(MatchOver.class);
		kryo.register(byte[].class);
		
	}
}
