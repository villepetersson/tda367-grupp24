package pong;

import java.awt.Point;

public class Ball {
	private double x;
	private double y;
	private double angle=0;
	private double speed=2;
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getAngle() {
		return angle;
	}
	
	public void setAngle(double angle) {
		angle+=Math.PI*2; // Make sure angle never is negative.
		this.angle=angle%(Math.PI*2);
	}
	
	public void updatePosition() {
		x+=(Math.cos(angle)*speed);
		y+=(Math.sin(angle)*speed);
	}
	
	public Ball(Point position, double angle, double speed) {
		this.x=position.x;
		this.y=position.y;
		this.angle = angle;
		this.speed = speed;
	}

	public void setPosition(double x, double y) {
		this.x=x;
		this.y=y;
	}

	
}
