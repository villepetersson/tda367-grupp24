package utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import models.*;

import utilities.msg.messages.*;
import utilities.msg.messages.ClientMessageMessage.Status;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

/**
 *  This class is a convenient place to keep things common to both the client and server.
 */
public class Network {
	/**
	 * Port for Client-Masterserver connections.
	 */
	static public final int port = 54555;

	/**
	 *  This registers objects that are going to be sent over the network
	 *  so that Kryo is able to serialize them.
	 * @param endPoint
	 */
	static public void register (EndPoint endPoint) {
		Kryo kryo = endPoint.getKryo();
		
		// General
		kryo.register(Object.class);
		kryo.register(String[].class);
		kryo.register(byte[].class);
		kryo.register(ArrayList.class);
		kryo.register(Map.class);
		kryo.register(HashMap.class);
		kryo.register(List.class);
		kryo.register(Setting.class);
		kryo.register(Lobby.class);
		kryo.register(Slot.class);
		kryo.register(ImageIcon.class);
		kryo.register(Double.class);
		kryo.register(Integer.class);
		kryo.register(ClientMessageMessage.class);
		kryo.register(Status.class);



		
		
				
		// Lobby
		kryo.register(LobbyCreateMessage.class);
		kryo.register(LobbyUpdatedMessage.class);
		kryo.register(LobbySlotInviteMessage.class);
		kryo.register(LobbySlotInviteAcceptMessage.class);
		kryo.register(LobbySlotInviteDeclineMessage.class);
		kryo.register(LobbySlotUninviteMessage.class);
		kryo.register(LobbySettingsUpdateMessage.class);
		kryo.register(LobbySlotSettingsUpdateMessage.class);
		kryo.register(UserSetReadyMessage.class);
		kryo.register(UserIdleMessage.class);
		kryo.register(MatchmakingLobbyJoinedMessage.class);

		kryo.register(UserJoinQueueMessage.class);
		kryo.register(UserExitQueueMessage.class);

		// Users
		kryo.register(Profile.class);
		kryo.register(Friend.class);
		kryo.register(UserLoginMessage.class);
		kryo.register(UserLoginSuccessMessage.class);
		kryo.register(UserLoginFailedMessage.class);
		kryo.register(UserLogoutMessage.class);
		kryo.register(UserLogoutForceMessage.class);
		kryo.register(UserRegisterMessage.class);
		kryo.register(UserRegisterSuccessMessage.class);
		kryo.register(UserRegisterFailedMessage.class);
		kryo.register(UserSearchMessage.class);
		kryo.register(UserSearchResultsMessage.class);
		kryo.register(UserFriendAddMessage.class);
		kryo.register(UserFriendListMessage.class);
		kryo.register(UserFriendListGetMessage.class);
		kryo.register(UserFriendAcceptMessage.class);
		kryo.register(UserFriendRemoveMessage.class);
		kryo.register(UserUpdateLobbyMessage.class);
		kryo.register(LobbySlotInviteReceivedMessage.class);
		kryo.register(LobbySlotLeaveMessage.class);
		kryo.register(UserLaunchLobbyMessage.class);
		kryo.register(UserAvatarMessage.class);
		kryo.register(UserPasswordUpdateMessage.class);
		kryo.register(MatchResultsMessage.class);


		

		
		//Chat
		kryo.register(LobbyChatMessageRecieved.class);
		kryo.register(LobbyChatMessageSend.class);

		
		// GameHub
		kryo.register(GameHubRegisterMessage.class);
		kryo.register(Game.class);
		kryo.register(Game[].class);
		kryo.register(MatchStartedMessage.class);
		kryo.register(MatchStartMessage.class);
		kryo.register(ConnectToGameHubMessage.class);
		kryo.register(MatchEndedWithResultsMessage.class);
		
		// Game
		kryo.register(GameListMessage.class);
		kryo.register(GameListGetMessage.class);
		kryo.register(GameInfoGetMessage.class);
		kryo.register(GameInfoMessage.class);
		kryo.register(GameRankingGetMessage.class);
		kryo.register(GameRankingMessage.class);
		kryo.register(RankingResult.class);
		
	}
}
