package utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Game;
import models.Setting;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Helper class for stuff related to the manifest, such as reading games
 * and populating settings.
 */
public class ManifestHandler {
	/**
	 * Takes a List of Settings representing a settings factory specified in a manifest,
	 * and returns a map with Settings and default values.
	 * @param factory List of Settings representing a settings factory.
	 * @return a map with Settings and default values.
	 */
	public static Map<Setting,Object> populateSettingsWithFactory(List<Setting> factory) {
		return populateSettingsWithFactory(factory, new HashMap<String, String>());
	}
	/**
	 * Takes a List of Settings representing a settings factory specified in a manifest,
	 * and returns a map with Settings and values.
	 * @param factory List of Settings representing a settings factory.
	 * @param seed A map of seed settings identifiers and setting values as strings. 
	 * Input an empty map to use default values.
	 * @return a map with Settings and values.
	 */
	public static Map<Setting,Object> populateSettingsWithFactory(List<Setting> factory, Map<String,String> seed) {
		Map<Setting,Object> settings = new HashMap<Setting, Object>();
		for(Setting setting : factory) {
			String obj;
			if(seed!=null && seed.containsKey(setting.getIdentifier())) {
				obj = seed.get(setting);
			} else {
				obj = setting.getDefault();
			}
			Object value;
			if(setting.getInput().equals("Integer")) {
				value = new Integer(obj);
			} else if(setting.getInput().equals("Boolean")) {
				value = obj.equals("TRUE");
			} else if(setting.getInput().equals("Choice")) {
				value = new Integer(obj);
			} else {
				value = obj;
			}
			
			settings.put(setting, value);
		}
		return settings;
	}
	
	/**
	 * Converts a manifest input stream to a Game.
	 * @param in InputStream with a manifest.
	 * @return Game instance.
	 * @throws IOException
	 */
	public static Game getGameFromManifest(InputStream in) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		
		String line;
		StringBuilder strBuild = new StringBuilder();
		
		while ((line = reader.readLine()) != null) {
			strBuild.append(line);
		}
		
		if (reader != null) {
			reader.close();
		}
		
		Gson gson = new Gson();
		Game game = gson.fromJson(strBuild.toString(),Game.class);
		
		return game;
	}
	
	/**
	 * Recursively loops through specified folder and adds found files to games and gameClasses lists.
	 * @param games List of Games to fill.
	 * @param gameClasses Map of Game identifiers and Classes to launch.
	 * @param isServer Whether or not this method is run on a server or client.
	 * @param folder File or folder to search.
	 */
	public static void listGamesFromFolder(List<Game> games,Map<String,Class<?>> gameClasses,boolean isServer,final File folder) {
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listGamesFromFolder(games,gameClasses,isServer,fileEntry);
	        } else {
				try {
					URL[] mfURLs = { fileEntry.toURI().toURL() };
					URLClassLoader mfucl = new URLClassLoader(mfURLs);
					URL mfurl = mfucl.getResource("playfuel.mf");
				
					InputStream in = mfurl.openStream();

					Game game = ManifestHandler.getGameFromManifest(in);
					
			    	Class<?> c;
			    	if(isServer) {
						c = getClassFromURL(fileEntry.toURI().toURL(), game.getGameServerClassName());
			    	} else {
						c = getClassFromURL(fileEntry.toURI().toURL(), game.getGameClientClassName());
			    	}
					
					gameClasses.put(game.getIdentifier(), c);
					games.add(game);
				} catch (JsonSyntaxException e) {
					e.printStackTrace();
				} catch (IOException e) {
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (NullPointerException e) {}
	        }
	    }
	}
	
	/**
	 * Takes an URL and class name with package (like java.net.URL) and returns the class residing there.
	 * @param url to search.
	 * @param classname name with package (like java.net.URL).
	 * @return Class found.
	 * @throws ClassNotFoundException
	 */
	public static Class<?> getClassFromURL(URL url,String classname) throws ClassNotFoundException {
		URL[] urls = { url };
		URLClassLoader ucl = new URLClassLoader(urls);
		
    	Class<?> c;
		c = ucl.loadClass(classname);
		return c;
	}
}


