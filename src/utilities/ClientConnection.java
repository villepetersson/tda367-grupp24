package utilities;

import java.io.*;

import models.IMessage;

import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.UserLogoutForceMessage;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

/**
 * This is the connection for the Client and the Gamehub to the MasterServer
 */
public class ClientConnection implements IMessageHandler {
	private Client client;
	
	public ClientConnection(String address) {
		// Initialize the actual connection
		init(address, Network.port);	
		
		// Tell the messagebus that we handle outbound messages.
		MessageBus.getInstance().registerToServer(this);
	}

	public void init(final String ip, final int port) {
		System.out.println("-- Initializing connection to master server --");
		client = new Client(500000,500000); // 5 mfin' mehga bajjts.
		client.start();
		
		// For consistency, the classes to be sent over the network are
        // registered by the same method for both the client and server.
        Network.register(client);
		
		client.addListener(new Listener() {
            public void connected (Connection connection) {
            	System.out.println("-- Successfully connected to master server --");
            }

            public void received (Connection connection, Object object) {
            	// Step on the wonderful MessageBus
                if (object instanceof IMessage) {
                	MessageBus.getInstance().pushMessage((IMessage)object);
                    return;
                }
            }

            public void disconnected (Connection connection) {
            	MessageBus.getInstance().pushMessage(new UserLogoutForceMessage("Lost connection to server."));
            }
		});
		
		// The actual connection
		new Thread("Connect") {
			public void run () {
				try {
					client.connect(5000, ip, port);
				} catch (IOException ex) {
					ex.printStackTrace();
					System.exit(1);
				}
			}
		}.start();
	}
	
	/**
	 * Convenience method for knowing if the connection is active or not
	 * @return connection status
	 */
	public boolean isConnected() {
		return this.client.isConnected();
	}
	@Override
	public void onMessage(IMessage msg) {
		// If a msg that connection subscribed to, send it to the server
		client.sendTCP(msg);
	}
}