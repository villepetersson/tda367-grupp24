package utilities;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class ImageHandler {
	
	
	
	/**
	 * Rescales a BufferedImage to the provided width and height
	 * @param originalImage
	 * @param width
	 * @param height
	 * @return resized BufferedImage;
	 */
	public static BufferedImage rescale(BufferedImage originalImage, int width, int height) {
		int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
		BufferedImage resizedImage = new BufferedImage(width, height , type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        return resizedImage;
    }
	
	
	/**
	 * Converts a ImageIcon to a bufferedImage by painting 
	 * @param icon
	 * @return BufferedImage
	 */
	public static BufferedImage makeBufferedImage(ImageIcon icon){
		BufferedImage image = new BufferedImage(
			    icon.getIconWidth(),
			    icon.getIconHeight(),
			    BufferedImage.TYPE_INT_RGB);
		Graphics g = image.createGraphics();
		// paint the Icon to the BufferedImage.
		icon.paintIcon(null, g, 0,0);
		g.dispose();
		
		return image;
	}



}







