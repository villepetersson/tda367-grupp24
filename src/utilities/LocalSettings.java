package utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Class responsible for fetching and writing local client settings.
 */
public class LocalSettings {
	private String masterserver;
	private int theme;
	
	/**
	 * Empty constructor, for Gson marshalling.
	 */
	private LocalSettings(){};

	private static final String DEFAULT_SETTINGS = "{\n" +
			"\t\"masterserver\":\"localhost\",\n" +
			"\t\"theme\":0\n" +
			"}";
	
	/**
	 * Fetches the current local settings object.
	 * Saves the default settings if none are found. Handle this class
	 * as if it were a singleton.
	 * @return LocalSettings object.
	 */
	public static LocalSettings getSettings() {
		String path = System.getProperty("user.home") + File.separator + ".PlayFuelResources" + File.separator + "settings.json";
		File inFile = new File(path);
		Gson gson = new Gson();
		
		// If there are no settings, write the default ones.
		if (!inFile.exists()) {
			File res = new File(System.getProperty("user.home") + File.separator + ".PlayFuelResources");
			if(! res.isDirectory()){
	        	res.mkdir();
	        }
			FileWriter outFile;
			try {
				outFile = new FileWriter(path);
				PrintWriter out = new PrintWriter(outFile);
				
				out.println(DEFAULT_SETTINGS);
				
				out.close();
				
				outFile.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		try {
			FileReader inFileReader = new FileReader(new File(path));
			BufferedReader br = new BufferedReader(inFileReader);
			
			StringBuilder settingsString = new StringBuilder();
			String line = null;
			while((line = br.readLine()) != null) {
				settingsString.append(line);
			}
			
			inFileReader.close();
			
			return gson.fromJson(settingsString.toString(), LocalSettings.class);

		} catch (Exception e) {
			e.printStackTrace();
			// The file was found, but faulty, print some errors and use the default settings.
			return gson.fromJson(DEFAULT_SETTINGS, LocalSettings.class);
		}
	}

	/**
	 * Fetches the masterserver IP as String.
	 * @return masterserver IP as String
	 */
	public String getMasterserver() {
		return masterserver;
	}

	/**
	 * Fetches an int representing a user client theme.
	 * Which int represents which theme is decided in ClientModel.
	 * @return an int representing a user client theme.
	 */
	public int getTheme() {
		return theme;
	}

	/**
	 * Set the theme identifier.
	 * @param theme identifier as int.
	 */
	public void setTheme(int theme) {
		this.theme = theme;
		LocalSettings.updateSettings(this);
	}
	
	/**
	 * Saves a LocalSettings instance as current client settings,
	 * and saves those to disk.
	 * @param settings
	 */
	public static void updateSettings(LocalSettings settings) {
		String path = System.getProperty("user.home") + File.separator + ".PlayFuelResources" + File.separator + "settings.json";
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		

		File res = new File(System.getProperty("user.home") + File.separator + ".PlayFuelResources");
		if(! res.isDirectory()){
        	res.mkdir();
        }
		FileWriter outFile;
		try {
			outFile = new FileWriter(path);
			PrintWriter out = new PrintWriter(outFile);
			
			out.println(gson.toJson(settings));
			
			out.close();
			
			outFile.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
