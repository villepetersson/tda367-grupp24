package utilities.msg.messages;

import models.IMessage;

public class UserExitQueueMessage implements IMessage.ToServer {
	public String filename;
	public UserExitQueueMessage() {
		;
	}	
	
	public UserExitQueueMessage(String filename) {
		this.filename=filename;
	}
}
