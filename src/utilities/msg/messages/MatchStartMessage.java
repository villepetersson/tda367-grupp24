package utilities.msg.messages;

import models.IMessage;
import models.Lobby;

public class MatchStartMessage implements IMessage.FromServer {
	public Lobby lobby;

	public MatchStartMessage(){
		;
	}
	
	public MatchStartMessage(Lobby lobby){
		this.lobby=lobby;
	}
}
