package utilities.msg.messages;

import models.IMessage;

public class UserPasswordUpdateMessage implements IMessage.ToServer {
	
	public String newPassword;
	
	public UserPasswordUpdateMessage(){
		;
	}
	
	public UserPasswordUpdateMessage(String password){
		this.newPassword = password;
	}
}
