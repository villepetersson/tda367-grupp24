package utilities.msg.messages;

import models.IMessage;

/**
 * Used to sign out a user from the client
 * @author Joakim
 *
 */
public class UserLogoutMessage implements IMessage.ToServer {	
	public UserLogoutMessage() {
		;
	}
}
