package utilities.msg.messages;

import models.IMessage;

public class GameInfoMessage implements IMessage.FromServer {
	public String name;
	public String description;
	public String identifier;
	
	public GameInfoMessage(){
		
	}
	
	public GameInfoMessage(String name, String description,String identifier){
		this.name = name;
		this.description = description;
		this.identifier = identifier;
	}
	
}
