package utilities.msg.messages;

import models.IMessage;

public class UserLoginFailedMessage implements IMessage.FromServer {
	public String username;
	
	public UserLoginFailedMessage() {
		;
	}
	
	public UserLoginFailedMessage(String username) {
		this.username = username;
	}
}
