package utilities.msg.messages;

import models.IMessage;

public class UserRegisterFailedMessage implements IMessage.FromServer {
	private String username;
	
	public UserRegisterFailedMessage() {
		;
	}
	
	public UserRegisterFailedMessage(String u) {
		this.username = u;
	}

	public String getUsername() {
		return this.username;
	}
}
