package utilities.msg.messages;

import models.IMessage;

public class UserJoinQueueMessage implements IMessage.ToServer {
	public String gameIdentifier;
	public UserJoinQueueMessage() {
		;
	}	
	
	public UserJoinQueueMessage(String gameIdentifier) {
		this.gameIdentifier=gameIdentifier;
	}
}
