package utilities.msg.messages;

import models.IMessage;
import models.Lobby;

public class LobbyUpdatedMessage implements IMessage.FromServer {
	public Lobby lobby;

	public LobbyUpdatedMessage() {
		;
	}
	public LobbyUpdatedMessage(Lobby lobby) {
		this.lobby=lobby;
	}
	
}
