package utilities.msg.messages;

import models.IMessage;

public class LobbySlotUninviteMessage implements IMessage.ToServer {
	public int slotid;
	
	public LobbySlotUninviteMessage(){
		;
	}
	
	public LobbySlotUninviteMessage(int slotid){
		this.slotid = slotid;
	}
}
