package utilities.msg.messages;

import models.IMessage;

public class UserLaunchLobbyMessage implements IMessage.ToServer {
	public int lobbyid;
	public UserLaunchLobbyMessage(){;}
	public UserLaunchLobbyMessage(int lobbyid){this.lobbyid=lobbyid;}
}
