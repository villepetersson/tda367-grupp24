package utilities.msg.messages;

import models.IMessage;
import models.Lobby;

public class UserUpdateLobbyMessage implements IMessage.ToServer {
	public Lobby lobby;

	public UserUpdateLobbyMessage() {
		;
	}
	public UserUpdateLobbyMessage(Lobby lobby) {
		this.lobby=lobby;
	}
	
}
