package utilities.msg.messages;

import models.IMessage;

public class LobbySlotInviteDeclineMessage implements IMessage.ToServer {
	public int lobbyid;
	public int slotid;
	
	public LobbySlotInviteDeclineMessage(){
		;
	}
	
	public LobbySlotInviteDeclineMessage(int lobbyid,int slotid) {
		this.lobbyid = lobbyid;
		this.slotid = slotid;
	}
}
