package utilities.msg.messages;

import models.IMessage;

public class UserSearchMessage implements IMessage.ToServer {
	private String query;
	
	public UserSearchMessage() {
		;
	}
	
	public UserSearchMessage(String u) {
		this.query = u;
	}

	public String getQuery() {
		return this.query;
	}
}
