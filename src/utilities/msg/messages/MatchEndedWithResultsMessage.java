package utilities.msg.messages;

import java.util.Map;

import models.IMessage;


public class MatchEndedWithResultsMessage implements IMessage.ToServer {
	public int lobbyid;
	/**
	 * <SlotID, Placement>
	 */
	public Map<Integer, Integer> results;
	
	public MatchEndedWithResultsMessage() {
		;
	}
	
	public MatchEndedWithResultsMessage(int lobbyid,
			Map<Integer, Integer> results) {
		this.lobbyid=lobbyid;
		this.results=results;
	}

}
