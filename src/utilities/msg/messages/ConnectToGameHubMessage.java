package utilities.msg.messages;

import models.IMessage;

public class ConnectToGameHubMessage implements IMessage.FromServer {
	public ConnectToGameHubMessage(){;}
	
	public ConnectToGameHubMessage(String ipaddr, int port, int matchid, String password) {
		this.ipaddr = ipaddr;
		this.port = port;
		this.matchid = matchid;
		this.password=password;
	}
	public String ipaddr;
	public int port;
	public int matchid;
	public String password;

}
