package utilities.msg.messages;

import java.util.List;

import models.IMessage;
import models.Profile;

public class UserSearchResultsMessage implements IMessage.FromServer {
	private List<Profile> profiles;
	
	public UserSearchResultsMessage() {
		;
	}
	
	public UserSearchResultsMessage(List<Profile> results) {
		this.profiles = results;
	}

	public List<Profile> getProfiles() {
		return this.profiles;
	}
}
