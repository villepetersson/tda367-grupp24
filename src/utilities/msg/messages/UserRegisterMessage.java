package utilities.msg.messages;

import models.IMessage;

public class UserRegisterMessage implements IMessage.ToServer {
	private String username;
	private String password;
	private String email;
	
	public UserRegisterMessage() {
		;
	}
	
	public UserRegisterMessage(String u, char[] p, String e) {
		this.username = u;
		this.password = new String(p);
		this.email = e;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public String getEmail() {
		return this.email;
	}
}

