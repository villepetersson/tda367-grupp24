package utilities.msg.messages;

import models.IMessage;

public class UserFriendDeclineMessage implements IMessage.ToServer {
	private String username;
	
	public UserFriendDeclineMessage() {
		;
	}
	
	public UserFriendDeclineMessage(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return this.username;
	}
}
