package utilities.msg.messages;

import models.IMessage;

public class UserFriendAcceptMessage implements IMessage.ToServer {
	private String username;
	
	public UserFriendAcceptMessage() {
		;
	}
	
	public UserFriendAcceptMessage(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return this.username;
	}
}
