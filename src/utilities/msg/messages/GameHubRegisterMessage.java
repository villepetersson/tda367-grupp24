package utilities.msg.messages;

import java.util.ArrayList;
import java.util.List;

import models.Game;
import models.IMessage;

public class GameHubRegisterMessage implements IMessage.ToServer {
	private List<Game> games;
	
	public GameHubRegisterMessage() {
		;
	}
	
	public GameHubRegisterMessage(List<Game> games) {
		this.games = games;
	}

	public List<Game> getGames() {
		return this.games;
	}
}
