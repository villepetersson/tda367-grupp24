package utilities.msg.messages;

import models.IMessage;

public class UserFriendRemoveMessage implements IMessage.ToServer {
	private String username;
	
	public UserFriendRemoveMessage() {
		;
	}
	
	public UserFriendRemoveMessage(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return this.username;
	}
}
