package utilities.msg.messages;

import java.util.Map;

import models.IMessage;
import models.Setting;

public class LobbySettingsUpdateMessage implements IMessage.ToServer {
	public Map<Setting,Object> settings;
	public LobbySettingsUpdateMessage() {
		;
	}	
	
	public LobbySettingsUpdateMessage(Map<Setting,Object> settings) {
		this.settings = settings;
	}
}
