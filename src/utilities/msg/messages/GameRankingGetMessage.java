package utilities.msg.messages;

import models.IMessage;

public class GameRankingGetMessage implements IMessage.ToServer {
	public String gameIdentifier;
	
	public GameRankingGetMessage() {
		;
	}

	public GameRankingGetMessage(String gameIdentifier) {
		this.gameIdentifier = gameIdentifier;
	}
}
