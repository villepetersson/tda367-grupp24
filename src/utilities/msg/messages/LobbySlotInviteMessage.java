package utilities.msg.messages;

import models.IMessage;
import models.Slot;

public class LobbySlotInviteMessage implements IMessage.ToServer {
	public int slotid;
	public String username;
	
	public LobbySlotInviteMessage(){
		;
	}
	
	public LobbySlotInviteMessage(String username, int slotid){
		this.username = username;
		this.slotid = slotid;
	}
}
