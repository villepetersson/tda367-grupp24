package utilities.msg.messages;

import models.IMessage.FromServer;

public class LobbyChatMessageRecieved implements FromServer {
	public String message;
	public String username;
	
	public LobbyChatMessageRecieved(){
		;
	}
	
	public LobbyChatMessageRecieved(String message, String user){
		this.message=message;
		this.username=user;
	}
}
