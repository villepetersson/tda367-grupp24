package utilities.msg.messages;

import models.IMessage;

public class UserLoginMessage implements IMessage.ToServer {
	public String username;
	public String password;
	
	public UserLoginMessage() {
		;
	}
	
	public UserLoginMessage(String username, String password) {
		this.username = username;
		this.password = password;
	}
}
