package utilities.msg.messages;

import models.Game;
import models.IMessage;
import models.Lobby;

public class LobbySlotInviteReceivedMessage implements IMessage.FromServer{
	public int lobbyId;
	public int slotid;
	public Game game;
	
	public LobbySlotInviteReceivedMessage(){
		;
	}
	
	public LobbySlotInviteReceivedMessage(Lobby lobby,int slotid) {
		this.game = lobby.getGame();
		this.lobbyId = lobby.hashCode();
		this.slotid = slotid;
	}
}
