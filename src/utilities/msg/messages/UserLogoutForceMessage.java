package utilities.msg.messages;

import models.IMessage;

/**
 * Used by the server to force the client to sign out
 * @author Joakim
 *
 */
public class UserLogoutForceMessage implements IMessage.FromServer {
	public String reason;
	public boolean showModal;
	
	public UserLogoutForceMessage() {
		;
	}
	
	public UserLogoutForceMessage(String reason) {
		this(reason, true);
	}

	public UserLogoutForceMessage(String reason, boolean showModal) {
		this.reason = reason;
		this.showModal = showModal;
	}	
}
