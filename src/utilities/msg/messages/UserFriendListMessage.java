package utilities.msg.messages;

import java.util.List;

import models.Friend;
import models.IMessage;

public class UserFriendListMessage implements IMessage.FromServer {
	
	private List<Friend> friends;
	
	public UserFriendListMessage() {
		;
	}
	
	public UserFriendListMessage(List<Friend> friends) {
		this.friends = friends;
	}
	
	public List<Friend> getFriends() {
		return this.friends;
	}

}