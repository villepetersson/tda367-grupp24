package utilities.msg.messages;

import models.IMessage;
import models.Profile;

public class UserLoginSuccessMessage implements IMessage.FromServer {
	private Profile profile;
	
	public UserLoginSuccessMessage() {
		;
	}
	
	public UserLoginSuccessMessage(Profile p) {
		this.profile = p;
	}

	public Profile getProfile() {
		return this.profile;
	}
}
