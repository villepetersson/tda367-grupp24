package utilities.msg.messages;

import java.util.Map;

import models.IMessage;
import models.User;

public class MatchResultsMessage implements IMessage.FromServer {

	public Map<String, Double> preResults;
	public Map<String, Double> resultsDiff;
		
	public MatchResultsMessage() {
		;
	}
		
	public MatchResultsMessage(Map<String, Double> preResults, Map<String, Double> resultsDiff) {
		this.preResults = preResults;
		this.resultsDiff = resultsDiff;
	}
		
	
	
}
