package utilities.msg.messages;

import java.util.List;

import models.IMessage;
import models.RankingResult;

public class GameRankingMessage implements IMessage.ToServer {
	
	public String gameIdentifier;
	public List<RankingResult> rankings;
	public GameRankingMessage() {
		;
	}
	
	public GameRankingMessage(String gameIdentifier, List<RankingResult> rankings) {
		this.gameIdentifier = gameIdentifier;
		this.rankings = rankings;
	}
}
