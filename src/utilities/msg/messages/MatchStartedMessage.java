package utilities.msg.messages;

import java.util.HashMap;

import models.IMessage;

public class MatchStartedMessage implements IMessage.ToServer {
	public int lobbyid;
	public int matchid;
	public int port;
	public HashMap<String, Integer> passwords;
	
	public MatchStartedMessage(){;}
	
	public MatchStartedMessage(int lobbyid,int matchid,int port, HashMap<String, Integer> passwords) {
		this.lobbyid=lobbyid;
		this.matchid=matchid;
		this.port=port;
		this.passwords=passwords;
	}
}
