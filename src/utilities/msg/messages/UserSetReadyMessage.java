package utilities.msg.messages;

import models.IMessage;

public class UserSetReadyMessage implements IMessage.ToServer {
	public boolean value;
	public UserSetReadyMessage(){;}
	public UserSetReadyMessage(boolean value){this.value=value;}
}
