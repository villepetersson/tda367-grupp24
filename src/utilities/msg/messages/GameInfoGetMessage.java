package utilities.msg.messages;

import models.IMessage;

public class GameInfoGetMessage implements IMessage.ToServer {
	public String name;
	
	public GameInfoGetMessage(){
		;
	}
	public GameInfoGetMessage(String name){
		this.name = name;
	}
}
