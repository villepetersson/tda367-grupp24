package utilities.msg.messages;

import models.IMessage;

public class ClientMessageMessage implements IMessage.FromServer{
	
	public enum Status {
		SUCCESS,
		FAIL,
		NOTICE
		
	}
	
	public String message;
	public Status status;
	
	public ClientMessageMessage(){
		;
	}
	
	public ClientMessageMessage(String message, Status status){
		this.message = message;
		this.status = status;
	}	
}
