package utilities.msg.messages;

import models.IMessage;

public class UserAvatarMessage implements IMessage.ToServer {
	
	
	//public ImageIcon image;
	public byte[] image;
	
	public UserAvatarMessage(){
		;
	}
	
	public UserAvatarMessage(byte[] image){
		this.image = image;
	}
	
	
}
