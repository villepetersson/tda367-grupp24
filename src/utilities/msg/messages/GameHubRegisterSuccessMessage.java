package utilities.msg.messages;

import models.GameHub;
import models.IMessage;

public class GameHubRegisterSuccessMessage implements IMessage.FromServer {
	private GameHub gameHub;
	
	public GameHubRegisterSuccessMessage() {
		;
	}
	
	public GameHubRegisterSuccessMessage(GameHub gameHub) {
		this.gameHub = gameHub;
	}

	public GameHub getGameHub() {
		return this.gameHub;
	}
}
