package utilities.msg.messages;

import models.IMessage;

public class UserFriendAddMessage implements IMessage.ToServer {
	private String username;
	
	public UserFriendAddMessage() {
		;
	}
	
	public UserFriendAddMessage(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return this.username;
	}
}
