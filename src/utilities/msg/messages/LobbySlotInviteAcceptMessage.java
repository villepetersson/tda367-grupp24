package utilities.msg.messages;

import models.IMessage;

public class LobbySlotInviteAcceptMessage implements IMessage.ToServer {
	public int lobbyid;
	public int slotid;
	
	public LobbySlotInviteAcceptMessage(){
		;
	}
	
	public LobbySlotInviteAcceptMessage(int lobbyid,int slotid) {
		this.lobbyid = lobbyid;
		this.slotid = slotid;
	}
}
