package utilities.msg.messages;

import java.util.Map;

import models.IMessage;
import models.Setting;

public class LobbySlotSettingsUpdateMessage implements IMessage.ToServer {
	public Map<Setting,Object> settings;
	public LobbySlotSettingsUpdateMessage() {
		;
	}	
	
	public LobbySlotSettingsUpdateMessage(Map<Setting,Object> settings) {
		this.settings = settings;
	}
}
