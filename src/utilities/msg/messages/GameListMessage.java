package utilities.msg.messages;

import java.util.List;

import models.Game;
import models.IMessage;

public class GameListMessage implements IMessage.FromServer {
	
	private List<Game> games;
	
	public GameListMessage() {
		;
	}
	
	public GameListMessage(List<Game> games) {
		this.games = games;
	}
	
	public List<Game> getGames() {
		return this.games;
	}

}