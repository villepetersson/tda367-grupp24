package utilities.msg.messages;

import models.IMessage.ToServer;

public class LobbyChatMessageSend implements ToServer {
	public String message;
	public String username;
	public int lobbyID;
	
	public LobbyChatMessageSend() {
		;
	}
	
	public LobbyChatMessageSend(String message, String user, int lobbyID) {
		this.message = message;
		this.username = user;
		this.lobbyID = lobbyID;
	}
}
