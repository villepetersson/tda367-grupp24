package utilities.msg.messages;

import models.IMessage;

public class UserRegisterSuccessMessage implements IMessage.FromServer {
	private String username;
	
	public UserRegisterSuccessMessage() {
		;
	}
	
	public UserRegisterSuccessMessage(String u) {
		this.username = u;
	}

	public String getUsername() {
		return this.username;
	}
}
