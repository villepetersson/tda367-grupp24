package utilities.msg;

import models.IMessage;

/**
 * Interface for classes listening to messages
 *
 */

public interface IMessageHandler {

	
	
	
	/**
	 * Takes action on a messages received, messages a class listens for gets here
	 * @param msg - the message that is received
	 */
	public void onMessage(IMessage msg);
}
