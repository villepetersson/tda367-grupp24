package utilities.msg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import models.IMessage;

/**
 * Orchestrates all the messages inside Client and between client/gamehub and masterserver
 */
public class MessageBus {
	private final static MessageBus instance = new MessageBus();
	private Map<Class<? extends IMessage>,List<IMessageHandler>> receivers;

	private List<IMessageHandler> toServerListeners;
	private List<IMessageHandler> fromServerListeners;
	
	private MessageBus() {
		this.receivers = new HashMap<Class<? extends IMessage>,List<IMessageHandler>>();
		this.toServerListeners=new ArrayList<IMessageHandler>();
		this.fromServerListeners=new ArrayList<IMessageHandler>();
	}
	
	// To use as Singleton
	public synchronized static MessageBus getInstance() {
		return instance;
	}

	// Send this IMessage to all registered listeners
	public void pushMessage(IMessage msg) {
		if (msg instanceof IMessage.ToServer) {
			for(IMessageHandler toServer : toServerListeners) {
				toServer.onMessage(msg);
			}
		}
		if (msg instanceof IMessage.FromServer) {
			for(IMessageHandler fromServer : fromServerListeners) {
				fromServer.onMessage(msg);
			}
		}
		if (this.receivers.containsKey(msg.getClass())) {
			// Clone to avoid concurrent modification exceptions
			List<IMessageHandler> handlers = new ArrayList<IMessageHandler>(this.receivers.get(msg.getClass()));
			for (IMessageHandler handler : handlers) {
				handler.onMessage(msg);
			}
			
		}
	}

	// Register as listener for a specific message
	public void register(IMessageHandler handler, Class<? extends IMessage> msg) {
		if(!this.receivers.containsKey(msg)) {
			List<IMessageHandler> list = new ArrayList<IMessageHandler>();
			list.add(handler);
			this.receivers.put(msg, list);
		} else {
			List<IMessageHandler> list = this.receivers.get(msg);
			if(!list.contains(handler)) {
				list.add(handler);
			}
		}
	}
	
	// Register as listener for messages to the server
	public void registerToServer (IMessageHandler handler) {
		toServerListeners.add(handler);
	}	
	
	// Register as listener for messages for the server
	public void registerFromServer (IMessageHandler handler) {
		fromServerListeners.add(handler);
	}
}
