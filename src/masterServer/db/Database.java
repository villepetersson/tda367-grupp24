package masterServer.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Set;


import masterServer.db.tables.Friendship;
import masterServer.db.tables.GameHub;
import masterServer.db.tables.Lobby;
import masterServer.db.tables.Ranking;
import masterServer.db.tables.User;
import masterServer.matchmaker.Queue;
import models.DatabaseInterface;
import models.Friend;
import models.Game;
import models.RankingResult;
import models.RemoteConnection;

public enum Database implements DatabaseInterface {
	INSTANCE;
    private Connection connection = null;
    private Statement statement;
    
    private User userTbl;
    private Ranking rankingTbl;
    private Friendship friendshipTbl;
    private GameHub gameHubTbl;
    private Lobby lobbyTbl;
    

    /**
     * Method for initialing the connection to the database source
     * For now we are using sqlite
     */
	public void initConnection() {

	    try
	    {
		  Class.forName("org.sqlite.JDBC");
	      // create a database connection
	      connection = DriverManager.getConnection("jdbc:sqlite:playfuel.db");

	      statement = connection.createStatement();
	      statement.setQueryTimeout(30);  // set timeout to 30 sec.
	    }
	    catch(SQLException e) {
	    	System.err.println(e.getMessage());
	    } catch (ClassNotFoundException e) {
	    	System.err.println(e.getMessage());
		}
	    
	    userTbl = new User(statement);
	    friendshipTbl = new Friendship(statement);
	    rankingTbl = new Ranking(statement);
	    gameHubTbl = new GameHub(statement);
	    lobbyTbl = new Lobby(statement);
	}

	/**
	 * Closes the database connection
	 */
	public void closeConnection() {
	    try {
	    	if(connection != null)
	    		connection.close();
	    } catch(SQLException e) {
	    	System.err.println(e.getMessage());
	    }
  	}
	
	/**
	 *  Just defaults setup method to false
	 * @return setup success status
	 */
	public boolean setup() {
		return this.setup(false);
	}
	
	/**
	 * Wrapper method for installing and setting up the different tables in the database
	 * @param force if the install should overwrite current setup
	 * @return setup success status
	 */
	public boolean setup(boolean force) {
		try {
			// Separate into different methods for each db-table
			if (! userTbl.setup(force) || ! friendshipTbl.setup(force) || ! rankingTbl.setup(force)) {
				return false;
			}
			return true;
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Checks if a table exists
	 * @param tablename name of the table
	 * @return if it exists
	 */
	public boolean tableExists(String tablename) {
		DatabaseMetaData dbm;
		try {
			dbm = connection.getMetaData();
			// check if passed table exists
			ResultSet tables = dbm.getTables(null, null, tablename, null);
			
			if (tables.next()) {
				return true;
			}
			else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	// Friends methods
	@Override
	public boolean addFriendRequest(models.User requesting_user, models.User requested_user) {
		return friendshipTbl.create(requesting_user, requested_user, false);
	}
	
	@Override
	public boolean addFriend(models.User requesting_user, models.User requested_user, boolean accepted) {
		return friendshipTbl.create(requesting_user, requested_user, accepted);
	}
	
	@Override
	public boolean acceptFriendRequest(models.User requested_user, models.User requesting_user) {
		return friendshipTbl.accept(requested_user, requesting_user);
	}
	
	@Override
	public boolean declineFriendRequest(models.User requested_user, models.User requesting_user) {
		return friendshipTbl.decline(requested_user, requesting_user);
	}
	
	@Override
	public boolean removeFriend(models.User user1, models.User user2) {
		return friendshipTbl.remove(user1, user2);
	}
	
	// User methods

	@Override
	public List<Friend> findFriendsForUser(models.User user, boolean includeFriendRequests) {
		return userTbl.getFriends(user, includeFriendRequests);
	}
	
	@Override
	public boolean registerUser(models.User u) {
		return userTbl.create(u);
	}
	
	@Override
	public void updateUser(models.User user) {
		userTbl.update(user);
	}
		
	@Override
	public Set<models.User> findUsersByUsername(String query) {
		return userTbl.search(query);
	}
	
	@Override
	public models.User getUserByUsername(String username) {
		return userTbl.getByUsername(username);
	}
	
	@Override
	public models.User getUserByID(int userID) {
		return userTbl.getByID(userID);
	}
	
	@Override
	public Set<models.User> findAllUsers() {
		return userTbl.all();
	}
	
	@Override
	public void removeUserConnection(String username) {
		userTbl.removeConnection(username);
	}
	
	@Override
	public void setUserConnection(String username, RemoteConnection conn) {
		userTbl.setConnection(username, conn);
	}

	@Override
	public RemoteConnection getUserConnection(String username) {
		return userTbl.getConnection(username);
	}

	@Override
	public Map<String, byte[]> getUserImages() {
		return userTbl.getImages();
	}


	// Ranking methods
	
	@Override
	public double getUserRanking(models.User user, String game_identifier) {
		return rankingTbl.getUserRanking(user, game_identifier);
	}
	
	@Override
	public boolean setUserRanking(models.User user, String game_identifier, double ranking) {
		return rankingTbl.setUserRanking(user, game_identifier, ranking);
	}
	
	@Override
	public List<RankingResult> getGameRankings(String gameIdentifier, int limit, String order) {
		return rankingTbl.getGameRankings(gameIdentifier, limit, order);
	}
	
	// GameHub methods

	public void registerGameHub(models.GameHub gameHub, RemoteConnection rc) {
		gameHubTbl.register(gameHub, rc);
	}

	public void unRegisterGameHub(models.GameHub gameHub) {
		gameHubTbl.unregister(gameHub);
	}

	public List<models.GameHub> getGameHubs() {
		return gameHubTbl.findAll();
	}

	public RemoteConnection getGameHubConnection(int gameHubIdentifier) {
		return gameHubTbl.getConnection(gameHubIdentifier);
	}

	public Queue getMatchQueue(Game game) {
		return gameHubTbl.getMatchQueue(game);
	}

	// Lobby methods
	
	public models.Lobby registerLobby(Game game) {
		return lobbyTbl.create(game);
	}

	public models.Lobby getLobbyByID(int lobbyID) {
		return lobbyTbl.getByID(lobbyID);
	}

	public void removeLobby(models.Lobby lobby) {
		lobbyTbl.remove(lobby);
	}
}
