package masterServer.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import models.User;


public class DatabaseSeeder {

	public static void seedAll() {
		DatabaseSeeder.seedUser();
		DatabaseSeeder.seedFriendship();
		DatabaseSeeder.seedRanking();
	}
	
	public static void seedUser() {
		Database.INSTANCE.registerUser(new User("jos", "jos", Database.INSTANCE));
		Database.INSTANCE.registerUser(new User("johan", "johan", Database.INSTANCE));
		Database.INSTANCE.registerUser(new User("johanna", "johanna", Database.INSTANCE));
		Database.INSTANCE.registerUser(new User("Kalle A", "Kalle A", Database.INSTANCE));
		Database.INSTANCE.registerUser(new User("Gordon F", "Gordon F", Database.INSTANCE));
		Database.INSTANCE.registerUser(new User("dondraper", "dondraper", Database.INSTANCE));
		Database.INSTANCE.registerUser(new User("Jack N", "Jack N", Database.INSTANCE));	
		Database.INSTANCE.registerUser(new User("SkillBill", "SkillBill", Database.INSTANCE));	
		Database.INSTANCE.registerUser(new User("Tyler D", "Tyler D", Database.INSTANCE));	
		Database.INSTANCE.registerUser(new User("Mr. T", "Mr. T", Database.INSTANCE));	
		Database.INSTANCE.registerUser(new User("GammelSmurfen", "GammelSmurfen", Database.INSTANCE));	
	}
	
	public static void seedFriendship() {
		User u1 = Database.INSTANCE.getUserByUsername("jos");
		User u2 = Database.INSTANCE.getUserByUsername("johan");
		User u3 = Database.INSTANCE.getUserByUsername("johanna");
		User u5 = Database.INSTANCE.getUserByUsername("Kalle A");
		User u6 = Database.INSTANCE.getUserByUsername("Gordon F"); 
		User u7 = Database.INSTANCE.getUserByUsername("Jack N"); 
		User u8 = Database.INSTANCE.getUserByUsername("SkillBill"); 
		User u9 = Database.INSTANCE.getUserByUsername("Tyler D"); 
		User u10 = Database.INSTANCE.getUserByUsername("Mr. T");
		User u11 = Database.INSTANCE.getUserByUsername("GammelSmurfen"); 
		User u12 = Database.INSTANCE.getUserByUsername("dondraper"); 



		//Add friends to u12
		
		Database.INSTANCE.addFriendRequest(u12, u5);
		Database.INSTANCE.addFriendRequest(u12, u6);
		Database.INSTANCE.addFriendRequest(u12, u7);
		Database.INSTANCE.addFriendRequest(u12, u8);
		Database.INSTANCE.addFriendRequest(u12, u9);
		Database.INSTANCE.addFriendRequest(u12, u10);
		Database.INSTANCE.addFriendRequest(u12, u11);
	
		Database.INSTANCE.acceptFriendRequest(u5, u12);
		Database.INSTANCE.acceptFriendRequest(u6, u12);
		Database.INSTANCE.acceptFriendRequest(u7, u12);
		Database.INSTANCE.acceptFriendRequest(u8, u12);
		Database.INSTANCE.acceptFriendRequest(u9, u12);
		Database.INSTANCE.acceptFriendRequest(u10, u12);
		Database.INSTANCE.acceptFriendRequest(u11, u12);


		
		
		
		// User 1 sends a friend request to user 2
		Database.INSTANCE.addFriendRequest(u1, u2);
		
		// User 1 sends a friend request to user 3
		Database.INSTANCE.addFriendRequest(u1, u3);
		// .. but User 3 declines
		Database.INSTANCE.declineFriendRequest(u3, u1);
		
		// User 2 send a friend request to user 3
		Database.INSTANCE.addFriendRequest(u2, u3);
		// .. and user 3 accepts!
		Database.INSTANCE.acceptFriendRequest(u3, u2);
	}
	
	public static void seedRanking() {
		User u1 = Database.INSTANCE.getUserByUsername("jos");
		User u2 = Database.INSTANCE.getUserByUsername("johan");
		User u3 = Database.INSTANCE.getUserByUsername("johanna");
		User u5 = Database.INSTANCE.getUserByUsername("Kalle A");
		User u6 = Database.INSTANCE.getUserByUsername("Gordon F");
		User u7 = Database.INSTANCE.getUserByUsername("Jack N");
		User u8 = Database.INSTANCE.getUserByUsername("SkillBill");
		User u9 = Database.INSTANCE.getUserByUsername("Tyler D");
		User u10 = Database.INSTANCE.getUserByUsername("Mr. T");
		User u11 = Database.INSTANCE.getUserByUsername("GammelSmurfen");
		User u12 = Database.INSTANCE.getUserByUsername("dondraper");


		
		//Setup some fake ranking
		u1.setRanking("pong.jar", 1200.0);
		u2.setRanking("pong.jar", 1500.0);
		u3.setRanking("pong.jar", 1440.0);
		u5.setRanking("pong.jar", 1234.0);
		u6.setRanking("pong.jar", 1600.0);
		u7.setRanking("pong.jar", 1443.0);
		u8.setRanking("pong.jar", 1245.0);
		u9.setRanking("pong.jar", 1566.0);
		u10.setRanking("pong.jar", 1455.0);
		u11.setRanking("pong.jar", 1337.0);
		u12.setRanking("pong.jar", 2551.0);
		
		
	}
}
