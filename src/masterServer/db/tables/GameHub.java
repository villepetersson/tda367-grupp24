package masterServer.db.tables;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import masterServer.matchmaker.Queue;
import models.Game;
import models.RemoteConnection;

public class GameHub {
	
	private Statement stmnt;
	public int count = 1;
	private Map<Integer, RemoteConnection> gameHubConnections = new HashMap<Integer, RemoteConnection>();
	private List<models.GameHub> gameHubs = new ArrayList<models.GameHub>();
	private Map<String,Queue> matchQueues = new HashMap<String, Queue>();

	public GameHub(Statement statement) {
		this.stmnt = statement;
	}

	public void register(models.GameHub gameHub, RemoteConnection rc) {
		this.gameHubs.add(gameHub);
		this.gameHubConnections.put(count, rc);
		
		
		gameHub.setIdentifier(count);
		System.out.println("-- GameHub " + count + " registered --");
		System.out.println(">> has games:");
		for(Game g : gameHub.getGames()) {
			System.out.println(g.getGameName());
		}
		count++;
	}

	public void unregister(models.GameHub gameHub) {
		if (this.gameHubs.contains(gameHub)) {
			this.gameHubs.remove(gameHub);
			System.out.println("GameHub " + gameHub.getIdentifier() + " unregistered");
		}
	}

	public List<models.GameHub> findAll() {
		return this.gameHubs;
	}

	public RemoteConnection getConnection(int gameHubIdentifier) {
		return this.gameHubConnections.get(gameHubIdentifier);
	}

	public Queue getMatchQueue(Game game) {
		if(!matchQueues.containsKey(game.getGameName())) {
			matchQueues.put(game.getGameName(), new Queue(game));
		}
		return matchQueues.get(game.getGameName());
	}
}
