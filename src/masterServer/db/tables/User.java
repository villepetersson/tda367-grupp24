package masterServer.db.tables;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import masterServer.db.Database;
import models.Friend;
import models.RemoteConnection;

public class User {
	
	public final static String TABLE = "user";
	private Statement stmnt;
	
	// Store these in memory instead of in DB
	// Maps username to image 
	private static Map<String, byte[]> userImages = new HashMap<String, byte[]>();
	// Maps username to the connection thread
	private static Map<String, RemoteConnection> userConnections = new HashMap<String, RemoteConnection>();
	
	public User(Statement statement) {
		this.stmnt = statement;
	}

	/**
	 * Create the table containing users
	 * @param force if the installation should overwrite existing table
	 * @return success status
	 * @throws SQLException
	 */
	public boolean setup(boolean force) throws SQLException {
		if (force || ! Database.INSTANCE.tableExists(User.TABLE)) {
			this.stmnt.executeUpdate("DROP TABLE if exists " + User.TABLE);
			this.stmnt.executeUpdate(
					"CREATE TABLE " + User.TABLE + 
					"(" + 
						"id INTEGER PRIMARY KEY," + 
						"username VARCHAR(50) UNIQUE," + 
						"password VARCHAR(64) NOT NULL," +
						"in_game BIT DEFAULT 0 NOT NULL," +
						"in_lobby INTEGER DEFAULT 0" +
					")"
				);
			return true;
		}
		return false;
	}
	
	/**
	 * Retrieves all friends for passed user
	 * @param user
	 * @param includeFriendRequests if friend requests also should be included in results
	 * @return
	 */
	public List<Friend> getFriends(models.User user, boolean includeFriendRequests) {
		List<Friend> friends = new ArrayList<Friend>();
		try {
			ResultSet rs = this.stmnt.executeQuery(
				"SELECT * " + 
				"FROM " + User.TABLE + " " + 
				"LEFT JOIN " + Friendship.TABLE + " f " +
					"ON (" + User.TABLE + ".id = f.requesting_user) " +
				"WHERE f.requested_user=" + user.getID()
			);
			while(rs.next()) {
				
				for(Friend f : friends) {
					
				}
				
				boolean isAccepted = rs.getBoolean("accepted");
				models.User u = this.createFromDbRecord(rs);
				
				Friend friend = new Friend(u, isAccepted);
				
				friend.setOnline(this.getConnection(u.getUsername()) != null ? true : false);
				friend.setBusy(u.getLobbyID() == 0 ? false : true);
				friend.setInGame(u.isIngame());
				
				friends.add(friend);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return friends;
	}

	/**
	 * Register a new user. A user is identified by username and is always stored in lowercase
	 * @param User new user
	 * @return true if registered, false if failed, probably with username taken
	 */
	public boolean create(models.User u) {
		// Dont allow registering with an existing username
		if (this.getByUsername(u.getUsername()) != null) {
			return false;
		}
		try {
			this.stmnt.executeUpdate(
				"INSERT INTO " + User.TABLE + 
				" (username, password) values('"+u.getUsername().toLowerCase()+"', '"+u.getPassword()+"')"
			);
			return true;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	/**
     * Finds users by provided searchstring
     * @param query
     * @return a set of found users
     */
	public Set<models.User> search(String query) {
		Set<models.User> foundUsers = new HashSet<models.User>();
		try {
			ResultSet rs = this.stmnt.executeQuery(
				"SELECT *" + 
				" FROM " + User.TABLE + 
				" WHERE (username) LIKE ('%" + query.toLowerCase() + "%')" 
			);
			while(rs.next()) {
				foundUsers.add(this.createFromDbRecord(rs));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return foundUsers;
	}

	/**
	 * Finds the user by provided username
     * @param String username
     * @return the found User
     */
	public models.User getByUsername(String username) {
		models.User foundUser = null;
		try {
			ResultSet rs = this.stmnt.executeQuery(
				"SELECT *" + 
				" FROM " + User.TABLE + 
				" WHERE (username)='" + username.toLowerCase() + "'" + 
				" LIMIT 1"
			);
			while(rs.next()) {
				foundUser = this.createFromDbRecord(rs);
				break;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return foundUser;
	}
	
	/**
	 * Finds the user by provided user id
     * @param int userID
     * @return the found User or null if not found
     */
	public models.User getByID(int userID) {
		models.User foundUser = null;
		try {
			ResultSet rs = this.stmnt.executeQuery(
				"SELECT *" + 
				" FROM " + User.TABLE + 
				" WHERE id='" + userID + "'" + 
				" LIMIT 1"
			);
			while(rs.next()) {
				foundUser = this.createFromDbRecord(rs);
				break;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return foundUser;
	}

	/**
	 * Retrieves all users from db
	 * @return set of all users
	 */
	public Set<models.User> all() {
		Set<models.User> users = new HashSet<models.User>();

		try {
			ResultSet rs = this.stmnt.executeQuery("SELECT * FROM " + User.TABLE);
			while(rs.next()) {
				models.User u = this.createFromDbRecord(rs);
				users.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}
	
	/**
	 * Syncs the user object to the database
	 * @param user to be updated in database
	 * @return update success
	 */
	public boolean update(models.User user) {
		try {
			this.stmnt.executeUpdate(
				"UPDATE " + User.TABLE + 
				" SET" +
						" password='" + user.getPassword() + "'," +
						" in_lobby='" + user.getLobbyID() + "'," +
						" in_game='" + (user.isIngame() ? 1 : 0) + "'" +
				" WHERE id='" + user.getID() + "'"
			);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Creates a user in a unified way from a resultset
	 * @param rs results from db query
	 * @return newly create User object
	 * @throws SQLException
	 */
	public models.User createFromDbRecord(ResultSet rs) throws SQLException {
		models.User u = new models.User(rs.getString("username"), rs.getString("password"), Database.INSTANCE);
		
		int lobbyID = rs.getInt("in_lobby");
		
		u.setID(rs.getInt("id"));
		u.setIngame(rs.getBoolean("in_game"));
		u.setLobby(lobbyID);
		
		return u;
	}

	/**
	 * Removes a connection (= kills a user connection)
	 * @param username to remove connection for
	 */
	public void removeConnection(String username) {
		userConnections.remove(username);
	}

	/**
	 * Map a connection to a user
	 * @param username of the user to map the connection to
	 * @param conn the remote connection
	 */
	public void setConnection(String username, RemoteConnection conn) {
		userConnections.put(username, conn);
	}

	/**
	 * Get the mapped remote connection for the user
	 * @param username
	 * @return
	 */
	public RemoteConnection getConnection(String username) {
		return userConnections.get(username);
	}

	/**
	 * Gets all user images
	 * @return user images
	 */
	public Map<String, byte[]> getImages() {
		return this.userImages;
	}
}
