package masterServer.db.tables;

import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import utilities.ManifestHandler;

import models.Game;
import models.Slot;

public class Lobby {
	
	private Statement stmnt;
	
	private static Map<Integer,models.Lobby> listOfLobbies = new HashMap<Integer,models.Lobby>();
	
	// Start on 1 to since lobby = 0 is considered empty for a user object
	static int id = 1;
	
	public Lobby(Statement statement) {
		this.stmnt = statement;
	}

	public models.Lobby create(Game game) {
		models.Lobby lobby = new models.Lobby(game,id++);
		
		lobby.setGlobalSettings(
				ManifestHandler.populateSettingsWithFactory(
						lobby.getGame().getSettingsFactory()
				)
		);
		
		for(int i=0;i<game.getMaxNumberOfSlots();i++) {
			Slot slot = new Slot(i);

			slot.setSettings(
					ManifestHandler.populateSettingsWithFactory(
							lobby.getGame().getSlotSettingsFactory()
					)
			);
			
			lobby.addSlot(slot);
		}
		
		listOfLobbies.put(lobby.hashCode(),lobby);
		return lobby;
	}

	public models.Lobby getByID(int lobbyID) {
		return listOfLobbies.get(new Integer(lobbyID));
	}

	public void remove(models.Lobby lobby) {
		listOfLobbies.remove(lobby);
	}
}
