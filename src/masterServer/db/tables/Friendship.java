package masterServer.db.tables;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import masterServer.db.Database;

public class Friendship {
	
	public final static String TABLE = "friendship";
	private Statement stmnt;
	
	public Friendship(Statement statement) {
		this.stmnt = statement;
	}

	/**
	 * Create the pivot table containing friendships
	 * @param force if the installation should overwrite existing table
	 * @return success status
	 * @throws SQLException
	 */
	public boolean setup(boolean force) throws SQLException {
		if (force || ! Database.INSTANCE.tableExists(Friendship.TABLE)) {
			this.stmnt.executeUpdate("DROP TABLE if exists " + Friendship.TABLE);

			this.stmnt.executeUpdate(
					"CREATE TABLE " + Friendship.TABLE + 
					"(" + 
						"requesting_user INT NOT NULL," + 
						"requested_user INT NOT NULL," + 
						"accepted BIT DEFAULT 0 NOT NULL," + 
						"PRIMARY KEY(requesting_user, requested_user)" + 
						"FOREIGN KEY (requesting_user) REFERENCES " + User.TABLE + "(id)," + 
						"FOREIGN KEY (requested_user) REFERENCES " + User.TABLE + "(id)" +
					")"
				);
			return true;
		}
		return false;
	}

	/**
	 * Create a new friend request
	 * @param models.User requesting_user
	 * @param models.User requested_user
	 * @return success status
	 */
	public boolean create(models.User requesting_user, models.User requested_user, boolean accepted) {
		try {
			int acc = accepted ? 1 : 0;
			this.stmnt.executeUpdate(
					"INSERT INTO " + Friendship.TABLE + 
					" (requesting_user, requested_user, accepted)" +
					" values('" + requesting_user.getID() + "','" + requested_user.getID() + "','" + acc + "')"
				);

			return true;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	/**
	 * Accepts a friend request
	 * @param models.User requested_user is the user that got the friend request, usually "me"
	 * @param models.User requesting_user is the user the friend request is from
	 * @return boolean success
	 */
	public boolean accept(models.User requested_user, models.User requesting_user) {
		try {
			// First change the flag of the existing friend request
			this.stmnt.executeUpdate(
					"UPDATE " + Friendship.TABLE + 
					" SET accepted=1" +
					" WHERE requesting_user='" + requesting_user.getID() + "' AND requested_user='" + requested_user.getID() + "'"
				);
			
			//Add the reverse friendship and immediately set to accepted (maybe not needed)
			this.create(requested_user, requesting_user, true);
			
			return true;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	/**
	 * Decline a friend request
	 * @param models.User requested_user is the user that got the friend request, usually "me"
	 * @param models.User requesting_user is the user the friend request is from
	 * @return boolean success
	 */
	public boolean decline(models.User requested_user, models.User requesting_user) {
		try {
			int deletedRows = this.stmnt.executeUpdate(
					"DELETE FROM " + Friendship.TABLE + 
					" WHERE requesting_user='" + requesting_user.getID() + "' AND requested_user='" + requested_user.getID() + "'"
				);
			// Return true only if a friendship was actually declined (=deleted)
			if (deletedRows > 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	/**
	 * Removes a friendship, both ways
	 * @param models.User user1
	 * @param models.User user2
	 * @return boolean success
	 */
	public boolean remove(models.User user1, models.User user2) {
		try {
			int deletedRows = this.stmnt.executeUpdate(
					"DELETE FROM " + Friendship.TABLE + 
					" WHERE requesting_user IN (" + user1.getID() + ", " + user2.getID() + ") AND requested_user IN (" + user1.getID() + ", " + user2.getID() + ")"
				);
			// Return true only if a friendship was actually declined
			if (deletedRows > 1) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return false;
		}
	}
}
