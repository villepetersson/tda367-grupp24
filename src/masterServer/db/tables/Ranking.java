package masterServer.db.tables;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import masterServer.db.Database;
import models.RankingResult;

public class Ranking {
	
	public final static String TABLE = "ranking";
	private Statement stmnt;
	
	public Ranking(Statement statement) {
		this.stmnt = statement;
	}

	/**
	 * Create the table containing rankings
	 * @param force if the installation should overwrite existing table
	 * @return success status
	 * @throws SQLException
	 */
	public boolean setup(boolean force) throws SQLException {
		if (force || ! Database.INSTANCE.tableExists(Ranking.TABLE)) {
			this.stmnt.executeUpdate("DROP TABLE if exists " + Ranking.TABLE);
			this.stmnt.executeUpdate(
					"CREATE TABLE " + Ranking.TABLE + 
					"(" + 
						"id INTEGER," + 
						"user_id INTEGER KEY," + 
						"game_identifier VARCHAR(100)," +
						"ranking FLOAT," +
						"PRIMARY KEY (id)," +
						"FOREIGN KEY (user_id) REFERENCES " + User.TABLE + "(id)" +
					");"
				);
			// Create index for faster lookup speed
			this.stmnt.executeUpdate(
					//"CREATE INDEX game_identifier_idx ON " + Ranking.TABLE + "(game_identifier);"
				"CREATE INDEX `game_identifier_idx` ON " + Ranking.TABLE + "(`game_identifier` ASC);"
			);
			return true;
		}
		return false;
	}
	
	/**
	 * Gets the ranking for passed user
	 * @param user User model
	 * @param game_identifier is set in the games manifest file
	 * @return the players ranking for this game, 0 if not found
	 */
	public double getUserRanking(models.User user, String game_identifier) {
		double ranking = 0;
		try {
			ResultSet rs = this.stmnt.executeQuery(
				"SELECT *" + 
				" FROM " + Ranking.TABLE + 
				" WHERE (user_id)='" + user.getID() + "'" + 
				" LIMIT 1"
			);
			while(rs.next()) {
				ranking = rs.getDouble("ranking");
				break;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return ranking;
	}
	
	/**
	 * Sets the ranking for a user
	 * @param user to set ranking for
	 * @param game_identifier which game it concerns
	 * @param ranking new ranking
	 * @return success status
	 */
	public boolean setUserRanking(models.User user, String game_identifier, double ranking) {
		try {
			//Check if user has ranking for this game already
			if (this.getUserRanking(user, game_identifier) != 0) {
				// Update existing record
				this.stmnt.executeUpdate(
						"UPDATE " + Ranking.TABLE + 
						" SET ranking=" + ranking +
						" WHERE user_id='" + user.getID() + "' AND game_identifier='" + game_identifier + "'"
					);
			} else {
				// Create a new record
				this.stmnt.executeUpdate(
						"INSERT INTO " + Ranking.TABLE + 
						" (user_id, game_identifier, ranking) values('"+user.getID()+"', '"+game_identifier+"', '"+ranking+"')"
					);
				return true;
			}
			
			return true;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	/**
	 * Gets all the rankings for a game
	 * @param gameIdentifier the unique identifier for a game
	 * @param limit if you dont want to fetch all records at once
	 * @param order if fetching asc or desc
	 * @return
	 */
	public List<RankingResult> getGameRankings(String gameIdentifier, int limit, String order) {
		List<RankingResult> rankings = new ArrayList<RankingResult>();
		
		if (! order.toUpperCase().equals("DESC")) {
			order = "ASC";
		}
		
		try {
			ResultSet rs = this.stmnt.executeQuery(
				"SELECT " + Ranking.TABLE + ".ranking, u.*" +
				" FROM " + Ranking.TABLE +
			    " LEFT JOIN " + User.TABLE + " u" +
					" ON (user_id=u.id)" + 
				" WHERE (game_identifier)='" + gameIdentifier + "'" + 
				" ORDER BY ranking " + order.toUpperCase() +
				" LIMIT " + limit
			);
			while(rs.next()) {
				double rank = rs.getDouble("ranking");
				String username = rs.getString("username");
				// Only get the username, quite confising and bad but the resutset wont let us use the User.createFromDB method...
				RankingResult res = new RankingResult(new models.User(username, username, Database.INSTANCE), rank);
				rankings.add(res);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return rankings;
	}
}
