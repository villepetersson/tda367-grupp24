package masterServer;

import java.io.*;

/**
 * Main starter Class for the entire MasterServer
 * @author Joakim
 * @author Ville
 * @author Oscar
 * @author David
 *
 */
public class MasterMain {
	public static void main(String args[]) throws IOException {
		new MasterServer();
	}
}
