package masterServer.ctrl;

import java.util.ArrayList;
import java.util.List;


import masterServer.db.Database;
import masterServer.matchmaker.Queue;
import models.Game;
import models.GameHub;
import models.RankingResult;
import models.RemoteConnection;

/**
 * Controller class to handle games and gamehubs
 */
public class GameHubController {
	
	/**
	 * Registers a new gamehub to the system
	 * @param gameHub the new gamehub
	 * @param conn the connection to map to this gamehub
	 */
	public static void register(GameHub gameHub, RemoteConnection conn) {
		Database.INSTANCE.registerGameHub(gameHub, conn);
	}
	
	/**
	 * Unregisters a gamehub from the system
	 * @param gameHub to unregister
	 */
	public static void unregister(GameHub gameHub) {
		Database.INSTANCE.unRegisterGameHub(gameHub);
	}
	
	/**
	 * Returns all active gamehubs
	 * @return
	 */
	public static List<GameHub> getGameHubs() {
		return Database.INSTANCE.getGameHubs();
	}
	
	/**
	 * Finds a gamehub with the specified game
	 * @param game to look for gamehub that hosts
	 * @return the found gamehub or null if not found
	 */
	public static GameHub getGameHubWithGame(Game game) {
		//TODO Should check for the "best" hub to join
		for(GameHub gh : getGameHubs()) {
			if(gh.getGames().contains(game)) {
				return gh;
			}
		}
		return null;
	}
	
	/**
	 * Gets a list of all games on all gamehubs
	 * @return list of games
	 */
	public static List<Game> getListOfGames() {
		List<Game> games = new ArrayList<Game>();
		// Itarates over each GameHub and lists all games
        for(GameHub gh : getGameHubs()) {
            for(Game g : gh.getGames()) {
                if (! games.contains(g)) {
                	games.add(g);
                }
            }
        }
        return games;
	}
	
	/**
	 * Finds a game by identifier
	 * @param id game identifier
	 * @return found game or null if not found
	 */
	public static Game getGameWithId(String id) {
		for(Game game : getListOfGames()) {
			if (game.getIdentifier().equals(id)) {
				return game;
			}
		}
		return null;
	}
	
	/**
	 * Returns the connection for the specified gamehub
	 * @param gameHubIdentifier which gamehub to find connection for
	 * @return
	 */
	public static RemoteConnection getConnection(int gameHubIdentifier) {
		return Database.INSTANCE.getGameHubConnection(gameHubIdentifier);
	}

	/**
	 * Gets the matchqueue for specified game
	 * @param gameName to get queue for
	 * @return found Queue or null
	 */
	public static Queue getMatchQueue(Game game) {
		return Database.INSTANCE.getMatchQueue(game);
	}
	
	/**
	 * Get rankings for a game
	 * @param game to find rankings for
	 * @return rankings
	 */
	public static List<RankingResult> getRankings(Game game) {
		return Database.INSTANCE.getGameRankings(game.getIdentifier(), 20, "DESC");
	}
}
