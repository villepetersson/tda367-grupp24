package masterServer.ctrl;

import masterServer.db.Database;
import models.*;

/**
 * Controller class to handle lobbies
 */
public class LobbyController {

	
	// TODO thread safety
	/**
	 * Creates a new lobby based on a Game
	 * @param game that this Lobby is for
	 * @return
	 */
	public static Lobby createLobby(Game game) {
		return Database.INSTANCE.registerLobby(game);
	}
	
	/**
	 * Gets a lobby by lobby id
	 * @param lobbyID
	 * @return the requested lobby or null if it doesnt exist
	 */
	public static Lobby getLobby(int lobbyID) {
		return Database.INSTANCE.getLobbyByID(lobbyID);
	}
	
	/**
	 * Removes a Lobby and all references
	 * @param lobby to be removed
	 */
	public static void remove(Lobby lobby) {
		// First remove all user lobbies
		for (Slot s : lobby.getSlots()) {
			if(s.isOccupied()) {
				User u = UserController.findByUsername(s.getUser().getUsername());
				u.removeLobby();
				u.save();
			}
		}
		// Then remove the actual lobby
		Database.INSTANCE.removeLobby(lobby);
	}
}
