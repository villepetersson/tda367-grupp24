package masterServer.ctrl;

import java.util.HashMap;
import java.util.Map;

import com.esotericsoftware.kryo.Kryo;

import models.User;

/**
 * Controller class to handle games and gamehubs
 * @author Joakim
 */
public class RankingController {
	
	/**
	 * Calculates the probability that User a wins against User b
	 * @param a User to calculate probability 
	 * @param b User opponent
	 * @param game_identifier
	 * @return the probability that a wins over b in a game of game_identifier
	 */
	private static double calcutateWinningProbability(Double userARanking, Double userBRanking) {
		// http://en.wikipedia.org/wiki/Elo_rating_system
		return 1/(1 + Math.pow(10,((userBRanking-userARanking) / 400)) );
	}
	
	/**
	 * Calculates the new ranking based on the probability to win against the opponent
	 * @param username that needs recalculated ranking
	 * @param game_identifier
	 * @param probabilityToWin calculated probability pre-match
	 * @param isWinner if the user won or not
	 * @return the new ranking
	 */
	private static double calculateNewRanking(double old_ranking, double probabilityToWin, boolean isWinner) {
		int W = isWinner ? 1 : 0;
		return old_ranking + 32 * (W - probabilityToWin);
	}
	
	/**
	 * Calculates the new rankings for passed set of users (>2) based on their placement compared to each others
	 * Each player with a lower placement beats every player with a worse (higher) placement
	 * The important value is the relative player placement. If two players have the same placement, nothing happenes between them.
	 * This way you can also calculate team scores by setting the same placement for team members, example:
	 * 
	 * 		userScores.put(playerA, 1);
	 * 		userScores.put(playerB, 1);
	 * 		userScores.put(playerC, 2);
	 * 		userScores.put(playerD, 2);
	 * 
	 * @param userScores Map with user and their placement where lower is better
	 * @param gameIdentifier String that identifies the game
	 */
	public static void calculateRanking(Map<User, Integer> userScores, String gameIdentifier) {
		// We have to keep the pre match rankings not to base any calculations on changed values
		Map<User, Double> preMatchRankings = new HashMap<User, Double>();
		
		// We also store the post match rankings for post comparison
		Map<User, Double> postMatchRankings = new HashMap<User, Double>();
		
		for (Map.Entry<User, Integer> outerEntry : userScores.entrySet()) {
			User currentUser = outerEntry.getKey();
	        
	        // Store pre match ranking
	        preMatchRankings.put(currentUser, currentUser.getRanking(gameIdentifier));

	        // Automatic deep cloning thanks to kryo
	        Map<User, Integer> opponents = new Kryo().copy(userScores);
	        
	        // Remove the player that is currently handled since you are not your own opponent
	        opponents.remove(currentUser);
	        
	        //Iterate over the other players and calculate current players new ranking based on matches with each one
	        for (Map.Entry<User, Integer> innerEntry : opponents.entrySet()) {
		        User opponent = innerEntry.getKey();
		        
		        // Make sure the opponent has its pre match ranking stored
		        if (! preMatchRankings.containsKey(opponent)) {
		        	preMatchRankings.put(opponent, opponent.getRanking(gameIdentifier));
		        }
		        
		        // Only proceed if there was no draw (or they are team members which also have the same placement)
		        if ((Integer) outerEntry.getValue() != (Integer) innerEntry.getValue()) {
		        			        
			        // Check if current user won (or not)
			        boolean currentPlayerWon = (Integer) outerEntry.getValue() < (Integer) innerEntry.getValue() ? true : false;
			        
			        // Calculate the pre-match probabilities
			        double probabilityThatCurrentUserWins = calcutateWinningProbability(
			        		preMatchRankings.get(currentUser), preMatchRankings.get(opponent)
			        	);
			        // Calculate the new rankings
			        double currentUserNewRanking = calculateNewRanking(
			        		currentUser.getRanking(gameIdentifier), probabilityThatCurrentUserWins, currentPlayerWon);
	
			        //Set the new rankings
			        currentUser.setRanking(gameIdentifier, currentUserNewRanking);
			        postMatchRankings.put(currentUser, currentUserNewRanking);
			        
		        }
		    }
	    }

	    // Tweak the scores by divide the diff by # of players to give more just results in multiplayer matches
		if (userScores.size() > 2) {
	        for (Map.Entry<User, Integer> entry : userScores.entrySet()) {
	        	User user = entry.getKey();
	        	
	        	double diff = postMatchRankings.get(user) - preMatchRankings.get(user);
	        	double finalNewRanking = preMatchRankings.get(user) + (diff / (userScores.size() - 1));
	        	user.setRanking(gameIdentifier, finalNewRanking);
	        }
		}
	}
}
