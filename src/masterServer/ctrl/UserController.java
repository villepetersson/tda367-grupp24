package masterServer.ctrl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import utilities.msg.messages.ClientMessageMessage;
import utilities.msg.messages.LobbyUpdatedMessage;
import utilities.msg.messages.UserLogoutForceMessage;

import masterServer.MasterServer;
import masterServer.db.Database;
import models.Friend;
import models.Lobby;
import models.Profile;
import models.RemoteConnection;
import models.User;

/**
 * Controller class to handle users
 */
public class UserController {
	
	/**
	 * Attempts to sign in a user
	 * @param username to sign in with
	 * @param password to authenticate with
	 * @param conn the RemoteConnection that maps to the sign in reqest
	 * @return login success
	 */
	public static boolean login(String username, String password, RemoteConnection conn) {
		User user;
		if ((user = findByUsername(username)) != null && findByUsername(username).getPassword().equals(password)) {
			// If that user already has a connection, force log out
			RemoteConnection rc;
			if ((rc = user.getConnection()) != null) {
				rc.sendMessage(new UserLogoutForceMessage("Your account is beeing used somewhere else"));
			}
			// Map the connection to this user (overrides if had one)
			user.setConnection(conn);
			return true;
		}
		return false;
	}
	
	/**
	 * Registers a new user
	 * @param u user to attempt to register with
	 * @return true if registered, false otherwise
	 */
	public static boolean create(User u) {
		return Database.INSTANCE.registerUser(u);
	}
	
	/**
	 * Gets a single user from DB
	 * @param username to find by
	 * @return found user or null if not found
	 */
	public static User findByUsername(String username) {   
		return Database.INSTANCE.getUserByUsername(username);
	}
	
	/**
	 * Gets a single user from DB
	 * @param user_id to fetch by
	 * @return the found user or null if not found
	 */
	public static User findByID(int user_id) {   
		return Database.INSTANCE.getUserByID(user_id);
	}
	
	/**
	 * Gets all users from the DB
	 * @return all users in the system
	 */
	public static Set<User> findAll() {
		return Database.INSTANCE.findAllUsers();
	}
	
	/**
	 * Search for a user by username
	 * @param query to search for
	 * @param currentUser to know which are friends
	 * @return
	 */
	public static List<Profile> search(String query, User currentUser) {
		List<Profile> searchResults = new ArrayList<Profile>();
		List<Friend> friends = currentUser.getFriends(true);
		
		// Loop though to show friendships
		for(User other : Database.INSTANCE.findUsersByUsername(query)) {
			// Never return current user
			if (other.equals(currentUser)) {
				continue;
			}
			boolean friendFound = false;
			// Check if they are friends
			for(Friend f : friends) {
				if (f.equals(other)) {
					friendFound = true;
					searchResults.add(f);
				}
			}
			if (! friendFound) {
				searchResults.add(new Profile(other));
			}
		}
		return searchResults;
	}

	/**
	 * Gets all user images
	 * @return all user images
	 */
	public static Map<String, byte[]> getImages() {
		return Database.INSTANCE.getUserImages();
	}
	
	public static void notifyLobbyWhenLoggedOut(User user) {
		// Check if user has a Lobby and if is owner and no other players, delete the lobby
		Lobby userLobby = LobbyController.getLobby(user.getLobbyID());
		if (userLobby != null) {
			user.removeLobby();
			user.save();
			// If is Lobby owner, or the lobby is empty, remove the lobby
			if (userLobby.isOwner(new Profile(user)) || userLobby.getPlayers().size() <= 1) {
				// Tell ppl what is happening
				MasterServer.sendToEveryoneInLobby(userLobby, new ClientMessageMessage(user.getUsername() + " disconnected and the lobby was deleted",
						ClientMessageMessage.Status.NOTICE));
				// ..and remove their lobby
				MasterServer.sendToEveryoneInLobby(userLobby, new LobbyUpdatedMessage(null));
				
				// then remove the Lobby
				LobbyController.remove(userLobby);
			// Else notify the others
			} else {
				userLobby.removePlayer(user.getUsername());
				
				// Notify the others still in lobby
				MasterServer.sendToEveryoneInLobby(userLobby, new LobbyUpdatedMessage(userLobby));
				MasterServer.sendToEveryoneInLobby(userLobby, new ClientMessageMessage(user.getUsername() + " disconnected",
						ClientMessageMessage.Status.NOTICE));
			}
		}
	}
}
