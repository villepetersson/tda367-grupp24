package masterServer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import masterServer.ctrl.*;
import masterServer.db.Database;
import masterServer.db.DatabaseSeeder;
import masterServer.exceptions.UserNotLoggedInException;
import models.*;

import utilities.Network;
import utilities.msg.messages.*;
import utilities.msg.messages.ClientMessageMessage.Status;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

public class MasterServer {
	
	private Server server;

	public MasterServer () throws IOException {
		
		System.out.println("-- Master server initiated --");
		
		server = new Server(500000,500000) {
			protected Connection newConnection () {
				// By providing our own connection implementation, we can store per
				// connection objects like users or gamehubs to know which one is related to which connection
				return new RemoteConnection();
			}
		};

		// For consistency, the classes to be sent over the network are
		// registered by the same method for both the client and server.
		Network.register(server);

		// Initialize the database connection
		Database.INSTANCE.initConnection();
		
		// These are the listeners for the Kryonet library
		server.addListener(new Listener() {
			
			// When something was reveived
			public void received (Connection c, Object object) {
				// We know all connections for this server are RemoteConnections so we can safely typecast.
				RemoteConnection connection = (RemoteConnection)c;
				
				try {
					// Check for and setup User or GameHub object
					User currentUser = null;
					GameHub currentGameHub = null;
					if (connection.getObject() instanceof User) {
						currentUser = (User) connection.getObject();
						
						// Make sure we have the latest reference from DB
						User newUserFromDB;
						if ((newUserFromDB = UserController.findByID(currentUser.getID())) != null) {
							currentUser = newUserFromDB;
						}
						
					} else if (connection.getObject() instanceof GameHub) {
						currentGameHub = (GameHub) connection.getObject();
					}
					
					// Java cannot do switch on objects so we have this huge if else instead
					
					/**
					 * USER MESSAGES
					 */
					
					// User login
					if (object instanceof UserLoginMessage) {
						
						UserLoginMessage msg = (UserLoginMessage) object;
												
						// If successful login
						if(UserController.login(msg.username, msg.password, connection)) {
							User signedInUser = UserController.findByUsername(msg.username);

							
							//Map User object to this connection thread
							connection.setObject(signedInUser);
							
							// Notify the user that it signed in
							connection.sendMessage(new UserLoginSuccessMessage(new Profile(signedInUser)));
							
							// Send any queued messages to client
							//signedInUser.sendQueuedMessages();
							
							// Notify all friends that the user 
							MasterServer.sendUpdatedFriendList(signedInUser, 
									new ClientMessageMessage(signedInUser.getUsername() + " is online", ClientMessageMessage.Status.SUCCESS));
							
							// Check if user is in a lobby and in that case send that lobby (should only happen on disconnect)
							Lobby userLobby;
							if ((userLobby = LobbyController.getLobby(signedInUser.getLobbyID())) != null) {
								connection.sendMessage(new LobbyUpdatedMessage(userLobby));
							}
						//Login failed
						} else {
							connection.sendMessage(new UserLoginFailedMessage(msg.username));
						} 
						
					// Register new user
					} else if (object instanceof UserRegisterMessage) {
						UserRegisterMessage msg = (UserRegisterMessage) object;

						// Create a user to try to register
						User attempt = new User(msg.getUsername(), msg.getPassword(), Database.INSTANCE);
						// Success
						if (UserController.create(attempt)) {
							connection.sendMessage(new UserRegisterSuccessMessage(attempt.getUsername()));
						// Failed (probably username existed)
						} else {
							connection.sendMessage(new UserRegisterFailedMessage(attempt.getUsername()));
						}
						
					// User search
					} else if (object instanceof UserSearchMessage) {
						String query = ((UserSearchMessage)object).getQuery();
		
						if (currentUser != null) {						
							connection.sendMessage(new UserSearchResultsMessage(UserController.search(query, currentUser)));
						}
						
					// Message to tell the server that the user is not in match
					} else if (object instanceof UserIdleMessage) {
						currentUser.setIngame(false);
						currentUser.removeLobby();
						currentUser.save();
						
					//User selects an Avatar
					} else if (object instanceof UserAvatarMessage) {
						UserAvatarMessage msg = (UserAvatarMessage)object;
						UserController.getImages().put(currentUser.getUsername(), msg.image);
						
						Lobby lobby = LobbyController.getLobby(currentUser.getLobbyID());
						
						if(lobby!=null) {
							lobby.getSlot(currentUser.getUsername()).setImageToByteArray(msg.image);
							sendToEveryoneInLobby(lobby, new LobbyUpdatedMessage(lobby));
						}
						
					// User changed password
					} else if (object instanceof UserPasswordUpdateMessage) {
						UserPasswordUpdateMessage msg = (UserPasswordUpdateMessage)object;
						currentUser.setPassword(msg.newPassword);
						currentUser.save();

						connection.sendMessage(new ClientMessageMessage("Password successfully changed!", Status.SUCCESS));
						
					// User logout
					} else if (object instanceof UserLogoutMessage) {
						
						// Perform the logout
						currentUser.logout();
						
						// (maybe) notify the lobby of the sign out
						UserController.notifyLobbyWhenLoggedOut(currentUser);
						
						// Then send the logout message to force the client to sign out
						connection.sendMessage(new UserLogoutForceMessage("You signed out", false));

					/**
					 * GAMEHUB MESSAGES
					 */
						
					// Register new GameHub
					} else if (object instanceof GameHubRegisterMessage) {
						GameHubRegisterMessage msg = (GameHubRegisterMessage) object;
						GameHub gameHub = new GameHub(msg.getGames());
						GameHubController.register(gameHub, connection);
						
						//Map GameHub object to this connection thread
						connection.setObject(gameHub);
						
						// Notify users of new games
						if (msg.getGames().size() > 0) {
							for(User u : UserController.findAll()) {
								RemoteConnection userCon;
								if ((userCon = u.getConnection()) != null) {
									userCon.sendMessage(new GameListMessage(GameHubController.getListOfGames()));
								}
							}
						}
						
					// Gamehub has started a new match.
					} else if (object instanceof MatchStartedMessage) {
						MatchStartedMessage msg = (MatchStartedMessage)object;
						
						Lobby activeLobby = LobbyController.getLobby(msg.lobbyid);
						
						for (Slot slot : activeLobby.getSlots()) {
							if (slot.isOccupied()){
								User user = UserController.findByUsername(slot.getUser().getUsername());
								RemoteConnection conn;
								if((conn = user.getConnection()) !=null) {
									String password="wrongpassword";

									for (Entry<String, Integer> e : msg.passwords.entrySet()) {
									    Integer value = e.getValue();
									    if(value.equals(slot.getID())) {
									    	password = e.getKey();
									    }
									}
									
									try {
										User slotUser = UserController.findByUsername(slot.getUser().getUsername());
										slotUser.setIngame(true);
										slotUser.save();
										
										conn.sendMessage(new ConnectToGameHubMessage(
												
													(connection.getRemoteAddressTCP().getHostName().equals("localhost") ? 
																InetAddress.getLocalHost().getHostName() // gamehub is local, send public ip
															: 
																connection.getRemoteAddressTCP().getHostName()), // send the ip used.

															msg.port, msg.matchid, password));
										

										
									} catch (UnknownHostException e1) {
										e1.printStackTrace();
									}
								}
							}
						}
						LobbyController.remove(activeLobby);
						
					// A match has just ended
					} else if (object instanceof MatchEndedWithResultsMessage) {
						MatchEndedWithResultsMessage mewrm = (MatchEndedWithResultsMessage) object;
						
						Lobby lobby = LobbyController.getLobby(mewrm.lobbyid);
						Map<User, Integer> userResults = new HashMap<User, Integer>();
						Map<User, Double> userPreRanking = new HashMap<User, Double>();
						Map<String, Double> userNameRankDiff = new HashMap<String, Double>();
						Map<String, Double> userNamePreRank = new HashMap<String, Double>();

						
						for (Map.Entry<Integer, Integer> entry : mewrm.results.entrySet()) {
						    Integer slotID = entry.getKey();
						    Integer placement = entry.getValue();
						    
						    User player = UserController.findByUsername(lobby.getSlot(slotID).getUser().getUsername());
						    double preMatchRanking = player.getRanking(lobby.getGame().getIdentifier());
						    userResults.put(player, placement); 
						    userPreRanking.put(player, preMatchRanking);
						    
						    //Notify all users friends that they are available again
						    MasterServer.sendUpdatedFriendList(player, null);
						}
						
						RankingController.calculateRanking(userResults, lobby.getGame().getIdentifier());
						
						//calculate diff per player						
						//send userPreRanking + userRankDiff to all clients
						
						for ( Map.Entry<User, Double> entry : userPreRanking.entrySet()){
							
							double diff = entry.getKey().getRanking(lobby.getGame().getIdentifier()) - entry.getValue();
							userNameRankDiff.put(entry.getKey().getUsername(), diff);	
							userNamePreRank.put(entry.getKey().getUsername(), entry.getValue());
						}
						 						
						sendToEveryoneInLobby(lobby, new MatchResultsMessage(userNamePreRank, userNameRankDiff));
						
					/**
					 * GAME MESSAGES
					 */

					// Get list of games
					} else if (object instanceof GameListGetMessage) {
						connection.sendMessage(new GameListMessage(GameHubController.getListOfGames()));
						
					//Get info about a game
					} else if (object instanceof GameInfoGetMessage) {
						String name = ((GameInfoGetMessage) object).name;
						Game game = GameHubController.getGameWithId(name);
						
						connection.sendMessage(new GameInfoMessage(game.getGameName(), game.getGameDescription(),game.getIdentifier()));
						
					//Get rankings for a game
					} else if (object instanceof GameRankingGetMessage) {
						String gameIdentifier = ((GameRankingGetMessage) object).gameIdentifier;
						Game game = GameHubController.getGameWithId(gameIdentifier);
						
						connection.sendMessage(new GameRankingMessage(game.getGameName(), GameHubController.getRankings(game)));
						
					/**
					 * LOBBY MESSAGES
					 */
						
					// Create a new lobby
					} else if (object instanceof LobbyCreateMessage) {
						LobbyCreateMessage msg = (LobbyCreateMessage) object;

						Game game = GameHubController.getGameWithId(msg.filename);
						Lobby lobby = LobbyController.createLobby(game);
						
						// Slot 0 is the Lobby creator (and owner)
						lobby.addPlayer(new Profile(currentUser), 0, UserController.getImages().get(currentUser.getUsername()));
						currentUser.setLobby(lobby);
						currentUser.save();
						
						connection.sendMessage(new LobbyUpdatedMessage(lobby));
						
						// Push status to all friends
						MasterServer.sendUpdatedFriendList(currentUser, null);	
						
					// Invite user to specific slot
					} else if (object instanceof LobbySlotInviteMessage) {
						LobbySlotInviteMessage msg = (LobbySlotInviteMessage)object;
						User invitedUser = UserController.findByUsername(msg.username);
						
						// The lobby to invite to will always be the current users one
						Lobby lobby = LobbyController.getLobby(currentUser.getLobbyID());
						
						// Add the user but set accepted status to false
						lobby.addPlayer(new Profile(invitedUser), msg.slotid, UserController.getImages().get(invitedUser.getUsername()), false);
						
						// Notify invited player of the invite
						RemoteConnection otherCon;
						if ((otherCon = invitedUser.getConnection()) != null) {
							otherCon.sendMessage(new LobbySlotInviteReceivedMessage(lobby,msg.slotid));
						}
						
						// Send back the updated Lobby to eveyone in the lobby
						sendToEveryoneInLobby(lobby, new LobbyUpdatedMessage(lobby));
						
					// User accepted Lobby invite
					} else if (object instanceof LobbySlotInviteAcceptMessage) {
						LobbySlotInviteAcceptMessage msg = (LobbySlotInviteAcceptMessage)object;
						Lobby lobby = LobbyController.getLobby(msg.lobbyid);
						
						// Notify all players before adding the new player (to not notify the player itself)
						sendToEveryoneInLobby(lobby, new ClientMessageMessage(currentUser.getUsername() + " joined the lobby", ClientMessageMessage.Status.SUCCESS));

						// Add to the specific slot
						lobby.addPlayer(new Profile(currentUser), msg.slotid, UserController.getImages().get(currentUser.getUsername()));
						currentUser.setLobby(lobby);
						currentUser.save();
					
						// And send the new lobby to all players!
						sendToEveryoneInLobby(lobby, new LobbyUpdatedMessage(lobby));
						
						// And push new friendslist to all players friends
						MasterServer.sendUpdatedFriendList(currentUser, null);
						
						
					// User declines Lobby invite
					} else if (object instanceof LobbySlotInviteDeclineMessage) {
						LobbySlotInviteDeclineMessage msg = (LobbySlotInviteDeclineMessage)object;
						Lobby lobby = LobbyController.getLobby(msg.lobbyid);
						
						// Delete the user from the slot
						lobby.removeSlot(msg.slotid);
						currentUser.removeLobby();
						currentUser.save();
						
						// And notify the rest of the declinement
						sendToEveryoneInLobby(lobby, new LobbyUpdatedMessage(lobby));
						
						// Send empty lobby to declining user to clear any existing lobby 
						connection.sendMessage(new LobbyUpdatedMessage(null));
						
					// A user (slot) is uninvited by the lobby owner
					} else if (object instanceof LobbySlotUninviteMessage) {
						LobbySlotUninviteMessage lsum = (LobbySlotUninviteMessage) object;
						Lobby lobby = LobbyController.getLobby(currentUser.getLobbyID());
						
						// Remove the Lobby from the user (if it had one)
						UserController.findByUsername(lobby.getSlot(lsum.slotid).getUser().getUsername()).removeLobby();
						
						// Delete the user from the slot
						lobby.removeSlot(lsum.slotid);
						
						// And notify the rest with the new lobby
						sendToEveryoneInLobby(lobby, new LobbyUpdatedMessage(lobby));
	
					// The lobby owner changed global Lobby settings
					} else if (object instanceof LobbySettingsUpdateMessage) {
						LobbySettingsUpdateMessage msg = (LobbySettingsUpdateMessage)object;
						
						// Lobby changed will always be current users one
						Lobby activeLobby = LobbyController.getLobby(currentUser.getLobbyID());
						
						// Perform check that currentUser is actually the owner (is on slot 0)
						if (activeLobby.getSlot(0).getUser().equals(currentUser)) {
							// Update the global settings
							activeLobby.setGlobalSettings(msg.settings);
							
							// And notify the rest with the new lobby
							sendToEveryoneInLobby(activeLobby, new LobbyUpdatedMessage(activeLobby));
						}
						
					// User changed its slot settings in the lobby
					} else if (object instanceof LobbySlotSettingsUpdateMessage) {
						LobbySlotSettingsUpdateMessage msg = (LobbySlotSettingsUpdateMessage)object;
						
						// Lobby changed will always be current users one
						Lobby activeLobby = LobbyController.getLobby(currentUser.getLobbyID());
						
						// Loop through to find the correct slot
						for(Slot s : activeLobby.getSlots()) {
							if (s.getUser() != null && s.getUser().equals(currentUser)) {
								s.setSettings(msg.settings);
							}
						}
						
						// And notify the rest with the new lobby
						sendToEveryoneInLobby(activeLobby, new LobbyUpdatedMessage(activeLobby));
						
					// Lobby chat message
					} else if(object instanceof LobbyChatMessageSend){
						LobbyChatMessageSend msg = (LobbyChatMessageSend) object;
						Lobby lobby = LobbyController.getLobby(msg.lobbyID);
						lobby.addChatMessage(currentUser.getUsername(), msg.message);

						// Don't send the entire lobby every time, send the individual msg instead
						sendToEveryoneInLobby(lobby, new LobbyChatMessageRecieved(msg.message, msg.username));
						
					// User marks itself as ready
					} else if (object instanceof UserSetReadyMessage) {
						UserSetReadyMessage msg = (UserSetReadyMessage)object;						
						
						// Lobby in question will always be current users one
						Lobby activeLobby = LobbyController.getLobby(currentUser.getLobbyID());
						
						// Set user as ready
						activeLobby.getSlot(currentUser.getUsername()).setReady(msg.value);
						
						// If everyone are ready
						if(activeLobby.isReady()) {
							GameHub gh = GameHubController.getGameHubWithGame(activeLobby.getGame());
							
							// Send message to GameHub to start the match
							if(gh!=null) {
								GameHubController.getConnection(gh.getIdentifier()).sendMessage(new MatchStartMessage(activeLobby));
							}
						} else {
							sendToEveryoneInLobby(activeLobby, new LobbyUpdatedMessage(activeLobby));
						}
						
					// User wants to leave lobby
					} else if (object instanceof LobbySlotLeaveMessage) {
						// Lobby in question will always be current users one
						Lobby lobby = LobbyController.getLobby(currentUser.getLobbyID());
						
						// If its the lobby owner leaving, delete the lobby
						if (lobby.isOwner(new Profile(currentUser))) {
							// Send empty lobby to all users in lobby
							sendToEveryoneInLobby(lobby, new LobbyUpdatedMessage(null));
							
							// Send message to inform the deleter
							connection.sendMessage(new ClientMessageMessage("Lobby successfully deleted.", ClientMessageMessage.Status.NOTICE));
							
							// Send message to inform the other players
							sendToEveryoneInLobby(lobby, new ClientMessageMessage("Lobby deleted by " + lobby.getSlot(0).getUser().getUsername(), ClientMessageMessage.Status.NOTICE));
							
							// Also send updated friendslist since users will now be available
							//TODO: Fix this
							
							// At last delete the Lobby
							LobbyController.remove(lobby);
						} else {
							lobby.removePlayer(currentUser.getUsername());
							currentUser.removeLobby();
							currentUser.save();
							
							// And the rest should get updated lobby
							sendToEveryoneInLobby(lobby, new LobbyUpdatedMessage(lobby));
							
							// Send null lobby for emptying the GUI
							connection.sendMessage(new LobbyUpdatedMessage(null));
							
							// ... and a message to inform about the leaving
							sendToEveryoneInLobby(lobby, new ClientMessageMessage(currentUser.getUsername() + " left the lobby", ClientMessageMessage.Status.NOTICE));
						}
						
						// Notify all friends with an updated friendlist
						MasterServer.sendUpdatedFriendList(currentUser, null);
						
					/**
					 * MATCHMAKING MESSAGES
					 */
						
					// Join queue
					} else if (object instanceof UserJoinQueueMessage) {
						UserJoinQueueMessage msg = (UserJoinQueueMessage) object;

						GameHubController.getMatchQueue(GameHubController.getGameWithId(msg.gameIdentifier)).addPlayer(currentUser);

					// Exit queue
					} else if (object instanceof UserExitQueueMessage) {
						UserExitQueueMessage msg = (UserExitQueueMessage) object;
						
						GameHubController.getMatchQueue(GameHubController.getGameWithId(msg.filename)).removePlayer(currentUser);
		
					/**
					 * FRIEND MESSAGES
					 */
						
					// Friend add
					} else if (object instanceof UserFriendAddMessage) {
						if (currentUser != null) {
							User other = UserController.findByUsername(((UserFriendAddMessage)object).getUsername());
							
							// If there was an error, just don't continue
							if (! currentUser.addFriendRequest(other)) {
								return;
							}
							
							// Push a notice to the receiving user (if is online)
							RemoteConnection otherCon;
							if ((otherCon = other.getConnection()) != null) {
								otherCon.sendMessage(new UserFriendListMessage(other.getFriends(true)));
								otherCon.sendMessage(new ClientMessageMessage("New friendrequest received!", ClientMessageMessage.Status.NOTICE));
							}
						}
						
					// Accept Friend Request
					} else if (object instanceof UserFriendAcceptMessage) {
						if (currentUser != null) {
							User requestingUser = UserController.findByUsername(((UserFriendAcceptMessage) object).getUsername());
							
							// If there was an error, just don't continue
							if (! currentUser.acceptFriendRequest(requestingUser)) {
								return;
							}
							
							// Try to push to the receiving user (if is online)
							RemoteConnection otherCon;
							if ((otherCon = requestingUser.getConnection()) != null) {
								otherCon.sendMessage(new UserFriendListMessage(requestingUser.getFriends(true)));
								otherCon.sendMessage(new ClientMessageMessage("Friend request to " + currentUser.getUsername() + " accepted!",
										ClientMessageMessage.Status.SUCCESS));
							}
							
							// Update sending users friends list
							connection.sendMessage(new UserFriendListMessage(currentUser.getFriends(true)));
						}
					
					// Decline Friend Request
					} else if (object instanceof UserFriendDeclineMessage) {
						if (currentUser != null) {
							User requestingUser = UserController.findByUsername(((UserFriendDeclineMessage) object).getUsername());
							
							// If there was an error, just don't continue
							if (! currentUser.declineFriendRequest(requestingUser)) {
								return;
							}
							// And send the updated friends list to the declining user
							connection.sendMessage(new UserFriendListMessage(currentUser.getFriends(true)));
						}
						
					// Remove a Friend
					} else if (object instanceof UserFriendRemoveMessage) {
						if (currentUser != null) {
							User other = UserController.findByUsername(((UserFriendRemoveMessage) object).getUsername());
							
							// If there was an error, just don't continue
							if (! currentUser.removeFriend(other)) {
								return;
							}
							
							// Try to push to the receiving user
							RemoteConnection otherCon;
							if ((otherCon = other.getConnection()) != null) {
								otherCon.sendMessage(new UserFriendListMessage(other.getFriends(true)));
							}
							// And send the updated friendslist to the declining user
							connection.sendMessage(new UserFriendListMessage(currentUser.getFriends(true)));
						}
						
					// Get Friendlist
					} else if (object instanceof UserFriendListGetMessage) {
						if (currentUser != null) {
							connection.sendMessage(new UserFriendListMessage(currentUser.getFriends(true)));
						}
						
					// If we arent logging in, or registering any user, queue exception.	
					} else if (connection.getObject()==null) {
						throw new UserNotLoggedInException();
					} 
				} catch (UserNotLoggedInException e) {
					
				} catch (NullPointerException e) {
					e.printStackTrace();
				};
			} 

			public void disconnected (Connection c) {
				RemoteConnection connection = (RemoteConnection)c;
				if (connection.getObject() != null && connection.getObject() instanceof User) {
					User user = ((User)connection.getObject());
					user.logout();
					
					UserController.notifyLobbyWhenLoggedOut(user);
					
					// Push updates to friends
					MasterServer.sendUpdatedFriendList(user, null);
				}
				if (connection.getObject() != null && connection.getObject() instanceof GameHub) {
					GameHub gameHub = ((GameHub)connection.getObject());
					GameHubController.unregister(gameHub);
					System.out.println("GameHub with " + gameHub.getGames().size() + " games disconnected");
				}
			}
		});
		
		// Initiate the server on the network
		server.bind(Network.port);
		server.start();
		
		MasterServer.startConsole();
	}
	
	private static void startConsole() {
		/**
		 * PLAYFUEL CONSOLE
		 */
		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter command or type help:");
			if (sc.hasNextLine()) {
				
				String command = sc.nextLine();
				
				if (command.equals("help")) {
					
					MasterServer.outputConsoleHelp();
					
				} else if (command.contains("db:seed")) {
					String database = command.replace("db:seed", "");
					
					// Seeding entire db
					if (database.length() == 0) {
						// User table serves as reference, if not installed, nothing probably is
						if (! Database.INSTANCE.tableExists(masterServer.db.tables.User.TABLE)) {
							System.out.println("Tables not found. Please run db:setup first");
						} else {
							System.out.println("Seeding entire database");
							DatabaseSeeder.seedAll();
						}
					} else {
						database = database.replace(":", "");
						String[] availableDatabases = {
							masterServer.db.tables.User.TABLE,
							masterServer.db.tables.Friendship.TABLE,
							masterServer.db.tables.Ranking.TABLE
						};
						
						if (Arrays.asList(availableDatabases).contains(database)) {
							try {
								Method m;
								if ((m = DatabaseSeeder.class.getMethod("seed" + database.substring(0,1).toUpperCase() + database.substring(1))) != null) {
									m.invoke(DatabaseSeeder.class);
								}
							} catch (SecurityException e) {
								e.printStackTrace();
							} catch (NoSuchMethodException e) {
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
						}
					}
					
				} else if (command.equals("db:setup")) {
					System.out.println("Installing database");
					if (! Database.INSTANCE.setup() ) {
						System.out.println("Database is already initialized. Use db:setup:force to reinstall");
					}
					
				} else if (command.equals("db:setup:force")) {
					System.out.println("Forcing database install");
					if (! Database.INSTANCE.setup(true) ) {
						System.out.println("Something went wrong installing database");
					}
					
				} else if (command.contains("game")) {
					String[] commands = command.split(":");
					if (commands.length < 3) {
						System.out.println("Syntax error");
						MasterServer.outputConsoleHelp();
						continue;
					}
					String gameIdentifier = commands[1];
					String action = commands[2];
					
					Game game = GameHubController.getGameWithId(gameIdentifier);
					
					if (game != null) {
						if (action.equals("ranking")) {
							System.out.println("Rankings for: " + game.getGameName());
							List<RankingResult> rankings = GameHubController.getRankings(game);
							
							System.out.println(rankings.size() + " players has rankings:");
							for (RankingResult ranking : rankings) {
							    System.out.println(" >> " + ranking.getProfile().getUsername() + ":" + ranking.getRanking());
							}
						} else {
							System.out.println("Unknown command");
							MasterServer.outputConsoleHelp();
						}
					} else {
						System.out.println("Game " + gameIdentifier + " not found");
					}
					
				} else if (command.contains("user")) {
					String[] commands = command.split(":");
					if (commands.length < 3) {
						System.out.println("Syntax error");
						MasterServer.outputConsoleHelp();
						continue;
					}
					String username = commands[1];
					String action = commands[2];
					
					if (action.equals("message")) {
						if (commands.length < 4) {
							System.out.println("Syntax error");
							MasterServer.outputConsoleHelp();
							continue;
						}
						User user = UserController.findByUsername(username);
						RemoteConnection userConnection;

						if ((userConnection = user.getConnection()) != null) {
							userConnection.sendMessage(new ClientMessageMessage(commands[3], ClientMessageMessage.Status.SUCCESS));
						}
					}
					System.out.println(username);
					
				} else if (command.equals("quit")) {
					//TODO Perform server shutdown tasks
					System.exit(0);
				}
			}
		}
	}

	private static void outputConsoleHelp() {
		System.out.println("");
		System.out.println("<< PLAYFUEL SERVER CONSOLE >>");
		System.out.println("");
		System.out.println("Available commands:");
		System.out.println("db:setup(:force) \t\t Use to setup the database tables. Append :force to reinstall database.");
		System.out.println("db:seed \t\t\t Use to seed database with example data.");
		System.out.println("db:seed:<table> \t\t Use to seed a specific table. Available tables: " +  masterServer.db.tables.User.TABLE + ", " + masterServer.db.tables.Friendship.TABLE + ", " + masterServer.db.tables.Ranking.TABLE);
		System.out.println("");
		System.out.println("game:<game_identifier>:ranking \t Use to list rankings for a game");
		System.out.println("");
		System.out.println("user:<username>:message:<Message String> \t Use to send a message to the user");
		System.out.println("");
		System.out.println("quit \t\t\t\t Use to shutdown server");
		System.out.println("");
	}
	
	/**
	 * Convenience method for sening an updated friends list to all passed users friends
	 * @param user whose friends to notify
	 * @param imsg optional message to send to friends
	 */
	public static void sendUpdatedFriendList(User user, IMessage imsg) {
		for(Friend f : user.getFriends(false)) {
			RemoteConnection friendConnection;
			User friendUser = UserController.findByUsername(f.getUsername());
			if (( friendConnection = friendUser.getConnection()) != null) {
				friendConnection.sendMessage(new UserFriendListMessage(friendUser.getFriends()));
				if (imsg instanceof IMessage) {
					friendConnection.sendMessage(imsg);
				}
			}
		}
	}
		
	/**
	 * Sends a message to everyone currently in lobby
	 * @param lobby the lobby in question
	 * @param msg the message to send
	 */
	public static void sendToEveryoneInLobby(Lobby lobby,IMessage msg) {
		for (Slot slot : lobby.getSlots()) {
			// Only send to accepted players
			if (slot.isOccupied() && slot.isAccepted() ){
				User slotUser = UserController.findByUsername(slot.getUser().getUsername());
				RemoteConnection conn = slotUser.getConnection();
				if(conn!=null) {
					conn.sendMessage(msg);
				}
			}
		}
	}
}
