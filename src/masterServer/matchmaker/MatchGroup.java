package masterServer.matchmaker;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableSet;

import utilities.msg.messages.LobbyUpdatedMessage;
import utilities.msg.messages.MatchmakingLobbyJoinedMessage;

import masterServer.MasterServer;
import masterServer.ctrl.*;
import masterServer.db.Database;
import models.*;

public class MatchGroup implements Comparable<MatchGroup> {
	private Queue queue;
	protected List<Player> players;
	private Game game = new Game();
	private double ranking;
	//private double treshold=20;
	private double maxtresh=150;
	//private int slots = 4;
	
	/**
	 * Used to created dummy groups for comparing.
	 * @param queue
	 * @param ranking
	 */
	public MatchGroup(double ranking) {
		this.ranking=ranking;
	}
	
	public MatchGroup(Queue queue, Player player, Game game){
		this.players = new ArrayList<Player>();
		this.queue=queue;
		this.game=game;
		putPlayer(player);
	};
	
	public boolean isFull() {
		return players.size()==game.getMaxNumberOfSlots();
	}
	
	public void putPlayer(Player player) {
		players.add(player);
		updateRanking();
	}
	
	public void givePlayer(Player player, MatchGroup otherGroup) {
		if(!isFull() && !otherGroup.isFull()) {
			if(players.remove(player)) {
				otherGroup.putPlayer(player);
				updateRanking();
			}
		}
	}
	
	public void updateRanking() {
		double ranksum = 0;
		for(Player player : players) {
			ranksum+=player.user.getRanking(game.getIdentifier());
		}
		ranking=ranksum/players.size();
	}
	
	public double getTreshold() {
		double tressum = 0;
		for(Player player : players) {
			tressum+=queue.getTick()-player.born;
		}

		return tressum/players.size()*3;
	}
	
	public double getMin() {
		return ranking - getTreshold();
	}
	
	public double getMax() {
		return ranking + getTreshold();
	}
	
	public boolean isWithinTreshold(double value) {
		return value>= getMin() && value <= getMax();
	}
	
	public void tick() {		
		NavigableSet<MatchGroup> groups = queue.getGroups().subSet(new MatchGroup(getMin()), false,
				new MatchGroup(getMax()), false);
		groups.remove(this);
		
		for(MatchGroup group : groups) {
			for(Player player : new ArrayList<Player>(group.players)) {
				if(isWithinTreshold(player.user.getRanking(game.getIdentifier()))) {
					group.givePlayer(player, this);
				}
			}
		}
		if(isFull()) {
			this.groupIsNowFull();
		}
		if(players.size()==0) {
			queue.remove.add(this);
		}
	}
	
	private void groupIsNowFull() {
		queue.remove.add(this);
		
		Lobby lobby = LobbyController.createLobby(game);
				
		int i=0;
		for(Player player : players) {
			lobby.addPlayer(new Profile(player.user), i, UserController.getImages().get(player.user.getUsername()));
			player.user.setLobby(lobby.getID());
			player.user.save();
			
			i++;
		}
		
		MasterServer.sendToEveryoneInLobby(lobby, new MatchmakingLobbyJoinedMessage());
		MasterServer.sendToEveryoneInLobby(lobby, new LobbyUpdatedMessage(lobby));
	}

	public double getRanking() {
		return ranking;
	}

	@Override
	public int compareTo(MatchGroup o) {
		return (int)(ranking-o.getRanking());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((players == null) ? 0 : players.hashCode());
		long temp;
		temp = Double.doubleToLongBits(ranking);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(getTreshold());
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchGroup other = (MatchGroup) obj;
		if (players == null) {
			if (other.players != null)
				return false;
		} else if (!players.equals(other.players))
			return false;
		if (Double.doubleToLongBits(ranking) != Double
				.doubleToLongBits(other.ranking))
			return false;
		if (Double.doubleToLongBits(getTreshold()) != Double
				.doubleToLongBits(other.getTreshold()))
			return false;
		return true;
	}
	
	
}
