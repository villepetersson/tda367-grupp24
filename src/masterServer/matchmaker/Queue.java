package masterServer.matchmaker;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

import models.Game;
import models.User;

public class Queue extends Thread{
	private int tick=0;
	private TreeSet<MatchGroup> groups = new TreeSet<MatchGroup>();
	public List<MatchGroup> remove = new ArrayList<MatchGroup>();
	public List<Player> removePlayers = new ArrayList<Player>();
	public Game game;
	
	public int getTick() {
		return tick;
	}
	
	public void addPlayer(User user) {
		Player player = new Player(this, user);
		MatchGroup ceiling = getGroups().ceiling(new MatchGroup(player.user.getRanking(game.getIdentifier())));
		MatchGroup floor = getGroups().floor(new MatchGroup(player.user.getRanking(game.getIdentifier())));
		
		if(ceiling != null && ceiling.isWithinTreshold(player.user.getRanking(game.getIdentifier()))) {
			ceiling.putPlayer(player);
		} else if(floor != null && floor.isWithinTreshold(player.user.getRanking(game.getIdentifier()))) {
			floor.putPlayer(player);
		} else {
			groups.add(new MatchGroup(this, player, game));
		}
	}
	
	public void removePlayer(User user) {
		final Player player = new Player(this, user);
		new Thread()
		{
		    public void run() {
				synchronized (removePlayers) {
					removePlayers.add(player);
				}
		    }
		}.start(); // Put this in a thread so that the master wont lock waiting for the queue.
	}
	
	public TreeSet<MatchGroup> getGroups() {
		return groups; // Should be divided to make stuff quicker.
	}
	
	public Queue(Game game) {
		this.game=game;
		new Thread()
		{
		    public void run() {
				while(true) {
					//if(tick%3==0) {
					//	this.addPlayer(new Player(this, 1500+((random.nextDouble()-0.5)*1000)));
					//}
					TreeSet<MatchGroup> itgroups = new TreeSet<MatchGroup>(groups);
					synchronized (removePlayers) {
						for(MatchGroup group : itgroups) {
							for(Player player : removePlayers) {
								if(group.players.contains(player)) {
									group.players.remove(player);
								}
							}
							group.tick();
						}
						removePlayers = new ArrayList<Player>();
					}
					
					groups=itgroups;
					for(MatchGroup group : remove) {
						groups.remove(group);
					}
					remove = new ArrayList<MatchGroup>();
					tick++;
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}	
		    }
		}.start();
	}
	
	/*public static void main(String[] args) {
		new Thread(){
			Queue queue;
			
			public Thread withQueue(Queue queue) {
				this.queue=queue;
				return this;
			}
			
			public void run() {
		Queue queue=new Queue();
		//queue.start();

			}
		}.withQueue(new Queue()).start();
	}*/
}
