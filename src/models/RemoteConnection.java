package models;


import com.esotericsoftware.kryonet.Connection;

/**
 * This holds a connection between client and masterserver or gamehub and masterserver
 * @author Joakim
 *
 */
public class RemoteConnection extends Connection {
	private Object clientObject;
	
	public RemoteConnection() {
		;
	}
	
	public void setObject(Object o) {
		this.clientObject = o;
	}
	
	public Object getObject() {
		return this.clientObject;
	}
	
	public void sendMessage(IMessage msg) {			
		// Send this message to the connected
		this.sendTCP(msg);
	}
}