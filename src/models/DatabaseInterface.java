package models;

import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Interface specifying the Database
 * @author Joakim
 *
 */
public interface DatabaseInterface {
	
	/**
	 * Friend methods
	 */
	
	/**
	 * Add a new friendrequest
	 * @param User requesting_user
	 * @param User requested_user
	 * @return success status
	 */
	public boolean addFriendRequest(User requesting_user, models.User requested_user);
	
	/**
	 * Instantly add a friendrow in the db
	 * @param User requesting_user
	 * @param User requested_user
	 * @param accepted
	 * @return success status
	 */
	public boolean addFriend(User requesting_user, User requested_user, boolean accepted);
	
	/**
	 * Accepts a friend request
	 * @param models.User requested_user is the user that got the friend request, usually "me"
	 * @param models.User requesting_user is the user the friend request is from
	 * @return boolean success
	 */
	public boolean acceptFriendRequest(models.User requested_user, models.User requesting_user);
	
	/**
	 * Declines a friend request
	 * @param models.User requested_user is the user that got the friend request, usually "me"
	 * @param models.User requesting_user is the user the friend request is from
	 * @return boolean success
	 */
	public boolean declineFriendRequest(models.User requested_user, models.User requesting_user);
	
	/**
	 * Removes a friendship, both ways
	 * @param models.User user1
	 * @param models.User user2
	 * @return boolean success
	 */
	public boolean removeFriend(models.User user1, models.User user2);
	
	/**
	 * User methods
	 */
	
	/**
	 * Finds all friends for passed user
	 * @param user
	 * @param includeFriendRequests if friendrequests also should be included in results
	 * @return Found friends
	 */
	public List<Friend> findFriendsForUser(User user, boolean includeFriendRequests);
	
	/**
	 * Register a new user if the username is available
	 * @param User new user
	 * @return success status, false if the username is taken
	 */
	public boolean registerUser(User u);
	
	/**
	 * Updates a user
	 * @param user to update
	 */
	public void updateUser(User user);
	
	/**
     * Find a user by provided search string
     * @param username query to search by
     * @return a set of found users
     */
	public Set<User> findUsersByUsername(String query);
	
	/**
	 * Get a user by provided username
     * @param String username
     * @return the found User or null if not found
     */
	public User getUserByUsername(String username);
	
	/**
	 * Get a user by provided user id
     * @param int userID
     * @return the found User or null if not found
     */
	public User getUserByID(int userID);
	
	/**
	 * Retrieves all users from db
	 * @return set of all users
	 */
	public Set<models.User> findAllUsers();

	/**
	 * Map a connection to a user by username
	 * @param username to map to
	 * @param conn the connection object
	 */
	public void setUserConnection(String username, RemoteConnection conn);

	/**
	 * Get a connection for a user
	 * @param username
	 * @return the connection or null if offline
	 */
	public RemoteConnection getUserConnection(String username);
	
	/**
	 * Remove a user connection by username
	 * @param username to remove connection for
	 */
	public void removeUserConnection(String username);

	/**
	 * Get all user images
	 * @return user images
	 */
	public Map<String, byte[]> getUserImages();
	
	/**
	 * Ranking methods
	 */
	
	/**
	 * Gets the ranking for passed user
	 * @param user User model
	 * @param game_identifier is set in the games manifest file
	 * @return the players ranking for this game, 0 if not found
	 */
	public double getUserRanking(User user, String game_identifier);
	
	/**
	 * Set the ranking for a user
	 * @param user to set ranking for
	 * @param game_identifier which game the ranking belongs to
	 * @param ranking the actual ranking
	 * @return
	 */
	public boolean setUserRanking(models.User user, String game_identifier, double ranking);
	
	/**
	 * Gets all the rankings for a game
	 * @param gameIdentifier the unique identifier for a game
	 * @param limit if you dont want to fetch all records at once
	 * @param order if fetching asc or desc
	 * @return
	 */
	public List<RankingResult> getGameRankings(String gameIdentifier, int limit, String order);
}
