package models;

import java.util.*;

public class GameHub {
	private List<Game> games = new ArrayList<Game>();
	private int identifier;

	public GameHub() {
		;
	}
	
	public int getIdentifier() {
		return identifier;
	}

	public void setIdentifier(int identifier) {
		this.identifier = identifier;
	}
	
	public GameHub(List<Game> games) {
		this.games = games;
	}
	
	public void setGames(List<Game> games) {
		this.games = games;
	}
	
	public List<Game> getGames() {
		return this.games;
	}
}
