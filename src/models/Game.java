package models;

import java.util.List;

import javax.swing.ImageIcon;

import masterServer.db.Database;

public class Game {
	private String gamename;
	private String description;
	private List<Setting> settingsfactory;
	private List<Setting> slotsettingsfactory;
	private List<ImageIcon> screenshots;
	private String playfuelgameserverclass;
	private String playfuelgameclientclass;
	private String identifier;
	private String maxnumberofslots;
	
	public Game() {
		;
	}
	
	public String getGameDescription(){
		return description;  
	}

	public String getGameName() {
		return gamename;
	}

	public List<Setting> getSettingsFactory() {
		return settingsfactory;
	}

	public String getGameServerClassName() {
		return playfuelgameserverclass;
	}

	public String getGameClientClassName() {
		return playfuelgameclientclass;
	}
	
	public int getMaxNumberOfSlots() {
		return new Integer(maxnumberofslots);
	}

	public String getIdentifier() {
		return identifier;
	}

	public List<Setting> getSlotSettingsFactory() {
		return slotsettingsfactory;
	}
	
	public List<ImageIcon> getScreenShots() {
		return screenshots;
	}
}
