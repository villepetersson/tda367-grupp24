package models;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;


public class Slot {

	private Profile playerInSlot;
	private boolean isAccepted;
	private boolean isReady;
	private Map<Setting,Object> settings = new HashMap<Setting,Object>();
	private int id;
	private byte[] image = null;

	// Must have empty constructor for Kryonet
	public Slot() {
		;
	}
	
	/**
	 * Create slot without player
	 * @param id slot id
	 */
	public Slot(int id){
		this(null,id);
	}
	
	/**
	 * Create a new slot with a player and slot id
	 * @param user player
	 * @param id slot id
	 */
	public Slot(Profile user,int id){
		playerInSlot = user;
		isReady = false;
		isAccepted = false;
		this.id=id;
	}
	
	public int getID() {
		return this.id;
	}
	
	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}
	
	public Map<Setting,Object> getSettings() {
		return settings;
	}
	
	public void setSettings(Map<Setting, Object> settings) {
		this.settings = settings;
	}
	
	public boolean isOccupied(){
		return playerInSlot!=null;
	}
	
	public Profile getUser(){
		return playerInSlot;
	}
	
	public void setUser(Profile user){
		playerInSlot=user;
	}

	public boolean isReady(){
		return isReady;
	}
	
	public void setReady(boolean value) {
		isReady=value;
	}
	
	/**
	 * Get a slot setting by identifier
	 * @param identifier the setting identifier
	 * @return the requested setting or null if not found
	 */
	public Object getSetting(String identifier) {
		Iterator iter = settings.keySet().iterator();

		while(iter.hasNext()) {
		Setting setting = (Setting) iter.next();
			
			if(setting.hashCode()==identifier.hashCode()) {
				return settings.get(setting);
			}
		}
		return null;
	}
	
	public int hashCode() {
		return id;
	}
	
	public BufferedImage getImage() {
		if (image==null) {
			return null;
		}
		try {
			return ImageIO.read(new ByteArrayInputStream(image));
		} catch (IOException e) {
			return null;
		}
	}
	
	public void setImage(BufferedImage buffimg) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(buffimg,"jpg",baos);
        baos.flush();
		image = baos.toByteArray();
		baos.close();
	}

	public void setImageToByteArray(byte[] image) {
		this.image=image;
	}
}
