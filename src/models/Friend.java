package models;


public class Friend extends Profile {
	private boolean accepted = false;
	
	public Friend() {
		super();
	}
	
	public Friend(User u) {
		super(u);
	}
	
	public Friend(User u, boolean accepted) {
		super(u);
		this.accepted = accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}
	
	public boolean isAccepted() {
		return accepted;
	}
}
