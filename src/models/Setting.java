package models;

public class Setting {
	private String input;
	private String defaultval;
	private String identifier;
	private String prettyname;
	
	// Choice
	private String[] values = null;
	
	// Integer
	private String minvalue = null;
	private String maxvalue = null;
	
	public Setting () {
		;
	}
	
	public Setting (String input,
					String defaultval,
					String identifier,
					String prettyname) {
		this.input=input;
		this.defaultval=defaultval;
		this.identifier=identifier;
		this.prettyname=prettyname;
	}

	public String getInput() {
		return input;
	}
	
	public String getDefault() {
		return defaultval;
	}	
	public String getPrettyname() {
		return prettyname;
	}
	
	@Override
	public boolean equals(Object that) {
		return this.hashCode()==that.hashCode(); // Not the prettiest equals.
	}

	@Override
	public int hashCode() {
		return identifier.hashCode();
	}
	
	public String getIdentifier() {
		return identifier;
	}

	public String[] getValues() throws NotSetInManifestException {
		if (values==null) {
			throw new NotSetInManifestException();
		}
		return values;
	}

	public int getMinvalue() throws NotSetInManifestException {
		if (minvalue==null) {
			throw new NotSetInManifestException();
		}
		return new Integer(minvalue);
	}

	public int getMaxvalue() throws NotSetInManifestException {
		if (maxvalue==null) {
			throw new NotSetInManifestException();
		}
		return new Integer(maxvalue);
	}
	
	public class NotSetInManifestException extends Exception {
		
	}
}
