package models;

import java.util.ArrayList;
import java.util.List;


import masterServer.db.Database;



public class User {
	private int ID;
	private String username;
	private String password;
	private DatabaseInterface db;
	private boolean isIngame = false;
	private int lobbyID = 0;
	//private static List<IMessage> queuedMessages = new ArrayList<IMessage>(); 
	
	public User() {
		;
	}
	
	public User(String username, String password, DatabaseInterface db)
	{
		this.db = db;
		this.username = username;
		this.password = password;
	}
	
	public User(User u, DatabaseInterface db) {
		this(u.username, u.password, db);
	}
	
	public void setID(int ID) {
		this.ID = ID;
	}
	
	public int getID() {
		return this.ID;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String newPassword) {
		this.password = newPassword;
	}

	public boolean isIngame() {
		return isIngame;
	}

	public void setIngame(boolean isIngame) {
		this.isIngame = isIngame;
	}
	
	public void setLobby(Lobby lobby) {
		this.lobbyID = lobby.getID();
	}
	
	public void setLobby(int lobbyID) {
		this.lobbyID = lobbyID;
	}
	
	public int getLobbyID() {
		return this.lobbyID;
	}
	
	public void removeLobby() {
		this.lobbyID = 0;
	}
	
	public void save() {
		db.updateUser(this);
	}
	
	// Gets player ranking for a specific game
	public double getRanking(String gameIdentifier) {
		double ranking = db.getUserRanking(this, gameIdentifier);
		// Set default ranking if the user didn't have a ranking
		if (ranking == 0) {
			db.setUserRanking(this, gameIdentifier, 1500);
			return db.getUserRanking(this, gameIdentifier);
		}
		return ranking;
	}
	
	// Sets player ranking for a specific game
	public void setRanking(String game_identifier, double ranking) {
		db.setUserRanking(this, game_identifier, ranking);
	}
	
	public List<Friend> getFriends() {
		return this.getFriends(false);
	}
	
	public List<Friend> getFriends(boolean includeFriendRequests) {
		// Check friend status
		List<Friend> friends = db.findFriendsForUser(this, includeFriendRequests);

		return friends;
	}
	
	public boolean addFriendRequest(User toUser) {
		return db.addFriendRequest(this, toUser);
	}
	
	/*
	// Queue a message if the client was not online
	public void queueMessage(IMessage msg) {
		queuedMessages.add(msg);
	}
	
	public List<IMessage> getQueuedMessages() {
		return queuedMessages;
	}
	
	// Send all queued messages
	public void sendQueuedMessages() {
		if ((this.db.getUserConnection(this.getUsername())) != null) {
			for(IMessage imsg : this.getQueuedMessages()) {
				this.db.getUserConnection(this.getUsername()).sendMessage(imsg);
			}
		}
	}
	*/
	public boolean acceptFriendRequest(User requestingUser) {
		return db.acceptFriendRequest(this, requestingUser);
	}

	public boolean declineFriendRequest(User requestingUser) {
		return db.declineFriendRequest(this, requestingUser);
	}

	public boolean removeFriend(User secondUser) {
		return db.removeFriend(this, secondUser);
	}
		
	public void setConnection(RemoteConnection conn) {
		db.setUserConnection(this.getUsername(), conn);
	}
	
	public RemoteConnection getConnection() {
		return db.getUserConnection(this.getUsername());
	}
	
	/**
	 * Signs out a user by letting go of their connection thread
	 * @param user to sign out
	 */
	public void logout() {
		db.removeUserConnection(this.getUsername());
	}
	@Override
	public int hashCode() {
		return username.hashCode();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null || other.getClass() != getClass()) {
			return false;
		}
		return username.equals(((User) other).username);
	}
}
