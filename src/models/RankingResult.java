package models;

/**
 * Convenience class to handle ranking results
 * @author Joakim
 *
 */
public class RankingResult {
	private Profile profile;
	private double ranking;
	
	public RankingResult() {
		;
	}
	
	public RankingResult(models.User user, double ranking) {
		this.ranking = ranking;
		this.profile = new Profile(user);
	}
	
	public Profile getProfile() {
		return this.profile;
	}
	
	public double getRanking() {
		return this.ranking;
	}
}