package models;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class ModelUtils {
	public static String ModelEncode(String str) {
		try{
			return URLEncoder.encode(str,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			return str.replace(" ", ""); // Ghetto removes spaces.
		}
	}
	
	public static String ModelDecode(String str) {
		try{
			return URLDecoder.decode(str,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			return str;
		}
	}
}
