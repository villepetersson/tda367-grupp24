package models;

public interface IMessage {
	
	//public IMessage handle(Object model);
	
	public interface ToServer extends IMessage {
		;
	}	
	
	public interface FromServer extends IMessage {
		;
	}

}
