package models;


public class Profile {
	private String username;
	private boolean isOnline = false;
	private boolean isBusy = false;
	private boolean isInGame = false;
	
	public Profile() {
		;
	}
	
	public Profile(User u) {
		this.setUsername(u.getUsername());
		this.setInGame(u.isIngame());
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public boolean isInGame() {
		return isInGame;
	}

	public void setInGame(boolean isIngame) {
		this.isInGame = isIngame;
	}
	
	public void setOnline(boolean status) {
		this.isOnline = status;
	}
	
	public boolean isOnline() {
		return this.isOnline;
	}
	
	public void setBusy(boolean status) {
		this.isBusy = status;
	}
	
	public boolean isBusy() {
		return this.isBusy;
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		// Specific check for User
		if (other instanceof User) {
			return ((User) other).getUsername().equals(this.getUsername());
		} else if (other.getClass() != getClass()) {
			return false;
		}
		return username.equals(((Profile) other).username);
	}
}
