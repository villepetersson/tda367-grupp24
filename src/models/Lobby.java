package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Lobby {
	private List<Slot> slots;
	private Map<Setting,Object> settings;
	private List<String> chatMessages;
	private Game game;
	private int id;
	
	// Must have an empty constructor for Kryonet
	public Lobby() {
		;
	}
	
	public Lobby(Game game,int id) {
		this.game=game;
		this.id=id;
		this.slots = new ArrayList<Slot>();
		this.chatMessages = new ArrayList<String>();
		this.settings = new HashMap<Setting,Object>();
	}
	
	/**
	 * Returns the Lobby ID
	 * @return lobby id
	 */
	public int getID() {
		return this.id;
	}
	
	/**
	 * Returns whether the entire Lobby is ready or not
	 * @return true if everyone is ready, false otherwise
	 */
	public boolean isReady(){
		boolean ready = true;
		
		for( Slot s : slots){
			if( !(s.isReady()) ){
				ready = false;
			}
		}			
		return ready;
	}
	
	/**
	 * Checks if passed user is owner
	 * @param user to be checked
	 * @return if is owner or not
	 */
	public boolean isOwner(Profile user) {
		return this.getSlot(0).getUser().equals(user);
	}
	
	/**
	 * Gets the game of the Lobby
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}
	
	// Just default to true
	public void addPlayer(Profile player, int slotID, byte[] image) {
		this.addPlayer(player, slotID, image, true);
	}
	
	/**
	 * Adds a player to the lobby on a specified slot
	 * @param player to add
	 * @param slotID to add the player to
	 * @param accepted if it has accepted or not
	 */
	public void addPlayer(Profile player, int slotID, byte[] image, boolean accepted) {
		// Add the user to the slot
		Slot slot = this.getSlot(slotID);
		slot.setUser(player);
		slot.setAccepted(accepted);
		slot.setImageToByteArray(image);
	}
	
	/**
	 * Gets list of all players
	 * @return list of players in Lobby
	 */
	public List<Profile> getPlayers() {
		List<Profile> players = new ArrayList<Profile>();
		for(Slot s : this.getSlots()) {
			if (s.isOccupied()) {
				players.add(s.getUser());
			}
		}
		return players;
	}
	
	/**
	 * Gets the lobby global settings
	 * @return lobby settings
	 */
	public Map<Setting,Object> getGlobalSettings() {
		return settings;
	}
	
	/**
	 * Sets new global settings
	 * @param settings to set to this Lobby
	 */
	public void setGlobalSettings(Map<Setting, Object> settings) {
		this.settings = settings;
	}
	
	/**
	 * Returns all empty slots on the lobby
	 * @return list of empty slots
	 */
	public List<Slot> getEmptySlots(){
		
		List<Slot> emptySlots = new ArrayList();
		
		for( Slot s : slots){
			if( !(s.isOccupied()) ){
				emptySlots.add(s);
			}
		}	
		return emptySlots;
	}
	
	/**
	 * Returns all slots of the Lobby
	 * @return list of slots
	 */
	public List<Slot> getSlots() {
		return slots;
	}
	
	public void addSlot(Slot slot) {
		this.slots.add(slot);
	}
	
	/**
	 * Returns a single slot by slot id
	 * @param slotID the slot ID
	 * @return single Slot
	 */
	public Slot getSlot(int slotID) {
		return slots.get(slotID);
	}
	
	
	/**
	 * Returns a single slot by username
	 * @param username
	 * @return single Slot
	 */
	public Slot getSlot(String username) {
		for(Slot slot : this.getSlots()) {
			if(slot.isOccupied() && slot.getUser().getUsername().equals(username)) {
				return slot;
			}
		}
		return new Slot();
	}
	
	/**
	 * Removes a slot by ID
	 * @param slotID the slot ID to be removed
	 */
	public void removeSlot(int slotID) {
		slots.set(slotID, new Slot(slotID));
	}
	
	/**
	 * Removes a slot by username
	 * @param username to delete
	 */
	public void removePlayer(String username) {
		for(Slot slot : slots) {
			if(slot.isOccupied() && slot.getUser().getUsername().equals(username)) {
				// Set an empty slot on that position
				this.removeSlot(slots.indexOf(slot));
			}
		}
	}
	
	/**
	 * Gets a single setting by identifier
	 * @param settingIdentifier the identifier of the setting (from game manifest)
	 * @return
	 */
	public Object getSetting(String settingIdentifier) {
		for (Iterator iter = settings.keySet().iterator(); iter.hasNext();) {
			Setting setting = (Setting) iter.next();
			
			if(setting.hashCode()==settingIdentifier.hashCode()) {
				return settings.get(setting);
			}
		}
		return null;
	}
	
	/**
	 * Add a chat message to the lobby. Saves it as username:message
	 * NOTICE this is an ugly solution and should use some kind of multimap instead
	 * @param msg to be added
	 */
	public void addChatMessage(String username, String msg) {
		this.chatMessages.add(username + ":" + msg);
	}
	
	/**
	 * Returns all chat messages
	 * @return all chat messages
	 */
	public List<String> getChatMessage() {
		return this.chatMessages;
	}
	@Override
	public int hashCode() {
		return id;
	}
	
	public String toString(){
		String players = "";
		for(Profile p: getPlayers()){
			players = players + " " + p.getUsername();
		}
		return "number of players in lobby: "+ this.getPlayers().size() + " : " + players;
	}
	
}
