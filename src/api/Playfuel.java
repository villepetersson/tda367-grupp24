package api;

import java.util.Map;

import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public interface Playfuel {
	public class NoSuchPasswordException extends RuntimeException {

	}

	/**
	 * Invalid slot. 
	 * Probably it doesn't exist
	 *
	 */
	public class InvalidSlotException extends RuntimeException {

	}
	
	/**
	 * Invalid setting. 
	 * Probably it doesn't exist
	 *
	 */
	public class InvalidSettingException extends RuntimeException {

	}

	/**
	 * Malformed results. 
	 * @see matchEndedWithResult
	 *
	 */
	public class MalformedResultsException extends RuntimeException {
	}


	/**
	 * Get the match identifier. 
	 * This is a unique ID representing one match to the gamehub.
	 * @return match id
	 */
	public int getMatchId();
	
	/**
	 * Get the port number which game clients should connect to the game server through.
	 * The PlayfuelGameServer should never use any other ports than the one assigned through
	 * this method.
	 * @return port number
	 */
	public int getPortNumber();
	
	/**
	 * Gets the name of the lobby that started this match.
	 * @return the lobby name
	 */
	public String getLobbyName();
	
	/**
	 * Gets a value of a global setting.
	 * These settings are fetched from the playfuel.mf and are set by the owner of the lobby.
	 * @param identifier for the option in the manifest.
	 * @return value of setting as Object, should be safe to type cast to the intended type.
	 * @throws InvalidSettingException if no setting with that identifier exists.
	 */
	public Object getValueOfGlobalSetting(String identifier) throws InvalidSettingException;
	
	/**
	 * Gets a value of a slot-specific setting.
	 * These settings are fetched from the playfuel.mf and are set by each player in the lobby
	 * @param slotid which slot
	 * @param identifier for the option in the manifest
	 * @return value of setting as Object, should be safe to type cast to the intended type.
	 * @throws InvalidSlotException
	 * @throws InvalidSettingException if no setting with that identifier exists.
	 */
	public Object getValueOfSlotSetting(int slotid,String identifier) throws InvalidSlotException,InvalidSettingException;

	/**
	 * Get how many slots there are in the match.
	 * @return total number of slots
	 */
	public int getNumberOfSlots();
	
	/**
	 * Identifies a slot on password string.
	 * The passwords are alphanumeric strings which are generated and sent to each PlayfuelGameClient
	 * on launch. Get this value from each client and use this method to decide which client should
	 * be assigned to which slot.
	 * @param password recieved from game client
	 * @return slotid
	 * @throws NoSuchPasswordException
	 */
	public int getSlotWithPassword(String password) throws NoSuchPasswordException;
	
	/**
	 * Returns a map of the passwords and slotids.
	 * @deprecated If possible, use {@link getSlotWithPassword(String password)} instead.
	 * @return java.util.Map with passwords as keys, and slotid's as values.
	 */
	@Deprecated
	public Map<String,Integer> getPasswordMap();
	
	/**
	 * Gets the username for a slot id
	 * @param slotid which slot
	 * @return username for that slot
	 * @throws InvalidSlotException
	 */
	public String getUserNameForSlot(int slotid) throws InvalidSlotException;
	
	/**
	 * Get the user image for a slot id
	 * @param slotid which slot
	 * @return user avatar
	 * @throws InvalidSlotException
	 */
	public ImageIcon getImageForSlot(int slotid) throws InvalidSlotException;
	
	/**
	 * Call this method when the server is started, and ready to accept connections
	 * from game clients.
	 */
	public void serverIsReady();
	
	/**
	 * Call this method when the match is ended.
	 * Sends back scores to the master server.
	 * 
	 * The ranking value for each player should be an Integer, where 1 means the player
	 * was the winner, and higher values means the player was ranked worse in this match.
	 * 
	 * @param results a map with key slotid and value ranking ranging from 1 to number of players.
	 * @throws MalformedResultsException if not all slots are present.
	 */
	public void matchEndedWithResult(Map<Integer,Integer> results) throws MalformedResultsException;
	
	/**
	 * Call this method to end an unranked match without sending scores back to the master server.
	 */
	public void matchEndedWithoutResults();
	
	/**
	 * Call this method if the match was aborted prematurely.
	 */
	public void matchAborted();
}
