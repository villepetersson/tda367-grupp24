package mockimpl;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

import api.*;
import api.Playfuel.MalformedResultsException;


import utilities.ManifestHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.MatchEndedWithResultsMessage;
import models.Lobby;
import models.Slot;
import gameHub.MatchHandler;

public class MockMatchHandler extends MatchHandler {
	private String jarfile;
	public MockMatchHandler(Lobby lobby, int matchid, int port, String jarfile) {
		super(lobby, matchid, port);
		this.jarfile=jarfile;
	}

	@Override
	public void serverIsReady() {
		for(Slot slot : lobby.getSlots()) {
			try {
				Object obj;
				obj = ManifestHandler
							.getClassFromURL(new URL("file://"+jarfile), lobby.getGame().getGameClientClassName())
							.getConstructor()
							.newInstance();
	
				
				for(final Entry<String, Integer> ent : passwords.entrySet()) {
					if(ent.getValue().equals(new Integer(slot.getID()))) {
						if(obj instanceof PlayfuelGameClient) {
							final PlayfuelGameClient gameclient = (PlayfuelGameClient)obj;
							new Thread()
				    		{
				    		    public void run() {
				    		    	gameclient.clientLaunchedByPlayfuel("localhost",port,ent.getKey());
				    		    }
				    		}.start();
				    		System.out.println("Client Launched");
				    	}
					}
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

		}
			
	}
	
	@Override
	public void matchEndedWithResult(Map<Integer, Integer> results)
			throws MalformedResultsException {
		System.out.println(results);
	}
}
