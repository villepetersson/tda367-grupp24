package mockimpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.JFileChooser;

import api.*;

import models.Game;
import models.Lobby;
import models.Profile;
import models.Slot;

import utilities.ManifestHandler;

public class PlayfuelTester {
	private static int slots;
	private static Map<Integer,String> users = new HashMap<Integer, String>();
	private static Map<String,String> settings = new HashMap<String, String>();

	private static Map<Integer,Map<String,String>> slotsettings = new HashMap<Integer, Map<String,String>>();

	
	public static void main(String[] args) throws IOException, IllegalArgumentException, SecurityException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.print("\nEnter command (or write 'help'): ");
			if (sc.hasNextLine()) {
				try {
					String input = sc.nextLine();
					String command[] = input.split(" ");
					
					if(command[0].equals("file")) {
						JFileChooser chooser = new JFileChooser(".");
						//chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
						chooser.showOpenDialog(null);
						File file = chooser.getSelectedFile();
						sc = new Scanner(file);
					}
					
					if(command[0].equals("endfile")) {
						sc = new Scanner(System.in);
					}
					
					if(command[0].equals("help")) {
						System.out.println("Commands:\n" +
								"file - Select a file with several commands to run, one on each line.\n" +
								"endfile - Used in files to let the mockclient return to taking cli input.\n" +
								"slots <num> - Sets number of slots\n" +
								"user <id> <name> - Creates a user with name on slot id.\n" +
								"setting <identifier> <value> - Sets the setting identifier to value.\n" +
								"slotsetting <slotid> <identifier> <value> - Sets the slotsetting for slotid.\n" +
								"start - Launches gui to select jar or manifest.");
					}
					
					if(command[0].equals("slots")) {
						// 1: number of users
						slots=new Integer(command[1]).intValue();
					}
					
					if(command[0].equals("user")) {
						// 1: number of users
						users.put(new Integer(command[1]).intValue(),command[2]);
					}
					
					if(command[0].equals("setting")) {
						// 1: setting name
						// 2: setting value
						settings.put(command[1], command[2]);
					}
					
					if(command[0].equals("slotsetting")) {
						// 1: slot
						// 2: setting name
						// 3: setting value
						if(!slotsettings.containsKey(new Integer(command[1]).intValue())) {
							slotsettings.put(new Integer(command[1]).intValue(),new HashMap<String, String>());
						}
						
						slotsettings.get(new Integer(command[1]).intValue())
								.put(command[2], command[3]);
					}
					
					if(command[0].equals("start")) {
						// start jarfile
						// start jarfile manifestfile
						// start folder manifestfile
						JarFile jarFile;
						String file;
						Object obj;
						InputStream in = null;
						if(command.length>1) {
							file = command[1];
							jarFile = new JarFile(file);
							
							if(command.length==2) {
								String manifestfile = command[2];
								
								in = new FileInputStream("file://"+manifestfile);
							}
						} else {
							JFileChooser chooser = new JFileChooser(".");
							//chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
							chooser.showOpenDialog(null);
							file = chooser.getSelectedFile().toURI().getPath();
							if(file.endsWith(".jar")) {
								jarFile = new JarFile(file);
								JarEntry manifestentry = jarFile.getJarEntry("playfuel.mf");
								in = jarFile.getInputStream(manifestentry);
								
							} else if(file.endsWith(".mf")) {
								in = new FileInputStream(file);
								file=file.replace("playfuel.mf", "");
							} else {
								throw new IllegalArgumentException("Not a jar or manifest.");
							}
						}
						
						
						Game game = ManifestHandler.getGameFromManifest(in);
	
						Lobby lobby = new Lobby(game, 0);
	
						lobby.setGlobalSettings(
								ManifestHandler.populateSettingsWithFactory(
										lobby.getGame().getSettingsFactory(),
										settings
								)
						);
						
						if(slots==0) {
							slots=game.getMaxNumberOfSlots();
						}
						
						for(int i=0;i<slots;i++) {
							lobby.getSlots().add(new Slot(i));
							
							if(users!=null && users.containsKey(new Integer(i))) {
								Profile profile = new Profile();
								profile.setUsername(users.get(new Integer(i)));
								
								lobby.getSlot(i).setUser(profile);
							}
							
							lobby.getSlot(i).setSettings(
									ManifestHandler.populateSettingsWithFactory(
											lobby.getGame().getSlotSettingsFactory(),
											slotsettings.get(new Integer(i))
											)
							);
	
						}
						
						obj = ManifestHandler
								.getClassFromURL(new URL("file://"+file), game.getGameServerClassName())
								.getConstructor()
								.newInstance();
						
				    	if(obj instanceof PlayfuelGameServer) {
				    		final PlayfuelGameServer gameserver = (PlayfuelGameServer)obj;
				    		
				    		class Gamethread extends Thread {
				    			Lobby lobby;
				    			String file;
				    			public Gamethread(Lobby lobby, String file) {
				    				this.lobby=lobby;
				    				this.file=file;
				    			}
				    			public void run() {
						    		gameserver.serverLaunchedByPlayfuel(new MockMatchHandler(lobby,1,6790,file));
				    		    }
				    		}
				    		
				    		new Gamethread(lobby, file).start();			    		
				    		
				    		System.out.println("Server Launched");
				    	}
						
					
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
