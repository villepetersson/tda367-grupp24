package gameHub;

import java.io.File;
import java.io.IOException;
import java.util.*;

import models.Game;

import utilities.*;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			if(args.length==1) {
				new GameHub(args[0]);
			} else {
				new GameHub(LocalSettings.getSettings().getMasterserver());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
