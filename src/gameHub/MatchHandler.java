package gameHub;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.ImageIcon;

import api.Playfuel;

import models.Lobby;
import models.Setting;

import utilities.msg.MessageBus;
import utilities.msg.messages.GameListGetMessage;
import utilities.msg.messages.MatchEndedWithResultsMessage;
import utilities.msg.messages.MatchStartedMessage;

public class MatchHandler implements Playfuel {
	protected Lobby lobby;
	protected int matchid;
	protected int port;
	protected HashMap<String, Integer> passwords;
		
	public MatchHandler(Lobby lobby,int matchid,int port) {
		this.lobby=lobby;
		this.matchid=matchid;
		this.port=port;
		this.passwords=new HashMap<String, Integer>();
		
		SecureRandom random = new SecureRandom();
		String seed = new BigInteger(130, random).toString(32);
		
		for(int i=0;i<lobby.getSlots().size();i++) {
			try {
				byte[] message = (lobby.getSlot(i).getUser().getUsername()+seed).getBytes("UTF-8");
				
				MessageDigest md = MessageDigest.getInstance("MD5");
				String pw = (new BigInteger(1,md.digest(message))).toString(16);
				passwords.put(pw,i);
				
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}

	}

	@Override
	public int getMatchId() {
		return matchid;
	}

	@Override
	public int getPortNumber() {
		return port;
	}

	@Override
	public String getLobbyName() {
		return this.lobby.getGame().getGameName();
	}

	@Override
	public Object getValueOfGlobalSetting(String identifier) throws InvalidSettingException {
		Object value = null;
		for(Entry<Setting, Object> e : lobby.getGlobalSettings().entrySet()) {
			if(e.getKey().hashCode()==identifier.hashCode()) {
				value=e.getValue();
			}
		}
		
		if (value!=null) {
			return value;
		} else {
			throw new InvalidSettingException();
		}
	}

	@Override
	public Object getValueOfSlotSetting(int slotid, String identifier)
			throws InvalidSlotException, InvalidSettingException {
		try{
			Object value = lobby.getSlot(slotid).getSetting(identifier);
			
			if (value!=null) {
				return value;
			} else {
				throw new InvalidSettingException();
			}
		} catch (IndexOutOfBoundsException exc) {
			throw new InvalidSlotException();
		}
	}

	@Override
	public int getNumberOfSlots() {
		return lobby.getGame().getMaxNumberOfSlots();
	}
	
	@Override
	public String getUserNameForSlot(int slotid) throws InvalidSlotException {
		try{
			return lobby.getSlot(slotid).getUser().getUsername();
		} catch (IndexOutOfBoundsException exc) {
			throw new InvalidSlotException();
		}
	}

	@Override
	public ImageIcon getImageForSlot(int slotid) throws InvalidSlotException {
		return new ImageIcon(this.lobby.getSlot(slotid).getImage());
	}

	@Override
	public int getSlotWithPassword(String password)
			throws NoSuchPasswordException {
		if(passwords.containsKey(password)) {
			return passwords.get(password);
		}
		throw new NoSuchPasswordException();
	}

	@Override
	public void serverIsReady() {
		MessageBus.getInstance().pushMessage(new MatchStartedMessage(lobby.hashCode(), this.matchid,this.port,this.passwords));
	}

	@Override
	@Deprecated
	public Map<String, Integer> getPasswordMap() {
		return passwords;
	}

	@Override
	public void matchEndedWithoutResults() {
		// TODO
	}
	
	@Override
	public void matchEndedWithResult(Map<Integer, Integer> results)
			throws MalformedResultsException {
		MessageBus.getInstance().pushMessage(new MatchEndedWithResultsMessage(lobby.hashCode(),results));
		
	}

	@Override
	public void matchAborted() {
		// TODO
	}

}
