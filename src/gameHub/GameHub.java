package gameHub;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


import api.Playfuel;
import api.PlayfuelGameServer;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import models.Game;
import models.IMessage;
import models.Setting;
import models.User;

import utilities.ClientConnection;
import utilities.ManifestHandler;
import utilities.msg.IMessageHandler;
import utilities.msg.MessageBus;
import utilities.msg.messages.GameHubRegisterMessage;
import utilities.msg.messages.MatchStartMessage;
import utilities.msg.messages.MatchStartedMessage;
import utilities.msg.messages.UserLoginMessage;

public class GameHub implements IMessageHandler {
	
	// List of hosted games
	private static List<Game> games = new ArrayList<Game>();
	private static HashMap<String,Class<?>> gameClasses = new HashMap<String,Class<?>>();
	
	private int port=6790;
	private int i=0;
	
	public GameHub(String address) throws IOException {
		MessageBus.getInstance().registerFromServer(this);
		System.out.println("-- Game server initiated --");
		
		// Tell the connection that it should send this to remotes
		ClientConnection clientConnection = new ClientConnection(address);
		
		
		
		final File folder = new File("./games/");
		
		ManifestHandler.listGamesFromFolder(games, gameClasses, true, folder);
		
		// This should notify the server about games that this hub hosts 
		boolean messageSent = false;
		while(messageSent == false) {
			if(clientConnection.isConnected()) {
				MessageBus.getInstance().pushMessage(new GameHubRegisterMessage(games));
				messageSent = true;
			}
		}
		while(true){} // stay alive
	}
	


	@Override
	public void onMessage(IMessage imsg) {
		if(imsg instanceof MatchStartMessage) {
			final MatchStartMessage msg = (MatchStartMessage)imsg;
						
			final Class<?> c = gameClasses.get(msg.lobby.getGame().getIdentifier());
			
			new Thread(){
				public void run() {		
					Object obj;
					try {
						obj = c.getConstructor().newInstance();
						
				    	if(obj instanceof PlayfuelGameServer) {
				    		PlayfuelGameServer gameserver = (PlayfuelGameServer)obj;
				    		gameserver.serverLaunchedByPlayfuel(new MatchHandler(msg.lobby,i++,port++));
				    	}
						
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (SecurityException e) {
						e.printStackTrace();
					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					}
				}
			}.start();
		}
	}
}
