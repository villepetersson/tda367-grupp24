package models;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import utilities.ManifestHandler;

import static org.mockito.Mockito.*;



public class LobbyModelTest {
	private Lobby model;
	private Game game;
	private int ID =1;
	
	
	@Before
	public void before() {
		game = new Game();
		model = new Lobby(game , ID);		
	}
	
	@Test
	public void testLobbyCanBeCrated() {
		assertNotNull(model);
	}
	
	@Test
	public void testGameIsSetProperly() {
		assertTrue(model.getGame().equals(game));
	}
	@Test
	public void testIdIsSetProperly() {
		assertTrue(model.getID() == (ID));
	}
	
	@Test
	public void testSlotsAreSetProperly() {
		model.addSlot(new Slot(0));
		assertEquals(1, model.getSlots().size());
	}
	
	@Test
	public void testEmptySlotsAreReportedCorrectly() {
		// Empty slot
		model.addSlot(new Slot(0));
		// Non-empty
		model.addSlot(new Slot(1));
		model.getSlot(1).setUser(new Profile());
		assertEquals(1, model.getEmptySlots().size());
	}
	
	@Test
	public void testLobbyIsNotReadyWhenOnePlayerIsNot() {
		model.addSlot(new Slot(0));
		model.addSlot(new Slot(1));
		
		model.getSlot(0).setReady(true);
		model.getSlot(1).setReady(false);
		
		assertFalse(model.isReady());
	}
	
	@Test
	public void testLobbyIsReadyWhenAllPlayersAre() {
		model.addSlot(new Slot(0));
		model.addSlot(new Slot(1));
		
		model.getSlot(0).setReady(true);
		model.getSlot(1).setReady(true);
		
		assertTrue(model.isReady());
	}
	
	@Test
	public void testOwnerIsReportedCorrect() {
		Profile playerOne = mock(Profile.class);
		Profile playerTwo = mock(Profile.class);
		
		model.addSlot(new Slot(0));
		model.addSlot(new Slot(1));
		
		model.addPlayer(playerOne, 0, null);
		model.addPlayer(playerTwo, 1, null);
		
		assertTrue(model.isOwner(playerOne));
		assertFalse(model.isOwner(playerTwo));
	}
	
	@Test
	public void testPlayersAreSetProperly() {
		model.addSlot(new Slot(0));
		model.addSlot(new Slot(1));
		
		model.addPlayer(new Profile(), 0, null);
		model.addPlayer(new Profile(), 1, null);
		
		assertEquals(2, model.getPlayers().size());
	}
	
	@Test
	public void testFindSlotByPlayerUsernameWorks() {
		model.addSlot(new Slot(0));
		model.addSlot(new Slot(1));
		
		Profile p1 = new Profile();
		p1.setUsername("p1");
		
		Profile p2 = new Profile();
		p2.setUsername("p2");
		
		model.addPlayer(p1, 0, null);
		model.addPlayer(p2, 1, null);
		
		assertTrue(p2 == model.getSlot("p2").getUser());
		
		// Test that an empty slot is returned if username does not exist
		assertTrue(new Slot().hashCode() == model.getSlot("p3").hashCode());
	}
	
	@Test
	public void testHashCodeIsDifferentForTwoLobbies() {
		assertFalse(model.hashCode() == new Lobby(new Game(), 2).hashCode());
	}
	
	@Test
	public void testChatMessagesAreAdded () {
		model.addChatMessage("mock", "mock");
		assertEquals(1, model.getChatMessage().size());
	}
	@Test
	public void testSettingsMaptVariabel(){
		assertTrue(model.getGlobalSettings() instanceof HashMap<?, ?>);
	}
	
	@Test
	public void testChatMessageListVariabel(){
		assertTrue(model.getChatMessage() instanceof ArrayList<?>);
	}
	
	@Test
	public void testAddSlotMethod(){
		Profile player = new Profile(new User());
		Slot slot = new Slot(player, 0);
		model.addSlot(slot);
		assertTrue(slot.equals(model.getSlot(0)));
	}
	
	@Test
	public void testRemoveSlotMethod(){
		Profile player = new Profile(new User());
		Slot slot = new Slot(player, 0);
		model.addSlot(slot);
		model.removeSlot(slot.getID());
		assertFalse(slot.equals(model.getSlot(slot.getID())));
	}
	
	@Test
	public void testAddPlayerMethod() {
		Profile mockProfile = mock(Profile.class);
		model.addSlot(new Slot(0));
		model.addPlayer(mockProfile, 0, null);
		
		assertTrue(mockProfile.equals(model.getSlot(0).getUser()));
	}
	
	@Test
	public void testRemovePlayer(){
		Profile mockProfile = new Profile();
		mockProfile.setUsername("test");
		model.addSlot(new Slot(0));
		model.addPlayer(mockProfile, 0, null);
		model.removePlayer(mockProfile.getUsername());
		
		
		assertNull(model.getSlot(0).getUser());
	}
	
	
}
