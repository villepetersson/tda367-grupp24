package masterServer.ctrl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.DatabaseInterface;
import models.Friend;
import models.RankingResult;
import models.RemoteConnection;
import models.User;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class RankingControllerTest {

	private RankingController rc;
	private String game_identifier;
	
	/**
	 * Before entire testsuite
	 */
	@BeforeClass
	public static void beforeClass() {}
	
	/**
	 * Before each test
	 */
	@Before
	public void before() {
		rc = new RankingController();
		game_identifier = "testgame";
	}
	
	/**
	 * After entire testsuite
	 */
	@AfterClass
	public static void afterClass() {}

	/**
	 * Make sure the creation of the class was successful
	 */
	@Test
	public void testClassCanBeCreated() {
		assertTrue(rc instanceof RankingController);
	}
	
	/**
	 * Test that the probability that two not equally good players sums ut to one
	 */
	@Test
	public void testProbabilityCountSumsUpToOneForUnequalPlayers() {
		User mockA = new User("usera", "passa", new TestDB());
		User mockB = new User("userb", "passb", new TestDB());
		String game_identifier = "testgame";
		
		// Set some rankings
		mockA.setRanking(game_identifier, 1300);
		mockA.setRanking(game_identifier, 1200);
		
		// Use reflection for testing private methods
		Method method;
		try {
			method = rc.getClass().getDeclaredMethod("calcutateWinningProbability", new Class[] { Double.class, Double.class });
			
			method.setAccessible(true);
			
			Object probA = method.invoke(rc, new Object[] { mockA.getRanking(game_identifier), mockB.getRanking(game_identifier) });
			Object probB = method.invoke(rc, new Object[] { mockB.getRanking(game_identifier), mockA.getRanking(game_identifier) });
			
			// With considered accepted rounding precicion
			assertTrue(((Double) probA + (Double) probB) - 1 < 1e-14);
			
		} catch (SecurityException e) {
			fail();
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			fail();
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			fail();
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail();
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			fail();
			e.printStackTrace();
		}
	}
	
	/**
	 * Test that the probability that two equally good players sums ut to one
	 */
	@Test
	public void testProbabilityCountSumsUpToOneForEqualPlayers() {
		User mockA = new User("usera", "passa", new TestDB());
		User mockB = new User("userb", "passb", new TestDB());
		
		// Set some rankings
		mockA.setRanking(game_identifier, 1300);
		mockA.setRanking(game_identifier, 1300);
		
		// Use reflection for testing private methods
		Method method;
		try {
			method = rc.getClass().getDeclaredMethod("calcutateWinningProbability", new Class[] { Double.class, Double.class });

			method.setAccessible(true);

			Object probA = method.invoke(rc, new Object[] { mockA.getRanking(game_identifier), mockB.getRanking(game_identifier) });
			Object probB = method.invoke(rc, new Object[] { mockB.getRanking(game_identifier), mockA.getRanking(game_identifier) });

			assertTrue((Double) probA + (Double) probB == 1);

		} catch (SecurityException e) {
			fail();
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			fail();
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			fail();
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail();
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			fail();
			e.printStackTrace();
		}
	}
	
	/**
	 * Test that the probability that a better player wins is bigger than 50%
	 */
	@Test
	public void testProbabilityIsBiggerThatABetterPlayerWins() {
		User mockA = new User("usera", "passa", new TestDB());
		User mockB = new User("userb", "passb", new TestDB());
		
		// Set some rankings
		mockA.setRanking(game_identifier, 1400);
		mockA.setRanking(game_identifier, 1200);
		
		// Use reflection for testing private methods
		Method method;
		try {
			method = rc.getClass().getDeclaredMethod("calcutateWinningProbability", new Class[] { Double.class, Double.class });

			method.setAccessible(true);

			Object probA = method.invoke(rc, new Object[] { mockA.getRanking(game_identifier), mockB.getRanking(game_identifier) });
			Object probB = method.invoke(rc, new Object[] { mockB.getRanking(game_identifier), mockA.getRanking(game_identifier) });
			
			assertTrue((Double) probA < (Double) probB);

		} catch (SecurityException e) {
			fail();
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			fail();
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			fail();
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			fail();
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			fail();
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCalculatingNewRankingWhenTwoPlayersIsZeroSum() {
		User userA = new User("usera", "passa", new TestDB());
		User userB = new User("userb", "passb", new TestDB());
		
		userA.setRanking(game_identifier, 1200);
		userB.setRanking(game_identifier, 1300);
		
		double totalRankingBeforeMatch = userA.getRanking(game_identifier) + userB.getRanking(game_identifier);
		
		// User B won
		Map<User, Integer> results = new HashMap<User, Integer>();
		results.put(userA, 2);
		results.put(userB, 1);
		
		//Now calculate all the new rankings at once
		rc.calculateRanking(results, game_identifier);
		
		double totalRankingAfterMatch = userA.getRanking(game_identifier) + userB.getRanking(game_identifier);

		//Make sure there is no inflation in the ranking system
		assertTrue(totalRankingBeforeMatch == totalRankingAfterMatch);
	}
	
	/**
	 * Tests that multiple players (>2) scores are zero sum
	 */
	@Test
	public void testCalculatingNewRankingWhenMultiplayerAreZeroSum() {

		User userA = new User("usera", "passa", new TestDB());
		User userB = new User("userb", "passb", new TestDB());
		User userC = new User("userc", "passc", new TestDB());
		User userD = new User("userd", "passd", new TestDB());
		
		// User A and B are better ranked
		userA.setRanking(game_identifier, 1309);
		userB.setRanking(game_identifier, 1456);
		userC.setRanking(game_identifier, 1308);
		userD.setRanking(game_identifier, 1105);
		
		double totalRankingBeforeMatch = userA.getRanking(game_identifier) + userB.getRanking(game_identifier) +
				userC.getRanking(game_identifier) + userD.getRanking(game_identifier);
		
		Map<User, Integer> results = new HashMap<User, Integer>();
		results.put(userA, 1);
		results.put(userB, 2);
		results.put(userD, 3);
		results.put(userC, 4);
		
		//Now calculate all the new rankings at once
		rc.calculateRanking(results, game_identifier);
		
		double totalRankingAfterMatch = userA.getRanking(game_identifier) + userB.getRanking(game_identifier) +
				userC.getRanking(game_identifier) + userD.getRanking(game_identifier);

		//Make sure there is no inflation in the ranking system
		assertTrue(totalRankingBeforeMatch == totalRankingAfterMatch);
	}
	
	@Test
	public void testCalculatingNewRankingsWhenTeamsSreZeroSum() {

		User userA = new User("usera", "passa", new TestDB());
		User userB = new User("userb", "passb", new TestDB());
		User userC = new User("userc", "passc", new TestDB());
		User userD = new User("userd", "passd", new TestDB());
		
		// User A and B is a team and are better ranked
		userA.setRanking(game_identifier, 1309);
		userB.setRanking(game_identifier, 1456);
		
		userC.setRanking(game_identifier, 1308);
		userD.setRanking(game_identifier, 1105);
		
		double totalRankingBeforeMatch = userA.getRanking(game_identifier) + userB.getRanking(game_identifier) +
				userC.getRanking(game_identifier) + userD.getRanking(game_identifier);
		
		// Team scores are set so that the team members have the same placement
		Map<User, Integer> results = new HashMap<User, Integer>();
		results.put(userA, 1);
		results.put(userB, 1);
		results.put(userD, 2);
		results.put(userC, 2);
		
		//Now calculate all the new rankings at once
		rc.calculateRanking(results, game_identifier);
		
		double totalRankingAfterMatch = userA.getRanking(game_identifier) + userB.getRanking(game_identifier) +
				userC.getRanking(game_identifier) + userD.getRanking(game_identifier);

		//Make sure there is no inflation in the ranking system
		assertTrue(totalRankingBeforeMatch == totalRankingAfterMatch);
	}
	
	@Test
	public void testNothingHappensIfOnlyOneUserIsPassed() {
		User userA = new User("usera", "passa", new TestDB());
		userA.setRanking(game_identifier, 1309);
		
		double rankingBeforeMatch = userA.getRanking(game_identifier);
		
		Map<User, Integer> results = new HashMap<User, Integer>();
		results.put(userA, 1);
		
		rc.calculateRanking(results, game_identifier);
		
		assertTrue(rankingBeforeMatch == userA.getRanking(game_identifier));
	}
	
	@Test
	public void testUsersActuallyGetsNewRanking() {
		User userA = new User("usera", "passa", new TestDB());
		User userB = new User("userb", "passb", new TestDB());
		User userC = new User("userc", "passc", new TestDB());
		
		// User A and B is a team and are better ranked
		userA.setRanking(game_identifier, 1309);
		userB.setRanking(game_identifier, 1456);
		userC.setRanking(game_identifier, 1308);
		
		Map<User, Double> preMatchRankings = new HashMap<User, Double>();
		preMatchRankings.put(userA, userA.getRanking(game_identifier));
		preMatchRankings.put(userB, userB.getRanking(game_identifier));
		preMatchRankings.put(userC, userC.getRanking(game_identifier));
		
		// Match scores
		Map<User, Integer> results = new HashMap<User, Integer>();
		results.put(userA, 3);
		results.put(userB, 2);
		results.put(userC, 1);
		
		// Perform the actual calculations
		rc.calculateRanking(results, game_identifier);
		
		// Check that all users have gotten new rankings
		Iterator it = preMatchRankings.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry entry = (Map.Entry) it.next();
	        User currentUser = (User) entry.getKey();
	        
	        // Make sure no one has the same ranking after match
	        assertTrue(currentUser.getRanking(game_identifier) != (double) preMatchRankings.get(currentUser));
	    }
	}
	
	@Test
	public void testUsersDontGetNewRankingIfDraw() {
		User userA = new User("usera", "passa", new TestDB());
		User userB = new User("userb", "passb", new TestDB());
		
		double userAPreMatchRanking = 1309;
		double userBPreMatchRanking = 1456;
		
		userA.setRanking(game_identifier, userAPreMatchRanking);
		userB.setRanking(game_identifier, userBPreMatchRanking);
		
		Map<User, Integer> results = new HashMap<User, Integer>();
		results.put(userA, 2);
		results.put(userB, 2);
		
		// Perform the actual calculations
		rc.calculateRanking(results, game_identifier);
		
		assertTrue(userB.getRanking(game_identifier) == userBPreMatchRanking);
		assertTrue(userA.getRanking(game_identifier) == userAPreMatchRanking);
	}
	
	// Simple mock of the database to just store a users ranking
	private static class TestDB implements DatabaseInterface {
		
		private double ranking = 0; 

		@Override
		public double getUserRanking(User user, String game_identifier) {
			return this.ranking;
		}

		@Override
		public boolean setUserRanking(User user, String game_identifier,
				double ranking) {
			this.ranking = ranking;
			return true;
		}
		@Override
		public boolean addFriendRequest(User requesting_user,
				User requested_user) {
			return false;
		}

		@Override
		public boolean addFriend(User requesting_user, User requested_user,
				boolean accepted) {
			return false;
		}

		@Override
		public boolean acceptFriendRequest(User requested_user,
				User requesting_user) {
			return false;
		}

		@Override
		public boolean declineFriendRequest(User requested_user,
				User requesting_user) {
			return false;
		}

		@Override
		public boolean removeFriend(User user1, User user2) {
			// 
			return false;
		}

		@Override
		public List<Friend> findFriendsForUser(User user,
				boolean includeFriendRequests) {
			return null;
		}

		@Override
		public boolean registerUser(User u) {
			return false;
		}

		@Override
		public Set<User> findUsersByUsername(String query) {
			return null;
		}

		@Override
		public User getUserByUsername(String username) {
			return null;
		}

		@Override
		public User getUserByID(int userID) {
			return null;
		}

		@Override
		public Set<User> findAllUsers() {
			return null;
		}


		@Override
		public List<RankingResult> getGameRankings(String gameIdentifier,
				int limit, String order) {
			return null;
		}

		@Override
		public void updateUser(User user) {
		}

		@Override
		public void setUserConnection(String username, RemoteConnection conn) {
		}

		@Override
		public RemoteConnection getUserConnection(String username) {
			return null;
		}

		@Override
		public void removeUserConnection(String username) {}

		@Override
		public Map<String, byte[]> getUserImages() {
			return null;
		}
		
	}

}
