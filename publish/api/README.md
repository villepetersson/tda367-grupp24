playfuelllib: The main library to import.
PlayfuelTester: A utility to test the Playfuel implementation of games.
Manifests.pdf: Documentation for creating manifests.

examples/pong.jar: A very basic (and buggy) game which uses playfuel.
examples/pongscript: A script to use with "file" in PlayfuelTester when testing pong.
doc/: Javadoc of the lib.