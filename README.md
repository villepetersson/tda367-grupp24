# Installation

(See /docs for more in depth documentation)

## Before starting
All excecutable jars are in the /publish folder. Be sure to stand in that folder while excecuting not to get any path issues.

## Master Server 

The master server should be up and running before anything else. Simply run the master server excecutable (publish/masterserver.jar) to start it. The first time you start it on a new system you need to setup the database. You do that by using the Playfuel console. Simply type “help” in the console to bring up the help or just follow these instructions:

1. Type: “db:setup” to setup the database tables. (db:setup:force if you want to force reinstall). 
2. Type: “db:seed” (optional) to seed the database with example users, friendships and rankings.

## Game Hub 

The game hub should be started after the master server (but it's not neccesary) . Simply start the excecutable (publish/gh.jar) with the IP of the master server as argument. If no argument is provided it falls back to localhost.

## Client 

Start the excecutable (publish/client.jar). To set the IP of the masterserver you can edit the file “settings.json” which is put in your computers home folder in <home_folder>/.PlayfuelResources

## Troubleshooting & known bugs

- The remember me function is a bit unstable. It will say that your account is used somewhere else. To remove the autologin credentials, simply dismiss the modal, head to the Accountpanel and click sign out.
- If a user appears as yellow (idle) even if they are not. Just click back and forth to the Social panel to refresh the friendslist
- The actual Pong game logic is under some cirqumstances unstable on Linux